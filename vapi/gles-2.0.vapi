/* gles-2.0.vapi
 *
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

#if GLES2_AVAILABLE
[CCode (lower_case_cprefix="", cheader_filename="GLES2/gl2.h,GLES2/gl2ext.h")]
#else
#if WINDOWS
[CCode (cheader_filename="windows.h")]
#endif
[CCode (lower_case_cprefix="", cheader_filename="GLES2/gl2.h,GLES2/gl2ext.h")]
#endif

namespace GL {
    [CCode (cname="GLvoid")]
    public struct GLvoid { }
    [CCode (cname="GLchar")]
    public struct GLchar : char { }
    [CCode (cname="GLenum")]
    public struct GLenum : uint { }
    [CCode (cname="GLboolean")]
    public struct GLboolean : bool { }
    [CCode (cname="GLbitfield")]
    public struct GLbitfield : uint { }
    [CCode (cname="GLbyte")]
    public struct GLbyte : char { }
    [CCode (cname="GLshort")]
    public struct GLshort : short { }
    [CCode (cname="GLint")]
    public struct GLint : int { }
    [CCode (cname="GLsizei")]
    public struct GLsizei : int { }
    [CCode (cname="GLubyte")]
    public struct GLubyte : uchar { }
    [CCode (cname="GLushort")]
    public struct GLushort : ushort { }
    [CCode (cname="GLuint")]
    public struct GLuint : uint { }
    [CCode (cname="GLfloat")]
    [FloatingType (rank = 1)]
    public struct GLfloat : float { }
    [CCode (cname="GLclampf")]
    [FloatingType (rank = 1)]
    public struct GLclampf : float { }
    [CCode (cname="GLfixed")]
    public struct GLfixed : int { }
    [CCode (cname="GLintptr")]
    public struct GLintptr : int { }
    [CCode (cname="GLsizeiptr")]
    public struct GLsizeiptr : int { }

    // ClearBufferMask
    public const GLenum GL_DEPTH_BUFFER_BIT;
    public const GLenum GL_STENCIL_BUFFER_BIT;
    public const GLenum GL_COLOR_BUFFER_BIT;

    // Boolean
    public const GLboolean GL_FALSE;
    public const GLboolean GL_TRUE;

    // BeginMode
    public const GLenum GL_POINTS;
    public const GLenum GL_LINES;
    public const GLenum GL_LINE_LOOP;
    public const GLenum GL_LINE_STRIP;
    public const GLenum GL_TRIANGLES;
    public const GLenum GL_TRIANGLE_STRIP;
    public const GLenum GL_TRIANGLE_FAN;

    // BlendingFactorDest
    public const GLenum GL_ZERO;
    public const GLenum GL_ONE;
    public const GLenum GL_SRC_COLOR;
    public const GLenum GL_ONE_MINUS_SRC_COLOR;
    public const GLenum GL_SRC_ALPHA;
    public const GLenum GL_ONE_MINUS_SRC_ALPHA;
    public const GLenum GL_DST_ALPHA;
    public const GLenum GL_ONE_MINUS_DST_ALPHA;

    // BlendingFactorSrc
    public const GLenum GL_DST_COLOR;
    public const GLenum GL_ONE_MINUS_DST_COLOR;
    public const GLenum GL_SRC_ALPHA_SATURATE;

    // BlendEquationSeparate
    public const GLenum GL_FUNC_ADD;
    public const GLenum GL_BLEND_EQUATION;
    public const GLenum GL_BLEND_EQUATION_RGB;
    public const GLenum GL_BLEND_EQUATION_ALPHA;

    // BlendSubtract
    public const GLenum GL_FUNC_SUBTRACT;
    public const GLenum GL_FUNC_REVERSE_SUBTRACT;

    // Separate Blend Functions
    public const GLenum GL_BLEND_DST_RGB;
    public const GLenum GL_BLEND_SRC_RGB;
    public const GLenum GL_BLEND_DST_ALPHA;
    public const GLenum GL_BLEND_SRC_ALPHA;
    public const GLenum GL_CONSTANT_COLOR;
    public const GLenum GL_ONE_MINUS_CONSTANT_COLOR;
    public const GLenum GL_CONSTANT_ALPHA;
    public const GLenum GL_ONE_MINUS_CONSTANT_ALPHA;
    public const GLenum GL_BLEND_COLOR;

    // Buffer Objects
    public const GLenum GL_ARRAY_BUFFER;
    public const GLenum GL_ELEMENT_ARRAY_BUFFER;
    public const GLenum GL_ARRAY_BUFFER_BINDING;
    public const GLenum GL_ELEMENT_ARRAY_BUFFER_BINDING;
    public const GLenum GL_STREAM_DRAW;
    public const GLenum GL_STATIC_DRAW;
    public const GLenum GL_DYNAMIC_DRAW;
    public const GLenum GL_BUFFER_SIZE;
    public const GLenum GL_BUFFER_USAGE;
    public const GLenum GL_CURRENT_VERTEX_ATTRIB;

    // CullFaceMode
    public const GLenum GL_FRONT;
    public const GLenum GL_BACK;
    public const GLenum GL_FRONT_AND_BACK;

    // EnableCap
    public const GLenum GL_TEXTURE_2D;
    public const GLenum GL_CULL_FACE;
    public const GLenum GL_BLEND;
    public const GLenum GL_DITHER;
    public const GLenum GL_STENCIL_TEST;
    public const GLenum GL_DEPTH_TEST;
    public const GLenum GL_SCISSOR_TEST;
    public const GLenum GL_POLYGON_OFFSET_FILL;
    public const GLenum GL_SAMPLE_ALPHA_TO_COVERAGE;
    public const GLenum GL_SAMPLE_COVERAGE;

    // ErrorCode
    public const GLenum GL_NO_ERROR;
    public const GLenum GL_INVALID_ENUM;
    public const GLenum GL_INVALID_VALUE;
    public const GLenum GL_INVALID_OPERATION;
    public const GLenum GL_OUT_OF_MEMORY;

    // FogMode
    public const GLenum GL_EXP;
    public const GLenum GL_EXP2;

    // FogParameter
    public const GLenum GL_FOG_DENSITY;
    public const GLenum GL_FOG_START;
    public const GLenum GL_FOG_END;
    public const GLenum GL_FOG_MODE;
    public const GLenum GL_FOG_COLOR;

    // FrontFaceDirection
    public const GLenum GL_CW;
    public const GLenum GL_CCW;

    // GetPName
    public const GLenum GL_LINE_WIDTH;
    public const GLenum GL_ALIASED_POINT_SIZE_RANGE;
    public const GLenum GL_ALIASED_LINE_WIDTH_RANGE;
    public const GLenum GL_CULL_FACE_MODE;
    public const GLenum GL_FRONT_FACE;
    public const GLenum GL_DEPTH_RANGE;
    public const GLenum GL_DEPTH_WRITEMASK;
    public const GLenum GL_DEPTH_CLEAR_VALUE;
    public const GLenum GL_DEPTH_FUNC;
    public const GLenum GL_STENCIL_CLEAR_VALUE;
    public const GLenum GL_STENCIL_FUNC;
    public const GLenum GL_STENCIL_FAIL;
    public const GLenum GL_STENCIL_PASS_DEPTH_FAIL;
    public const GLenum GL_STENCIL_PASS_DEPTH_PASS;
    public const GLenum GL_STENCIL_REF;
    public const GLenum GL_STENCIL_VALUE_MASK;
    public const GLenum GL_STENCIL_WRITEMASK;
    public const GLenum GL_STENCIL_BACK_FUNC;
    public const GLenum GL_STENCIL_BACK_FAIL;
    public const GLenum GL_STENCIL_BACK_PASS_DEPTH_FAIL;
    public const GLenum GL_STENCIL_BACK_PASS_DEPTH_PASS;
    public const GLenum GL_STENCIL_BACK_REF;
    public const GLenum GL_STENCIL_BACK_VALUE_MASK;
    public const GLenum GL_STENCIL_BACK_WRITEMASK;
    public const GLenum GL_VIEWPORT;
    public const GLenum GL_SCISSOR_BOX;
    public const GLenum GL_COLOR_CLEAR_VALUE;
    public const GLenum GL_COLOR_WRITEMASK;
    public const GLenum GL_UNPACK_ALIGNMENT;
    public const GLenum GL_PACK_ALIGNMENT;
    public const GLenum GL_MAX_TEXTURE_SIZE;
    public const GLenum GL_MAX_VIEWPORT_DIMS;
    public const GLenum GL_SUBPIXEL_BITS;
    public const GLenum GL_RED_BITS;
    public const GLenum GL_GREEN_BITS;
    public const GLenum GL_BLUE_BITS;
    public const GLenum GL_ALPHA_BITS;
    public const GLenum GL_DEPTH_BITS;
    public const GLenum GL_STENCIL_BITS;
    public const GLenum GL_POLYGON_OFFSET_UNITS;
    public const GLenum GL_POLYGON_OFFSET_FACTOR;
    public const GLenum GL_TEXTURE_BINDING_2D;
    public const GLenum GL_SAMPLE_BUFFERS;
    public const GLenum GL_SAMPLES;
    public const GLenum GL_SAMPLE_COVERAGE_VALUE;
    public const GLenum GL_SAMPLE_COVERAGE_INVERT;
    public const GLenum GL_NUM_COMPRESSED_TEXTURE_FORMATS;
    public const GLenum GL_COMPRESSED_TEXTURE_FORMATS;

    // HintMode
    public const GLenum GL_DONT_CARE;
    public const GLenum GL_FASTEST;
    public const GLenum GL_NICEST;

    // HintTarget
    public const GLenum GL_GENERATE_MIPMAP_HINT;

    // DataType
    public const GLenum GL_BYTE;
    public const GLenum GL_UNSIGNED_BYTE;
    public const GLenum GL_SHORT;
    public const GLenum GL_UNSIGNED_SHORT;
    public const GLenum GL_FLOAT;
    public const GLenum GL_FIXED;

    // PixelFormat
    public const GLenum GL_DEPTH_COMPONENT;
    public const GLenum GL_ALPHA;
    public const GLenum GL_RGB;
    public const GLenum GL_RGBA;
    public const GLenum GL_LUMINANCE;
    public const GLenum GL_LUMINANCE_ALPHA;

    // PixelType
    public const GLenum GL_UNSIGNED_SHORT_4_4_4_4;
    public const GLenum GL_UNSIGNED_SHORT_5_5_5_1;
    public const GLenum GL_UNSIGNED_SHORT_5_6_5;

    // Shaders
    public const GLenum GL_FRAGMENT_SHADER;
    public const GLenum GL_VERTEX_SHADER;
    public const GLenum GL_MAX_VERTEX_ATTRIBS;
    public const GLenum GL_MAX_VERTEX_UNIFORM_VECTORS;
    public const GLenum GL_MAX_VARYING_VECTORS;
    public const GLenum GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS;
    public const GLenum GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS;
    public const GLenum GL_MAX_TEXTURE_IMAGE_UNITS;
    public const GLenum GL_MAX_FRAGMENT_UNIFORM_VECTORS;
    public const GLenum GL_SHADER_TYPE;
    public const GLenum GL_DELETE_STATUS;
    public const GLenum GL_LINK_STATUS;
    public const GLenum GL_VALIDATE_STATUS;
    public const GLenum GL_ATTACHED_SHADERS;
    public const GLenum GL_ACTIVE_UNIFORMS;
    public const GLenum GL_ACTIVE_UNIFORM_MAX_LENGTH;
    public const GLenum GL_ACTIVE_ATTRIBUTES;
    public const GLenum GL_ACTIVE_ATTRIBUTE_MAX_LENGTH;
    public const GLenum GL_SHADING_LANGUAGE_VERSION;
    public const GLenum GL_CURRENT_PROGRAM;

    // StencilFunction
    public const GLenum GL_NEVER;
    public const GLenum GL_LESS;
    public const GLenum GL_EQUAL;
    public const GLenum GL_LEQUAL;
    public const GLenum GL_GREATER;
    public const GLenum GL_NOTEQUAL;
    public const GLenum GL_GEQUAL;
    public const GLenum GL_ALWAYS;

    // StencilOp
    public const GLenum GL_KEEP;
    public const GLenum GL_REPLACE;
    public const GLenum GL_INCR;
    public const GLenum GL_DECR;
    public const GLenum GL_INVERT;
    public const GLenum GL_INCR_WRAP;
    public const GLenum GL_DECR_WRAP;

    // StringName
    public const GLenum GL_VENDOR;
    public const GLenum GL_RENDERER;
    public const GLenum GL_VERSION;
    public const GLenum GL_EXTENSIONS;

    // TextureMagFilter
    public const GLenum GL_NEAREST;
    public const GLenum GL_LINEAR;

    // TextureMinFilter
    public const GLenum GL_NEAREST_MIPMAP_NEAREST;
    public const GLenum GL_LINEAR_MIPMAP_NEAREST;
    public const GLenum GL_NEAREST_MIPMAP_LINEAR;
    public const GLenum GL_LINEAR_MIPMAP_LINEAR;

    // TextureParameterName
    public const GLenum GL_TEXTURE_MAG_FILTER;
    public const GLenum GL_TEXTURE_MIN_FILTER;
    public const GLenum GL_TEXTURE_WRAP_S;
    public const GLenum GL_TEXTURE_WRAP_T;

    // TextureTarget
    public const GLenum GL_TEXTURE;
    public const GLenum GL_TEXTURE_CUBE_MAP;
    public const GLenum GL_TEXTURE_BINDING_CUBE_MAP;
    public const GLenum GL_TEXTURE_CUBE_MAP_POSITIVE_X;
    public const GLenum GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
    public const GLenum GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
    public const GLenum GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
    public const GLenum GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
    public const GLenum GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
    public const GLenum GL_MAX_CUBE_MAP_TEXTURE_SIZE;

    // TextureUnit
    public const GLenum GL_TEXTURE0;
    public const GLenum GL_TEXTURE1;
    public const GLenum GL_TEXTURE2;
    public const GLenum GL_TEXTURE3;
    public const GLenum GL_TEXTURE4;
    public const GLenum GL_TEXTURE5;
    public const GLenum GL_TEXTURE6;
    public const GLenum GL_TEXTURE7;
    public const GLenum GL_TEXTURE8;
    public const GLenum GL_TEXTURE9;
    public const GLenum GL_TEXTURE10;
    public const GLenum GL_TEXTURE11;
    public const GLenum GL_TEXTURE12;
    public const GLenum GL_TEXTURE13;
    public const GLenum GL_TEXTURE14;
    public const GLenum GL_TEXTURE15;
    public const GLenum GL_TEXTURE16;
    public const GLenum GL_TEXTURE17;
    public const GLenum GL_TEXTURE18;
    public const GLenum GL_TEXTURE19;
    public const GLenum GL_TEXTURE20;
    public const GLenum GL_TEXTURE21;
    public const GLenum GL_TEXTURE22;
    public const GLenum GL_TEXTURE23;
    public const GLenum GL_TEXTURE24;
    public const GLenum GL_TEXTURE25;
    public const GLenum GL_TEXTURE26;
    public const GLenum GL_TEXTURE27;
    public const GLenum GL_TEXTURE28;
    public const GLenum GL_TEXTURE29;
    public const GLenum GL_TEXTURE30;
    public const GLenum GL_TEXTURE31;
    public const GLenum GL_ACTIVE_TEXTURE;

    // TextureWrapMode
    public const GLenum GL_REPEAT;
    public const GLenum GL_CLAMP_TO_EDGE;
    public const GLenum GL_MIRRORED_REPEAT;

    // Uniform Types
    public const GLenum GL_FLOAT_VEC2;
    public const GLenum GL_FLOAT_VEC3;
    public const GLenum GL_FLOAT_VEC4;
    public const GLenum GL_INT_VEC2;
    public const GLenum GL_INT_VEC3;
    public const GLenum GL_INT_VEC4;
    public const GLenum GL_BOOL;
    public const GLenum GL_BOOL_VEC2;
    public const GLenum GL_BOOL_VEC3;
    public const GLenum GL_BOOL_VEC4;
    public const GLenum GL_FLOAT_MAT2;
    public const GLenum GL_FLOAT_MAT3;
    public const GLenum GL_FLOAT_MAT4;
    public const GLenum GL_SAMPLER_2D;
    public const GLenum GL_SAMPLER_CUBE;

    // Vertex Arrays
    public const GLenum GL_VERTEX_ATTRIB_ARRAY_ENABLED;
    public const GLenum GL_VERTEX_ATTRIB_ARRAY_SIZE;
    public const GLenum GL_VERTEX_ATTRIB_ARRAY_STRIDE;
    public const GLenum GL_VERTEX_ATTRIB_ARRAY_TYPE;
    public const GLenum GL_VERTEX_ATTRIB_ARRAY_NORMALIZED;
    public const GLenum GL_VERTEX_ATTRIB_ARRAY_POINTER;
    public const GLenum GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING;

    // Read Format
    public const GLenum GL_IMPLEMENTATION_COLOR_READ_TYPE;
    public const GLenum GL_IMPLEMENTATION_COLOR_READ_FORMAT;

    // Shader Source
    public const GLenum GL_COMPILE_STATUS;
    public const GLenum GL_INFO_LOG_LENGTH;
    public const GLenum GL_SHADER_SOURCE_LENGTH;
    public const GLenum GL_SHADER_COMPILER;

    // Shader Binary
    public const GLenum GL_SHADER_BINARY_FORMATS;
    public const GLenum GL_NUM_SHADER_BINARY_FORMATS;

    // Shader Precision-Specified Types
    public const GLenum GL_LOW_FLOAT;
    public const GLenum GL_MEDIUM_FLOAT;
    public const GLenum GL_HIGH_FLOAT;
    public const GLenum GL_LOW_INT;
    public const GLenum GL_MEDIUM_INT;
    public const GLenum GL_HIGH_INT;

    // Framebuffer Object
    public const GLenum GL_FRAMEBUFFER;
    public const GLenum GL_RENDERBUFFER;
    public const GLenum GL_RGBA4;
    public const GLenum GL_RGB5_A1;
    public const GLenum GL_RGB565;
    public const GLenum GL_DEPTH_COMPONENT16;
    public const GLenum GL_STENCIL_INDEX8;
    public const GLenum GL_RENDERBUFFER_WIDTH;
    public const GLenum GL_RENDERBUFFER_HEIGHT;
    public const GLenum GL_RENDERBUFFER_INTERNAL_FORMAT;
    public const GLenum GL_RENDERBUFFER_RED_SIZE;
    public const GLenum GL_RENDERBUFFER_GREEN_SIZE;
    public const GLenum GL_RENDERBUFFER_BLUE_SIZE;
    public const GLenum GL_RENDERBUFFER_ALPHA_SIZE;
    public const GLenum GL_RENDERBUFFER_DEPTH_SIZE;
    public const GLenum GL_RENDERBUFFER_STENCIL_SIZE;
    public const GLenum GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE;
    public const GLenum GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME;
    public const GLenum GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL;
    public const GLenum GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE;
    public const GLenum GL_COLOR_ATTACHMENT0;
    public const GLenum GL_DEPTH_ATTACHMENT;
    public const GLenum GL_STENCIL_ATTACHMENT;
    public const GLenum GL_NONE;
    public const GLenum GL_FRAMEBUFFER_COMPLETE;
    public const GLenum GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT;
    public const GLenum GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT;
    public const GLenum GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS;
    public const GLenum GL_FRAMEBUFFER_UNSUPPORTED;
    public const GLenum GL_FRAMEBUFFER_BINDING;
    public const GLenum GL_RENDERBUFFER_BINDING;
    public const GLenum GL_MAX_RENDERBUFFER_SIZE;
    public const GLenum GL_INVALID_FRAMEBUFFER_OPERATION;


    /*************************************************************/
    // GL core functions
    [CCode (cname = "glActiveTexture")]
    public static void raw_glActiveTexture (GLenum texture);
    [CCode (cname = "glAttachShader")]
    public static void raw_glAttachShader (GLuint program, GLuint shader);
    [CCode (cname = "glBindAttribLocation")]
    public static void raw_glBindAttribLocation (GLuint program, GLuint index,
                                             string name);
    [CCode (cname = "glBindBuffer")]
    public static void raw_glBindBuffer (GLenum target, GLuint buffer);
    [CCode (cname = "glBindFramebuffer")]
    public static void raw_glBindFramebuffer (GLenum target, GLuint framebuffer);
    [CCode (cname = "glBindRenderbuffer")]
    public static void raw_glBindRenderbuffer (GLenum target, GLuint renderbuffer);
    [CCode (cname = "glBindTexture")]
    public static void raw_glBindTexture (GLenum target, GLuint texture);
    [CCode (cname = "glBlendColor")]
    public static void raw_glBlendColor (GLclampf red, GLclampf green,
                                     GLclampf blue, GLclampf alpha);
    [CCode (cname = "glBlendEquation")]
    public static void raw_glBlendEquation (GLenum mode);
    [CCode (cname = "glBlendEquationSeparate")]
    public static void raw_glBlendEquationSeparate (GLenum modeRGB,
                                                GLenum modeAlpha);
    [CCode (cname = "glBlendFunc")]
    public static void raw_glBlendFunc (GLenum sfactor, GLenum dfactor);
    [CCode (cname = "glBlendFuncSeparate")]
    public static void raw_glBlendFuncSeparate (GLenum srcRGB, GLenum dstRGB,
                                            GLenum srcAlpha, GLenum dstAlpha);
    [CCode (cname = "glBufferData")]
    public static void raw_glBufferData (GLenum target, GLsizeiptr size,
                                     GLvoid* data, GLenum usage);
    [CCode (cname = "glBufferSubData")]
    public static void raw_glBufferSubData (GLenum target, GLintptr offset,
                                        GLsizeiptr size, GLvoid* data);
    [CCode (cname = "glCheckFramebufferStatus")]
    public static GLenum raw_glCheckFramebufferStatus (GLenum target);
    [CCode (cname = "glClear")]
    public static void raw_glClear (GLbitfield mask);
    [CCode (cname = "glClearColor")]
    public static void raw_glClearColor (GLclampf red, GLclampf green,
                                     GLclampf blue, GLclampf alpha);
    [CCode (cname = "glClearDepthf")]
    public static void raw_glClearDepthf (GLclampf depth);
    [CCode (cname = "glClearStencil")]
    public static void raw_glClearStencil (GLint s);
    [CCode (cname = "glColorMask")]
    public static void raw_glColorMask (GLboolean red, GLboolean green,
                                    GLboolean blue, GLboolean alpha);
    [CCode (cname = "glCompileShader")]
    public static void raw_glCompileShader (GLuint shader);
    [CCode (cname = "glCompressedTexImage2D")]
    public static void raw_glCompressedTexImage2D (GLenum target, GLint level,
                                               GLenum internalformat,
                                               GLsizei width, GLsizei height,
                                               GLint border, GLsizei imageSize,
                                               GLvoid* data);
    [CCode (cname = "glCompressedTexSubImage2D")]
    public static void raw_glCompressedTexSubImage2D (GLenum target, GLint level,
                                                  GLint xoffset, GLint yoffset,
                                                  GLsizei width, GLsizei height,
                                                  GLenum format,
                                                  GLsizei imageSize,
                                                  GLvoid* data);
    [CCode (cname = "glCopyTexImage2D")]
    public static void raw_glCopyTexImage2D (GLenum target, GLint level,
                                         GLenum internalformat, GLint x,
                                         GLint y, GLsizei width, GLsizei height,
                                         GLint border);
    [CCode (cname = "glCopyTexSubImage2D")]
    public static void raw_glCopyTexSubImage2D (GLenum target, GLint level,
                                            GLint xoffset, GLint yoffset,
                                            GLint x, GLint y,
                                            GLsizei width, GLsizei height);
    [CCode (cname = "glCreateProgram")]
    public static GLuint raw_glCreateProgram ( );
    [CCode (cname = "glCreateShader")]
    public static GLuint raw_glCreateShader (GLenum type);
    [CCode (cname = "glCullFace")]
    public static void raw_glCullFace (GLenum mode);
    [CCode (cname = "glDeleteBuffers")]
    public static void raw_glDeleteBuffers ([CCode (array_length_pos = 0.9)]
                                        GLuint[] buffers);
    [CCode (cname = "glDeleteFramebuffers")]
    public static void raw_glDeleteFramebuffers ([CCode (array_length_pos = 0.9)]
                                             GLuint[] framebuffers);

    [CCode (cname = "glDeleteProgram")]
    public static void raw_glDeleteProgram (GLuint program);
    [CCode (cname = "glDeleteRenderbuffers")]
    public static void raw_glDeleteRenderbuffers ([CCode (array_length_pos = 0.9)]
                                              GLuint[] renderbuffers);
    [CCode (cname = "glDeleteShader")]
    public static void raw_glDeleteShader (GLuint shader);
    [CCode (cname = "glDeleteTextures")]
    public static void raw_glDeleteTextures ([CCode (array_length_pos = 0.9)]
                                         GLuint[] textures);
    [CCode (cname = "glDepthFunc")]
    public static void raw_glDepthFunc (GLenum func);
    [CCode (cname = "glDepthMask")]
    public static void raw_glDepthMask (GLboolean flag);
    [CCode (cname = "glDepthRangef")]
    public static void raw_glDepthRangef (GLclampf zNear, GLclampf zFar);
    [CCode (cname = "glDetachShader")]
    public static void raw_glDetachShader (GLuint program, GLuint shader);
    [CCode (cname = "glDisable")]
    public static void raw_glDisable (GLenum cap);
    [CCode (cname = "glDisableVertexAttribArray")]
    public static void raw_glDisableVertexAttribArray (GLuint index);
    [CCode (cname = "glDrawArrays")]
    public static void raw_glDrawArrays (GLenum mode, GLint first, GLsizei count);
    [CCode (cname = "glDrawElements")]
    public static void raw_glDrawElements (GLenum mode, GLsizei count, GLenum type,
                                       GLvoid* indices);
    [CCode (cname = "glEnable")]
    public static void raw_glEnable (GLenum cap);
    [CCode (cname = "glEnableVertexAttribArray")]
    public static void raw_glEnableVertexAttribArray (GLuint index);
    [CCode (cname = "glFinish")]
    public static void raw_glFinish ( );
    [CCode (cname = "glFlush")]
    public static void raw_glFlush ( );
    [CCode (cname = "glFramebufferRenderbuffer")]
    public static void raw_glFramebufferRenderbuffer (GLenum target,
                                                  GLenum attachment,
                                                  GLenum renderbuffertarget,
                                                  GLuint renderbuffer);
    [CCode (cname = "glFramebufferTexture2D")]
    public static void raw_glFramebufferTexture2D (GLenum target, GLenum attachment,
                                               GLenum textarget, GLuint texture,
                                               GLint level);
    [CCode (cname = "glFrontFace")]
    public static void raw_glFrontFace (GLenum mode);
    [CCode (cname = "glGenBuffers")]
    public static void raw_glGenBuffers ([CCode (array_length_pos = 0.9)]
                                     GLuint[] buffers);
    [CCode (cname = "glGenerateMipmap")]
    public static void raw_glGenerateMipmap (GLenum target);
    [CCode (cname = "glGenFramebuffers")]
    public static void raw_glGenFramebuffers ([CCode (array_length_pos = 0.9)]
                                          GLuint[] framebuffers);
    [CCode (cname = "glGenRenderbuffers")]
    public static void raw_glGenRenderbuffers ([CCode (array_length_pos = 0.9)]
                                           GLuint[] renderbuffers);
    [CCode (cname = "glGenTextures")]
    public static void raw_glGenTextures ([CCode (array_length_pos = 0.9)]
                                      GLuint[] textures);
    [CCode (cname = "glGetActiveAttrib")]
    public static void raw_glGetActiveAttrib (GLuint program, GLuint index,
                                          GLsizei bufsize, out GLsizei length,
                                          out GLint size, out GLenum type,
                                          string name);
    [CCode (cname = "glGetActiveUniform")]
    public static void raw_glGetActiveUniform (GLuint program, GLuint index,
                                           GLsizei bufsize, out GLsizei length,
                                           out GLint size, out GLenum type,
                                           string name);
    [CCode (cname = "glGetAttachedShaders")]
    public static void raw_glGetAttachedShaders (GLuint program, GLsizei maxcount,
                                             out GLsizei count,
                                             out GLuint shaders);
    [CCode (cname = "glGetAttribLocation")]
    public static int raw_glGetAttribLocation (GLuint program, string name);
    [CCode (cname = "glGetBooleanv")]
    public static void raw_glGetBooleanv (GLenum pname, [CCode (array_length = false)]
                                      GLboolean[] params);
    [CCode (cname = "glGetBufferParameteriv")]
    public static void raw_glGetBufferParameteriv (GLenum target, GLenum pname,
                                               out GLint params);
    [CCode (cname = "glGetError")]
    public static GLenum raw_glGetError ( );
    [CCode (cname = "glGetFloatv")]
    public static void raw_glGetFloatv (GLenum pname, [CCode (array_length = false)]
                                    GLfloat[] params);
    [CCode (cname = "glGetFramebufferAttachmentParameteriv")]
    public static void raw_glGetFramebufferAttachmentParameteriv (GLenum target,
                                                              GLenum attachment,
                                                              GLenum pname,
                                                              out GLint params);
    [CCode (cname = "glGetIntegerv")]
    public static void raw_glGetIntegerv (GLenum pname, [CCode (array_length = false)]
                                      GLint[] params);
    [CCode (cname = "glGetProgramiv")]
    public static void raw_glGetProgramiv (GLuint program, GLenum pname,
                                       out GLint params);
    [CCode (cname = "glGetProgramInfoLog")]
    public static void raw_glGetProgramInfoLog (GLuint program, GLsizei bufsize,
                                            out GLsizei length,
                                            [CCode (array_length = false)]
                                            GLchar[] infolog);
    [CCode (cname = "glGetRenderbufferParameteriv")]
    public static void raw_glGetRenderbufferParameteriv (GLenum target,
                                                     GLenum pname,
                                                     out GLint params);
    [CCode (cname = "glGetShaderiv")]
    public static void raw_glGetShaderiv (GLuint shader, GLenum pname,
                                      out GLint params);
    [CCode (cname = "glGetShaderInfoLog")]
    public static void raw_glGetShaderInfoLog (GLuint shader, GLsizei bufsize,
                                           out GLsizei length,
                                           [CCode (array_length = false)]
                                           GLchar[] infolog);
    [CCode (cname = "glGetShaderPrecisionFormat")]
    public static void raw_glGetShaderPrecisionFormat (GLenum shadertype,
                                                   GLenum precisiontype,
                                                   out GLint range,
                                                   out GLint precision);
    [CCode (cname = "glGetShaderSource")]
    public static void raw_glGetShaderSource (GLuint shader, GLsizei bufsize,
                                          out GLsizei length, string source);
    [CCode (cname = "glGetString")]
    public static string raw_glGetString (GLenum name);
    [CCode (cname = "glGetTexParameterfv")]
    public static void raw_glGetTexParameterfv (GLenum target, GLenum pname,
                                            out GLfloat params);
    [CCode (cname = "glGetTexParameteriv")]
    public static void raw_glGetTexParameteriv (GLenum target, GLenum pname,
                                            out GLint params);
    [CCode (cname = "glGetUniformfv")]
    public static void raw_glGetUniformfv (GLuint program, GLint location,
                                       [CCode (array_length = false)] GLfloat[]
                                       params);
    [CCode (cname = "glGetUniformiv")]
    public static void raw_glGetUniformiv (GLuint program, GLint location,
                                       [CCode (array_length = false)] GLint[]
                                       params);
    [CCode (cname = "glGetUniformLocation")]
    public static int raw_glGetUniformLocation (GLuint program, string name);
    [CCode (cname = "glGetVertexAttribfv")]
    public static void raw_glGetVertexAttribfv (GLuint index, GLenum pname,
                                            out GLfloat params);
    [CCode (cname = "glGetVertexAttribiv")]
    public static void raw_glGetVertexAttribiv (GLuint index, GLenum pname,
                                            out GLint params);
    [CCode (cname = "glGetVertexAttribPointerv")]
    public static void raw_glGetVertexAttribPointerv (GLuint index, GLenum pname,
                                                  out GLvoid* pointer);
    [CCode (cname = "glHint")]
    public static void raw_glHint (GLenum target, GLenum mode);
    [CCode (cname = "glIsBuffer")]
    public static GLboolean raw_glIsBuffer (GLuint buffer);
    [CCode (cname = "glIsEnabled")]
    public static GLboolean raw_glIsEnabled (GLenum cap);
    [CCode (cname = "glIsFramebuffer")]
    public static GLboolean raw_glIsFramebuffer (GLuint framebuffer);
    [CCode (cname = "glIsProgram")]
    public static GLboolean raw_glIsProgram (GLuint program);
    [CCode (cname = "glIsRenderbuffer")]
    public static GLboolean raw_glIsRenderbuffer (GLuint renderbuffer);
    [CCode (cname = "glIsShader")]
    public static GLboolean raw_glIsShader (GLuint shader);
    [CCode (cname = "glIsTexture")]
    public static GLboolean raw_glIsTexture (GLuint texture);
    [CCode (cname = "glLineWidth")]
    public static void raw_glLineWidth (GLfloat width);
    [CCode (cname = "glLinkProgram")]
    public static void raw_glLinkProgram (GLuint program);
    [CCode (cname = "glPixelStorei")]
    public static void raw_glPixelStorei (GLenum pname, GLint param);
    [CCode (cname = "glPolygonOffset")]
    public static void raw_glPolygonOffset (GLfloat factor, GLfloat units);
    [CCode (cname = "glReadPixels")]
    public static void raw_glReadPixels (GLint x, GLint y, GLsizei width,
                                     GLsizei height, GLenum format, GLenum type,
                                     GLvoid* pixels);
    [CCode (cname = "glReleaseShaderCompiler")]
    public static void raw_glReleaseShaderCompiler ( );
    [CCode (cname = "glRenderbufferStorage")]
    public static void raw_glRenderbufferStorage (GLenum target,
                                              GLenum internalformat,
                                              GLsizei width, GLsizei height);
    [CCode (cname = "glSampleCoverage")]
    public static void raw_glSampleCoverage (GLclampf value, GLboolean invert);
    [CCode (cname = "glScissor")]
    public static void raw_glScissor (GLint x, GLint y, GLsizei width,
                                  GLsizei height);
    [CCode (cname = "glShaderBinary")]
    public static void raw_glShaderBinary (GLsizei n, GLuint* shaders,
                                       GLenum binaryformat, GLvoid* binary,
                                       GLsizei length);
    [CCode (cname = "glShaderSource")]
    public static void raw_glShaderSource (GLuint shader, GLsizei count,
                                       [CCode (type="const GLchar * const*",
                                               array_length = false)]
                                       GLchar* str[],
                                       [CCode (array_length = false)]
                                       GLint[] length);
    [CCode (cname = "glStencilFunc")]
    public static void raw_glStencilFunc (GLenum func, GLint refer, GLuint mask);
    [CCode (cname = "glStencilFuncSeparate")]
    public static void raw_glStencilFuncSeparate (GLenum face, GLenum func,
                                              GLint ref, GLuint mask);
    [CCode (cname = "glStencilMask")]
    public static void raw_glStencilMask (GLuint mask);
    [CCode (cname = "glStencilMaskSeparate")]
    public static void raw_glStencilMaskSeparate (GLenum face, GLuint mask);
    [CCode (cname = "glStencilOp")]
    public static void raw_glStencilOp (GLenum fail, GLenum zfail, GLenum zpass);
    [CCode (cname = "glStencilOpSeparate")]
    public static void raw_glStencilOpSeparate (GLenum face, GLenum fail,
                                            GLenum zfail, GLenum zpass);
    [CCode (cname = "glTexImage2D")]
    public static void raw_glTexImage2D (GLenum target, GLint level,
                                     GLint internalformat, GLsizei width,
                                     GLsizei height, GLint border,
                                     GLenum format, GLenum type, GLvoid* data);
    [CCode (cname = "glTexParameterf")]
    public static void raw_glTexParameterf (GLenum target, GLenum pname,
                                        GLfloat param);
    [CCode (cname = "glTexParameterfv")]
    public static void raw_glTexParameterfv (GLenum target, GLenum pname,
                                         [CCode (array_length = false)]
                                         GLfloat[] params);
    [CCode (cname = "glTexParameteri")]
    public static void raw_glTexParameteri (GLenum target, GLenum pname,
                                        GLint param);
    [CCode (cname = "glTexParameteriv")]
    public static void raw_glTexParameteriv (GLenum target, GLenum pname,
                                         [CCode (array_length = false)]
                                         GLint[] params);
    [CCode (cname = "glTexSubImage2D")]
    public static void raw_glTexSubImage2D (GLenum target, GLint level,
                                        GLint xoffset, GLint yoffset,
                                        GLsizei width, GLsizei height,
                                        GLenum format, GLenum type,
                                        GLvoid* data);
    [CCode (cname = "glUniform1f")]
    public static void raw_glUniform1f (GLint location, GLfloat x);
    [CCode (cname = "glUniform1fv")]
    public static void raw_glUniform1fv (GLint location, GLsizei count,
                                     GLfloat* v);
    [CCode (cname = "glUniform1i")]
    public static void raw_glUniform1i (GLint location, GLint x);
    [CCode (cname = "glUniform1iv")]
    public static void raw_glUniform1iv (GLint location, GLsizei count, GLint* v);
    [CCode (cname = "glUniform2f")]
    public static void raw_glUniform2f (GLint location, GLfloat x, GLfloat y);
    [CCode (cname = "glUniform2fv")]
    public static void raw_glUniform2fv (GLint location, GLsizei count, GLfloat* v);
    [CCode (cname = "glUniform2i")]
    public static void raw_glUniform2i (GLint location, GLint x, GLint y);
    [CCode (cname = "glUniform2iv")]
    public static void raw_glUniform2iv (GLint location, GLsizei count, GLint* v);
    [CCode (cname = "glUniform3f")]
    public static void raw_glUniform3f (GLint location, GLfloat x, GLfloat y,
                                    GLfloat z);
    [CCode (cname = "glUniform3fv")]
    public static void raw_glUniform3fv (GLint location, GLsizei count,
                                     GLfloat* v);
    [CCode (cname = "glUniform3i")]
    public static void raw_glUniform3i (GLint location, GLint x, GLint y, GLint z);
    [CCode (cname = "glUniform3iv")]
    public static void raw_glUniform3iv (GLint location, GLsizei count, GLint* v);
    [CCode (cname = "glUniform4f")]
    public static void raw_glUniform4f (GLint location, GLfloat x, GLfloat y,
                                    GLfloat z, GLfloat w);
    [CCode (cname = "glUniform4fv")]
    public static void raw_glUniform4fv (GLint location, GLsizei count, GLfloat* v);
    [CCode (cname = "glUniform4i")]
    public static void raw_glUniform4i (GLint location, GLint x, GLint y, GLint z,
                                    GLint w);
    [CCode (cname = "glUniform4iv")]
    public static void raw_glUniform4iv (GLint location, GLsizei count, GLint* v);
    [CCode (cname = "glUniformMatrix2fv")]
    public static void raw_glUniformMatrix2fv (GLint location, GLsizei count,
                                           GLboolean transpose, GLfloat* v);
    [CCode (cname = "glUniformMatrix3fv")]
    public static void raw_glUniformMatrix3fv (GLint location, GLsizei count,
                                           GLboolean transpose, GLfloat* v);
    [CCode (cname = "glUniformMatrix4fv")]
    public static void raw_glUniformMatrix4fv (GLint location, GLsizei count,
                                           GLboolean transpose, GLfloat* v);
    [CCode (cname = "glUseProgram")]
    public static void raw_glUseProgram (GLuint program);
    [CCode (cname = "glValidateProgram")]
    public static void raw_glValidateProgram (GLuint program);
    [CCode (cname = "glVertexAttrib1f")]
    public static void raw_glVertexAttrib1f (GLuint indx, GLfloat x);
    [CCode (cname = "glVertexAttrib1fv")]
    public static void raw_glVertexAttrib1fv (GLuint indx, GLfloat* values);
    [CCode (cname = "glVertexAttrib2f")]
    public static void raw_glVertexAttrib2f (GLuint indx, GLfloat x, GLfloat y);
    [CCode (cname = "glVertexAttrib2fv")]
    public static void raw_glVertexAttrib2fv (GLuint indx, GLfloat* values);
    [CCode (cname = "glVertexAttrib3f")]
    public static void raw_glVertexAttrib3f (GLuint indx, GLfloat x, GLfloat y,
                                         GLfloat z);
    [CCode (cname = "glVertexAttrib3fv")]
    public static void raw_glVertexAttrib3fv (GLuint indx, GLfloat* values);
    [CCode (cname = "glVertexAttrib4f")]
    public static void raw_glVertexAttrib4f (GLuint indx, GLfloat x, GLfloat y,
                                         GLfloat z, GLfloat w);
    [CCode (cname = "glVertexAttrib4fv")]
    public static void raw_glVertexAttrib4fv (GLuint indx, GLfloat* values);
    [CCode (cname = "glVertexAttribPointer")]
    public static void raw_glVertexAttribPointer (GLuint indx, GLint size,
                                              GLenum type, GLboolean normalized,
                                              GLsizei stride, GLvoid* ptr);
    [CCode (cname = "glViewport")]
    public static void raw_glViewport (GLint x, GLint y, GLsizei width,
                                   GLsizei height);

    /*************************************************************/
    // GL extensions

    // GL_EXT_texture_storage
    // (with GL_OES_texture_float only)

    public const GLenum GL_TEXTURE_IMMUTABLE_FORMAT_EXT;
    public const GLenum GL_ALPHA8_EXT;
    public const GLenum GL_LUMINANCE8_EXT;
    public const GLenum GL_LUMINANCE8_ALPHA8_EXT;
    public const GLenum GL_RGBA32F_EXT;
    public const GLenum GL_RGB32F_EXT;
    public const GLenum GL_ALPHA32F_EXT;
    public const GLenum GL_LUMINANCE32F_EXT;
    public const GLenum GL_LUMINANCE_ALPHA32F_EXT;

    public static void TexStorage1DEXT (GLenum target, GLsizei levels,
                                        GLenum internalformat,
                                        GLsizei width);
    public static void TexStorage2DEXT (GLenum target, GLsizei levels,
                                        GLenum internalformat,
                                        GLsizei width, GLsizei height);
    public static void TexStorage3DEXT (GLenum target, GLsizei levels,
                                        GLenum internalformat,
                                        GLsizei width, GLsizei height,
                                        GLsizei depth);
    public static void TextureStorage1DEXT (GLuint texture, GLenum target,
                                            GLsizei levels,
                                            GLenum internalformat,
                                            GLsizei width);
    public static void TextureStorage2DEXT (GLuint texture, GLenum target,
                                            GLsizei levels,
                                            GLenum internalformat,
                                            GLsizei width, GLsizei height);
    public static void TextureStorage3DEXT (GLuint texture, GLenum target,
                                            GLsizei levels,
                                            GLenum internalformat,
                                            GLsizei width, GLsizei height,
                                            GLsizei depth);

    public delegate void PFNGLTEXSTORAGE1DEXTPROC (GLenum target,
                                                   GLsizei levels,
                                                   GLenum internalformat,
                                                   GLsizei width);
    public delegate void PFNGLTEXSTORAGE2DEXTPROC (GLenum target,
                                                   GLsizei levels,
                                                   GLenum internalformat,
                                                   GLsizei width,
                                                   GLsizei height);
    public delegate void PFNGLTEXSTORAGE3DEXTPROC (GLenum target,
                                                   GLsizei levels,
                                                   GLenum internalformat,
                                                   GLsizei width,
                                                   GLsizei height,
                                                   GLsizei depth);
    public delegate void PFNGLTEXTURESTORAGE1DEXTPROC (GLuint texture,
                                                       GLenum target,
                                                       GLsizei levels,
                                                       GLenum internalformat,
                                                       GLsizei width);
    public delegate void PFNGLTEXTURESTORAGE2DEXTPROC (GLuint texture,
                                                       GLenum target,
                                                       GLsizei levels,
                                                       GLenum internalformat,
                                                       GLsizei width,
                                                       GLsizei height);
    public delegate void PFNGLTEXTURESTORAGE3DEXTPROC (GLuint texture,
                                                       GLenum target,
                                                       GLsizei levels,
                                                       GLenum internalformat,
                                                       GLsizei width,
                                                       GLsizei height,
                                                       GLsizei depth);

}
