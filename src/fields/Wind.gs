/*
 *  libsoy - soy.fields.Wind
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GLib
    ode

class soy.fields.Wind : soy.fields.Field
    /*
    soy.fields.Wind

    Affects bodies according to the shape, size, orientation and relative velocities
    in x,z and z direction independently
    */

    //Properties of wind.
    // getter and setter defined below
    _wind_vect : soy.atoms.Vector
    _mass_density : float

    construct(density: float, wind : soy.atoms.Vector)
        self._mass_density = density
        self._wind_vect = wind




    // This function is applied once per physics cycle for each combination of affected body and field.
    // Check the step function in scene.gs for it call.
    def override exert(other : soy.bodies.Body) : bool
        if !inside_region(other)
            return false
        var dx = self._wind_vect.x - other.velocity.x
        var dy = self._wind_vect.y - other.velocity.y
        var dz = self._wind_vect.z - other.velocity.z
        var d2 = dx*dx+dy*dy+dz*dz
        //force is zero. Return
        if d2 == 0
            return true
        direction : soy.atoms.Vector = new soy.atoms.Vector(dx,dy,dz)
        direction.normalize();
        mass : float = other.volume()*other.density
        cross_sectional_area : float =  other.getCrossSectionalArea(
                                               new soy.atoms.Vector(
                                                             direction.x,
                                                             direction.y,
                                                             direction.z))

        var f = 0.5f * self._mass_density * d2 * other.drag_cofficient * cross_sectional_area

        //Explicite velocity update if force is too high
        vx,vy,vz : float

        // Checking that the applied force does not reverses the direction of body
        if(((direction.x*f/mass)*0.01+other.velocity.x)*other.velocity.x < 0)
            direction.x=0;
            vx=0
        //    print "too much wind force in x"
        else
            vx = other.velocity.x
        if(((direction.y*f/mass)*0.01+other.velocity.y)*other.velocity.y < 0)
            direction.y=0;
            vy=0
        //    print "too much wind force in y"
        else
            vy = other.velocity.y

        if(((direction.z*f/mass)*0.01+other.velocity.z)*other.velocity.z < 0)
            direction.z=0;
            vz=0
        //    print "too much wind force in z"
        else
            vz = other.velocity.z

        //updating velocity of the body
        other.velocity = new soy.atoms.Vector(vx,vy,vz)

        applied_force : soy.atoms.Vector = new soy.atoms.Vector(
                                                         direction.x*f,
                                                         direction.y*f,
                                                         direction.z*f)

        //Finally adding the wind Force :)
        other.addForce(applied_force.x,applied_force.y,applied_force.z)

        //slowing the rotation
        //TODO: Its doesn't depend on shape, its fixed
        // Make it more dynamic
        other.angular_velocity = new soy.atoms.Vector(
                          other.angular_velocity.x*(1.0f-0.0025f*self.density),
                          other.angular_velocity.y*(1.0f-0.0025f*self.density),
                          other.angular_velocity.z*(1.0f-0.0025f*self.density)
                          )
        return true

    //
    // Properties

    // Wind vector
    // Affects relative velocity of body and wind.
    prop wind : soy.atoms.Vector
        owned get
            return _wind_vect
        set
            _wind_vect = value

    // Wind density
    // densityy of the fluid,
    // affects how draw is calculated.
    prop density : float
        get
            return self._mass_density
        set
            self._mass_density = value
