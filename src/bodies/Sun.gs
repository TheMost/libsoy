/*
 *  libsoy - soy.bodies.Sun
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GL
    soy
    GLib.Math

class soy.bodies.Sun : soy.bodies.Light
    construct (timeOfDay : GLfloat)
        super(null, 1.0f, null)
        _timeOfDay = timeOfDay
        self.lightType = DIRECTIONAL_LIGHT_TYPE
        self._directionBasedOnTime(_timeOfDay)

    def _directionBasedOnTime(time : GLfloat)
        sunriseHour: GLfloat = 6
        sunsetHour: GLfloat = 19

        // Night
        if time < sunriseHour or time > sunsetHour
            self.direction = {0.0f, 0.0f, 0.0f}
        else
            // How much hours in the day we have daylight
            totalDaylightHours : GLfloat = sunsetHour - sunriseHour
            // How much degrees we see the sun rotate in the sky
            totalSunDegrees : GLfloat = 180

            dayPercentage : GLfloat = (time - sunriseHour)/totalDaylightHours
            sunAngle : GLfloat = dayPercentage*totalSunDegrees*((GLfloat) PI)/180.0f

            self.direction = {cosf(sunAngle), sinf(sunAngle), 0.0f}
    ////////////////////////////////////////////////////////////////////////
    // Properties

    //
    // timeOfDay Property
    _timeOfDay : GLfloat = 0.0f// Time of day in hours
    prop timeOfDay : float
        get
            return self._timeOfDay
        set
            _timeOfDay = value
            self._directionBasedOnTime(_timeOfDay)
