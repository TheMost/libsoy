/*
 *  libsoy - soy.recording.EncodedCmd.gs
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses GL

class soy.recording.EncodedCmd : Object
    command : GlFunc
    parameters : array of string

    construct (cmd : GlFunc, param : array of string)
        command = cmd
        parameters = new array of string[param.length]
        i : int
        for i=0 to (param.length-1)
            //we have to clone the string this way because vala is awful
            var sb = new StringBuilder()
            sb.append(param[i])
            parameters[i] = sb.str
