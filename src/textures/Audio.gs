/*
 *  libsoy - soy.textures.Audio
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GL

class soy.textures.Audio : soy.textures.Texture
    _alloced : uint
    _mutex : Mutex
    _samples : int16*

    init
        _alloced = 0
        _mutex = Mutex()
        _samples = null
        _size = 0
        translucent = false

    construct ()
        // Generate a simple 1x256 RGB texture
        resize(3, 1, 256)

        // Add a blue line down the center
        texels[383] = 255
        

    ////////////////////////////////////////////////////////////////////////
    // Methods

    def _generate ()
        // TODO generate audio spectrograph
        self.updated = true


    def append (value : int16)
        // Lock against rendering
        self._mutex.lock()

        // Resize buffer if we've already used the maximum size
        if _alloced == _size
            // New 64k buffer for first samples
            if _samples is null
                _alloced = 65536
                _samples = malloc0(65536)
            else
                // Double buffer size if under 16m
                if _alloced < 16777216
                    _alloced *= 2

                // Increase by 16m if already 16m or greater
                else
                    _alloced += 16777216

                // Reallocate sample storage for larger size
                _samples = realloc(_samples, _alloced) // FIXME *2 for int16?

        // Store new sample and regenerate
        _samples[_size] = value
        _size += 1
        _generate()

        // Unlock before return
        self._mutex.unlock()


    def new get (index : int) : int16
        // Return -1 if requested index is out of bounds
        if index < 0 or index >= _size
            return -1

        return _samples[index]


    def new set (index : int, value : int16)
        _samples[index] = value

        // Regenerate visual
        self._mutex.lock()
        _generate()
        self._mutex.unlock()


    ////////////////////////////////////////////////////////////////////////
    // Properties

    prop new readonly size : uint
