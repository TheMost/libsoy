/*
 *  libsoy - soy.scenes.Room
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GLib
    GL
    Gee
    ode
    GLib.Math

class soy.scenes.Room : soy.scenes.Scene
    //arrays to store geoms for each wall of room
    wall1 : array of ode.geoms.Box*
    wall2 : array of ode.geoms.Box*
    wall3 : array of ode.geoms.Box*
    wall4 : array of ode.geoms.Box*
    wall5 : array of ode.geoms.Box*
    wall6 : array of ode.geoms.Box*
    numgeoms : array of int
    _ebo : GLuint   // Element Buffer Object
    _vbo : GLuint   // Vertex Buffer Object
    _updated : bool // Buffers need updating
    _total_geoms : int
    //epsilon to improve rendering when walls overlap
    eps : float = 0.01f

    ////////////////////////////////////////////////////////////////////////
    // Properties

    _material : soy.materials.Material
    prop material : soy.materials.Material?
        get
            return _material
        set
            self._material = value

    _size : soy.atoms.Size
    prop readonly size : soy.atoms.Size
        get
            return _size

    _wall_width : float
    prop readonly wall_width : float
        get
            return _wall_width

    init
        wall1 = new array of ode.geoms.Box*[10]
        wall2 = new array of ode.geoms.Box*[10]
        wall3 = new array of ode.geoms.Box*[10]
        wall4 = new array of ode.geoms.Box*[10]
        wall5 = new array of ode.geoms.Box*[10]
        wall6 = new array of ode.geoms.Box*[10]
        numgeoms = new array of int[6]
    
    
    construct (size : soy.atoms.Size, wall_width : float, x : float, y : float, z : float)

        super()
        _wall_width = wall_width
        position.x = x
        position.y = y
        position.z = z
        _ebo = 0

        //order of walls : front, right, back, left, bottom, top
        wall1[0] = new ode.geoms.Box(super.space, size.width+_wall_width, 
                            size.height+_wall_width, _wall_width)
        wall2[0] = new ode.geoms.Box(super.space, _wall_width, 
                            size.height+_wall_width, size.depth+_wall_width)
        wall3[0] = new ode.geoms.Box(super.space, size.width+_wall_width, 
                            size.height+_wall_width, _wall_width)
        wall4[0] = new ode.geoms.Box(super.space, _wall_width, 
                            size.height+_wall_width, size.depth+_wall_width)
        wall5[0] = new ode.geoms.Box(super.space, size.width+_wall_width, 
                            _wall_width, size.depth+_wall_width)
        wall6[0] = new ode.geoms.Box(super.space, size.width+_wall_width, 
                            _wall_width, size.depth+_wall_width)

        wall1[0]->SetPosition(position.x+0.0f, position.y+0.0f, 
                            position.z+size.depth/2.0f-eps)
        wall2[0]->SetPosition(position.x+size.width/2.0f-eps, position.y+0.0f, 
                            position.z+0.0f)
        wall3[0]->SetPosition(position.x+0.0f, position.y+0.0f, 
                            position.z-size.depth/2.0f+eps)
        wall4[0]->SetPosition(position.x-size.width/2.0f+eps, position.y+0.0f, 
                            position.z+0.0f)
        wall5[0]->SetPosition(position.x+0.0f, position.y-size.height/2+eps, 
                            position.z+0.0f)
        wall6[0]->SetPosition(position.x+0.0f, position.y+size.height/2-eps, 
                            position.z+0.0f)

        wall1[0]->SetCategoryBits(1)
        wall2[0]->SetCategoryBits(1)
        wall3[0]->SetCategoryBits(1)
        wall4[0]->SetCategoryBits(1)
        wall5[0]->SetCategoryBits(1)
        wall6[0]->SetCategoryBits(1)

        for var i = 0 to 5
         numgeoms[i] = 1

        _size = size
        _updated = true

    ////////////////////////////////////////////////////////////////////////
    // Methods

    def override is_point_in_scene (pos : Vector3) : bool
        if (pos.x <= position.x+(_size.width/2)) and (
                                    pos.x >= position.x-(_size.width/2)) and (
                                    pos.y <= position.y+(_size.height/2)) and (
                                    pos.y >= position.y-(_size.height/2)) and (
                                    pos.z <= position.z+(_size.depth/2)) and (
                                    pos.z >= position.z-(_size.depth/2))
            return true
        else
            return false

    def override render_extra (view : array of GLfloat, projection : array of 
                               GLfloat, camera : soy.bodies.Camera, lights : 
                               array of soy.bodies.Light)

        if _material is null
            return
        // modelview matrix 
        model_view : array of GLfloat[16] = view 

        if _updated 
            _update_room() 

        // rebind buffers when not updating 
        else 
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo) 
            glBindBuffer(GL_ARRAY_BUFFER, _vbo) 

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, (GLsizei) 
                                (sizeof(GLfloat) * 11), null) 
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, (GLsizei) 
                                (sizeof(GLfloat) * 11), (GLvoid*) 
                                (sizeof(GLfloat) * 3)) 
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, (GLsizei) 
                                (sizeof(GLfloat) * 11), (GLvoid*) 
                                (sizeof(GLfloat) * 6)) 
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, (GLsizei) 
                                (sizeof(GLfloat) * 11), (GLvoid*)
                                (sizeof(GLfloat) * 8))
        
        glEnableVertexAttribArray(0) 
        glEnableVertexAttribArray(1) 
        glEnableVertexAttribArray(2) 
        glEnableVertexAttribArray(3) 

        i : int = 0 
        while self._material.enable(i, model_view, view, projection, lights, 
                                    ambient.get4f(), fog.get4f()) 
            glDrawElements(GL_TRIANGLES, (GLsizei) 36 * _total_geoms, 
                                    GL_UNSIGNED_SHORT, (GLvoid*) 0) 
            i++ 
        glDisableVertexAttribArray(0) 
        glDisableVertexAttribArray(1) 
        glDisableVertexAttribArray(2) 
        glDisableVertexAttribArray(3) 
        self._material.disable()



    def _update_room() 
        // on the first pass
        if _ebo is 0
            buffers : array of GLuint = {0,0}
            glGenBuffers(buffers)
            _ebo = buffers[0]
            _vbo = buffers[1]

        total_geoms : int = 0
        for var i = 0 to 5
            total_geoms+=numgeoms[i];

        _total_geoms = total_geoms
        elements : array of GLushort = new array of GLushort[36*total_geoms]
        vertices : array of GLfloat = new array of GLfloat[264*total_geoms]

        eoffset : int = 0
        voffset : int = 0
        evalue : GLushort = 0

        //add to vertices and elements for wall1
        for var i = 0 to (numgeoms[0]-1)
            _add_to_vertices(wall1[i], vertices, ref voffset)
            for var j = 0 to 5
                elements[eoffset] = evalue
                elements[eoffset+1] = evalue+1
                elements[eoffset+2] = evalue+2
                elements[eoffset+3] = evalue+2
                elements[eoffset+4] = evalue+1
                elements[eoffset+5] = evalue+3
                eoffset+=6
                evalue+=4
        

        //add to vertices and elements for wall2
        for var i = 0 to (numgeoms[1]-1)
            _add_to_vertices(wall2[i], vertices, ref voffset)
            for var j = 0 to 5
                elements[eoffset] = evalue
                elements[eoffset+1] = evalue+1
                elements[eoffset+2] = evalue+2
                elements[eoffset+3] = evalue+2
                elements[eoffset+4] = evalue+1
                elements[eoffset+5] = evalue+3
                eoffset+=6
                evalue+=4

        //add to vertices and elements for wall3
        for var i = 0 to (numgeoms[2]-1)
            _add_to_vertices(wall3[i], vertices, ref voffset)
            for var j = 0 to 5
                elements[eoffset] = evalue
                elements[eoffset+1] = evalue+1
                elements[eoffset+2] = evalue+2
                elements[eoffset+3] = evalue+2
                elements[eoffset+4] = evalue+1
                elements[eoffset+5] = evalue+3
                eoffset+=6
                evalue+=4

        //add to vertices and elements for wall4
        for var i = 0 to (numgeoms[3]-1)
            _add_to_vertices(wall4[i], vertices, ref voffset)
            for var j = 0 to 5
                elements[eoffset] = evalue
                elements[eoffset+1] = evalue+1
                elements[eoffset+2] = evalue+2
                elements[eoffset+3] = evalue+2
                elements[eoffset+4] = evalue+1
                elements[eoffset+5] = evalue+3
                eoffset+=6
                evalue+=4

        //add to vertices and elements for wall5
        for var i = 0 to (numgeoms[4]-1)
            _add_to_vertices(wall5[i], vertices, ref voffset)
            for var j = 0 to 5
                elements[eoffset] = evalue
                elements[eoffset+1] = evalue+1
                elements[eoffset+2] = evalue+2
                elements[eoffset+3] = evalue+2
                elements[eoffset+4] = evalue+1
                elements[eoffset+5] = evalue+3
                eoffset+=6
                evalue+=4

        //add to vertices and elements for wall6
        for var i = 0 to (numgeoms[5]-1)
            _add_to_vertices(wall6[i], vertices, ref voffset)
            for var j = 0 to 5
                elements[eoffset] = evalue
                elements[eoffset+1] = evalue+1
                elements[eoffset+2] = evalue+2
                elements[eoffset+3] = evalue+2
                elements[eoffset+4] = evalue+1
                elements[eoffset+5] = evalue+3
                eoffset+=6
                evalue+=4

        // bind elements 
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo) 
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizei) (36 * total_geoms * (
                                sizeof(GLushort))), elements, GL_STATIC_DRAW)

        // bind vertices 
        glBindBuffer(GL_ARRAY_BUFFER, _vbo) 
        glBufferData(GL_ARRAY_BUFFER, (GLsizei) (264 * total_geoms * (
                                sizeof(GLfloat))), vertices, GL_STATIC_DRAW) 

        // Reset updated flag 
        _updated = false



    def _add_to_vertices(box : ode.geoms.Box*, vertices : array of GLfloat, 
                        ref voffset : int)
        lengths : Vector3 = new Vector3()
        box->GetLengths(lengths)
        geompos : weak Vector3 = box->GetPosition()

        //back face        
        for var i = 0 to 1
            for var j = 0 to 1
                //position
                vertices[voffset] = (GLfloat)(geompos.x+(2*i-1)*(
                                        lengths.x/2.0f))
                vertices[voffset+1] = (GLfloat)(geompos.y+(2*j-1)*(
                                        lengths.y/2.0f))
                vertices[voffset+2] = (GLfloat)(geompos.z-(lengths.z/2.0f))
                //normal
                vertices[voffset+3] = 0.0f
                vertices[voffset+4] = 0.0f
                vertices[voffset+5] = -1.0f
                //uv
                vertices[voffset+6] = 1.0f-j
                vertices[voffset+7] = i
                //tangent
                vertices[voffset+8] = -1.0f
                vertices[voffset+9] = 0.0f
                vertices[voffset+10] = 0.0f
                voffset+=11

                

        //front face        
        for var i = 0 to 1
            for var j = 0 to 1
                //position
                vertices[voffset] = (GLfloat)(geompos.x+(2*j-1)*(
                                            lengths.x/2.0f))
                vertices[voffset+1] = (GLfloat)(geompos.y+(2*i-1)*(
                                            lengths.y/2.0f))
                vertices[voffset+2] = (GLfloat)(geompos.z+(lengths.z/2.0f))
                //normal
                vertices[voffset+3] = 0.0f
                vertices[voffset+4] = 0.0f
                vertices[voffset+5] = 1.0f
                //uv
                vertices[voffset+6] = j
                vertices[voffset+7] = i
                //tangent
                vertices[voffset+8] = 1.0f
                vertices[voffset+9] = 0.0f
                vertices[voffset+10] = 0.0f
                voffset+=11

        //left face        
        for var i = 0 to 1
            for var j = 0 to 1
                //position
                vertices[voffset] = (GLfloat)(geompos.x-(lengths.x/2.0f))
                vertices[voffset+1] = (GLfloat)(geompos.y+(2*i-1)*(
                                            lengths.y/2.0f))
                vertices[voffset+2] = (GLfloat)(geompos.z+(2*j-1)*(
                                            lengths.z/2.0f))
                //normal
                vertices[voffset+3] = -1.0f
                vertices[voffset+4] = 0.0f
                vertices[voffset+5] = 0.0f
                //uv
                vertices[voffset+6] = j
                vertices[voffset+7] = i
                //tangent
                vertices[voffset+8] = 0.0f
                vertices[voffset+9] = 0.0f
                vertices[voffset+10] = 1.0f
                voffset+=11

        //right face        
        for var i = 0 to 1
            for var j = 0 to 1
                //position
                vertices[voffset] = (GLfloat)(geompos.x+(lengths.x/2.0f))
                vertices[voffset+1] = (GLfloat)(geompos.y+(2*j-1)*(
                                            lengths.y/2.0f))
                vertices[voffset+2] = (GLfloat)(geompos.z+(2*i-1)*(
                                            lengths.z/2.0f))
                //normal
                vertices[voffset+3] = 1.0f
                vertices[voffset+4] = 0.0f
                vertices[voffset+5] = 0.0f
                //uv
                vertices[voffset+6] = 1.0f-i
                vertices[voffset+7] = j
                //tangent
                vertices[voffset+8] = 0.0f
                vertices[voffset+9] = 0.0f
                vertices[voffset+10] = -1.0f
                voffset+=11

        //bottom face        
        for var i = 0 to 1
            for var j = 0 to 1
                //position
                vertices[voffset] = (GLfloat)(geompos.x+(2*j-1)*(
                                                lengths.x/2.0f))
                vertices[voffset+1] = (GLfloat)(geompos.y-(lengths.y/2.0f))
                vertices[voffset+2] = (GLfloat)(geompos.z+(2*i-1)*(
                                                lengths.z/2.0f))
                //normal
                vertices[voffset+3] = 0.0f
                vertices[voffset+4] = -1.0f
                vertices[voffset+5] = 0.0f
                //uv
                vertices[voffset+6] = j
                vertices[voffset+7] = i
                //tangent
                vertices[voffset+8] = 1.0f
                vertices[voffset+9] = 0.0f
                vertices[voffset+10] = 0.0f
                voffset+=11

        //top face        
        for var i = 0 to 1
            for var j = 0 to 1
                //position
                vertices[voffset] = (GLfloat)(geompos.x+(2*i-1)*(
                                            lengths.x/2.0f))
                vertices[voffset+1] = (GLfloat)(geompos.y+(lengths.y/2.0f))
                vertices[voffset+2] = (GLfloat)(geompos.z+(2*j-1)*(
                                            lengths.z/2.0f))
                //normal
                vertices[voffset+3] = 0.0f
                vertices[voffset+4] = 1.0f
                vertices[voffset+5] = 0.0f
                //uv
                vertices[voffset+6] = 0.0f
                vertices[voffset+7] = 1.0f-j
                //tangent
                vertices[voffset+8] = 1.0f
                vertices[voffset+9] = 0.0f
                vertices[voffset+10] = 0.0f
                voffset+=11

    //order of walls : front, right, back, left, bottom, top    
    def add_door(wallid : int, x1 : float, y1 : float, x2 : float, y2 : float)
        if x1 > x2
            var tempint = x1
            x1 = x2
            x2 = tempint
        if y1 > y2
            var tempint = y1
            y1 = y2
            y2 = tempint
        
        case wallid
            //wall1 : front wall
            when 1 
                if x1 < 0
                    x1 = 0
                if y1 < 0
                    y1 = 0
                if x2 > _size.width
                    x2 = _size.width
                if y2 > _size.height
                    y2 = _size.height
                //get actual values of x1,x2,y1,y2
                x1 = position.x - (_size.width/2.0f) + x1
                x2 = position.x - (_size.width/2.0f) + x2
                y1 = position.y - (_size.height/2.0f)+ y1
                y2 = position.y - (_size.height/2.0f)+ y2

                for var i = 0 to (numgeoms[0]-1)
                    //for each geom, check if door intersects with the geom 
                    //or not
                    lengths : Vector3 = new Vector3();
                    wall1[i]->GetLengths(lengths)
                    geompos : weak Vector3 = wall1[i]->GetPosition()
                    geomx1 : float = (float)(geompos.x - (lengths.x/2.0f))
                    geomx2 : float = (float)(geompos.x + (lengths.x/2.0f))
                    geomy1 : float = (float)(geompos.y - (lengths.y/2.0f))
                    geomy2 : float = (float)(geompos.y + (lengths.y/2.0f))

                    //if geom lies within the door
                    if (x1 <= geomx1) and (y1 <= geomy1) and (x2 >= geomx2
                                    ) and (y2 >= geomy2)
                        delete wall1[i]
                        for var j = i to (numgeoms[0]-2)
                            wall1[j] = wall1[j+1];
                        numgeoms[0] = numgeoms[0] - 1
                        //recheck current i since array has been shifted
                        i = i -1


                    //if the geom and door intersect
                    else if (x1<geomx2) and (x2>geomx1) and (y1<geomy2) and (
                                    y2>geomy1)
                        tempx1 : float = x1
                        tempx2 : float = x2
                        tempy1 : float = y1
                        tempy2 : float = y2
                        //reduce door to cover only the geom
                        if x1<geomx1
                            tempx1=geomx1
                        if y1<geomy1
                            tempy1=geomy1
                        if x2>geomx2
                            tempx2=geomx2
                        if y2>geomy2
                            tempy2=geomy2

                        //add geom to left if needed
                        if tempx1>geomx1
                            wall1[numgeoms[0]] = new ode.geoms.Box(super.space,
                                        tempx1-geomx1, lengths.y, _wall_width)
                            wall1[numgeoms[0]]->SetPosition(
                                        (geomx1+tempx1)/2.0f, geompos.y, 
                                        geompos.z)
                            wall1[numgeoms[0]]->SetCategoryBits(1)
                            numgeoms[0]=numgeoms[0]+1
                        
                        //add geom to the right if needed
                        if tempx2<geomx2
                            wall1[numgeoms[0]] = new ode.geoms.Box(super.space,
                                        geomx2-tempx2, lengths.y, _wall_width)
                            wall1[numgeoms[0]]->SetPosition(
                                        (geomx2+tempx2)/2.0f, geompos.y, 
                                        geompos.z)
                            wall1[numgeoms[0]]->SetCategoryBits(1)
                            numgeoms[0]=numgeoms[0]+1

                        //add geom below if needed
                        if tempy1>geomy1
                            wall1[numgeoms[0]] = new ode.geoms.Box(super.space,
                                        tempx2-tempx1, tempy1-geomy1, 
                                        _wall_width)
                            wall1[numgeoms[0]]->SetPosition(
                                        (tempx1+tempx2)/2.0f, 
                                        (tempy1+geomy1)/2.0f, geompos.z)
                            wall1[numgeoms[0]]->SetCategoryBits(1)
                            numgeoms[0]=numgeoms[0]+1

                        //add geom above if needed
                        if tempy2<geomy2
                            wall1[numgeoms[0]] = new ode.geoms.Box(super.space,
                                        tempx2-tempx1, geomy2-tempy2, 
                                        _wall_width)
                            wall1[numgeoms[0]]->SetPosition(
                                        (tempx1+tempx2)/2.0f, 
                                        (tempy2+geomy2)/2.0f, geompos.z)
                            wall1[numgeoms[0]]->SetCategoryBits(1)
                            numgeoms[0]=numgeoms[0]+1

                        //delete geom and shift geom array
                        delete wall1[i]
                        for var j = i to (numgeoms[0]-2)
                            wall1[j] = wall1[j+1];
                        numgeoms[0] = numgeoms[0] - 1
                        //recheck current i since array has been shifted
                        i = i -1

            //wall2 : right wall
            when 2
                if x1 < 0
                    x1 = 0
                if y1 < 0
                    y1 = 0
                if x2 > _size.depth
                    x2 = _size.depth
                if y2 > _size.height
                    y2 = _size.height
                //get actual values of x1,x2,y1,y2
                x1 = position.z + (_size.depth/2.0f) - x1
                x2 = position.z + (_size.depth/2.0f) - x2
                y1 = position.y - (_size.height/2.0f)+ y1
                y2 = position.y - (_size.height/2.0f)+ y2

                for var i = 0 to (numgeoms[1]-1)
                    //for each geom, check if door intersects with the geom 
                    //or not
                    lengths : Vector3 = new Vector3();
                    wall2[i]->GetLengths(lengths)
                    geompos : weak Vector3 = wall2[i]->GetPosition()
                    geomx1 : float = (float)(geompos.z + (lengths.z/2.0f))
                    geomx2 : float = (float)(geompos.z - (lengths.z/2.0f))
                    geomy1 : float = (float)(geompos.y - (lengths.y/2.0f))
                    geomy2 : float = (float)(geompos.y + (lengths.y/2.0f))

                    //if geom lies within the door
                    if (x1 >= geomx1) and (y1 <= geomy1) and (
                                x2 <= geomx2) and (y2 >= geomy2)
                        delete wall2[i]
                        for var j = i to (numgeoms[1]-2)
                            wall2[j] = wall2[j+1];
                        numgeoms[1] = numgeoms[1] - 1
                        //recheck current i since array has been shifted
                        i = i - 1


                    //if the geom and door intersect
                    else if (x1>geomx2) and (x2<geomx1) and (y1<geomy2) and (
                                y2>geomy1)
                        tempx1 : float = x1
                        tempx2 : float = x2
                        tempy1 : float = y1
                        tempy2 : float = y2
                        //reduce door to cover only the geom
                        if x1>geomx1
                            tempx1=geomx1
                        if y1<geomy1
                            tempy1=geomy1
                        if x2<geomx2
                            tempx2=geomx2
                        if y2>geomy2
                            tempy2=geomy2

                        //add geom to left if needed
                        if tempx1<geomx1
                            wall2[numgeoms[1]] = new ode.geoms.Box(super.space,
                                        _wall_width, lengths.y, geomx1-tempx1)
                            wall2[numgeoms[1]]->SetPosition(geompos.x, 
                                        geompos.y, (geomx1+tempx1)/2.0f)
                            wall2[numgeoms[1]]->SetCategoryBits(1)
                            numgeoms[1]=numgeoms[1]+1
                        
                        //add geom to the right if needed
                        if tempx2>geomx2
                            wall2[numgeoms[1]] = new ode.geoms.Box(super.space,
                                        _wall_width, lengths.y, tempx2-geomx2)
                            wall2[numgeoms[1]]->SetPosition(geompos.x, 
                                        geompos.y, (geomx2+tempx2)/2.0f)
                            wall2[numgeoms[1]]->SetCategoryBits(1)
                            numgeoms[1]=numgeoms[1]+1

                        //add geom below if needed
                        if tempy1>geomy1
                            wall2[numgeoms[1]] = new ode.geoms.Box(super.space,
                                        _wall_width, tempy1-geomy1, 
                                        tempx1-tempx2)
                            wall2[numgeoms[1]]->SetPosition(geompos.x, 
                                        (tempy1+geomy1)/2.0f, 
                                        (tempx1+tempx2)/2.0f)
                            wall2[numgeoms[1]]->SetCategoryBits(1)
                            numgeoms[1]=numgeoms[1]+1

                        //add geom above if needed
                        if tempy2<geomy2
                            wall2[numgeoms[1]] = new ode.geoms.Box(super.space,
                                        _wall_width, geomy2-tempy2, 
                                        tempx1-tempx2)
                            wall2[numgeoms[1]]->SetPosition(geompos.x, 
                                        (tempy2+geomy2)/2.0f, 
                                        (tempx1+tempx2)/2.0f)
                            wall2[numgeoms[1]]->SetCategoryBits(1)
                            numgeoms[1]=numgeoms[1]+1

                        //delete geom and shift geom array
                        delete wall2[i]
                        for var j = i to (numgeoms[1]-2)
                            wall2[j] = wall2[j+1];
                        numgeoms[1] = numgeoms[1] - 1
                        //recheck current i since array has been shifted
                        i = i -1

            //wall3 : back wall
            when 3
                if x1 < 0
                    x1 = 0
                if y1 < 0
                    y1 = 0
                if x2 > _size.width
                    x2 = _size.width
                if y2 > _size.height
                    y2 = _size.height
                //get actual values of x1,x2,y1,y2
                x1 = position.x + (_size.width/2.0f) - x1
                x2 = position.x + (_size.width/2.0f) - x2
                y1 = position.y - (_size.height/2.0f)+ y1
                y2 = position.y - (_size.height/2.0f)+ y2

                for var i = 0 to (numgeoms[2]-1)
                    //for each geom, check if door intersects with the geom 
                    //or not
                    lengths : Vector3 = new Vector3();
                    wall3[i]->GetLengths(lengths)
                    geompos : weak Vector3 = wall3[i]->GetPosition()
                    geomx1 : float = (float)(geompos.x + (lengths.x/2.0f))
                    geomx2 : float = (float)(geompos.x - (lengths.x/2.0f))
                    geomy1 : float = (float)(geompos.y - (lengths.y/2.0f))
                    geomy2 : float = (float)(geompos.y + (lengths.y/2.0f))

                    //if geom lies within the door
                    if (x1 >= geomx1) and (y1 <= geomy1) and (
                                x2 <= geomx2) and (y2 >= geomy2)
                        delete wall3[i]
                        for var j = i to (numgeoms[2]-2)
                            wall3[j] = wall3[j+1];
                        numgeoms[2] = numgeoms[2] - 1
                        //recheck current i since array has been shifted
                        i = i -1


                    //if the geom and door intersect
                    else if (x1>geomx2) and (x2<geomx1) and (
                                y1<geomy2) and (y2>geomy1)
                        tempx1 : float = x1
                        tempx2 : float = x2
                        tempy1 : float = y1
                        tempy2 : float = y2
                        //reduce door to cover only the geom
                        if x1>geomx1
                            tempx1=geomx1
                        if y1<geomy1
                            tempy1=geomy1
                        if x2<geomx2
                            tempx2=geomx2
                        if y2>geomy2
                            tempy2=geomy2

                        //add geom to left if needed
                        if tempx1<geomx1
                            wall3[numgeoms[2]] = new ode.geoms.Box(super.space,
                                        geomx1-tempx1, lengths.y, _wall_width)
                            wall3[numgeoms[2]]->SetPosition(
                                        (geomx1+tempx1)/2.0f, geompos.y, 
                                        geompos.z)
                            wall3[numgeoms[2]]->SetCategoryBits(1)
                            numgeoms[2]=numgeoms[2]+1
                        
                        //add geom to the right if needed
                        if tempx2>geomx2
                            wall3[numgeoms[2]] = new ode.geoms.Box(super.space,
                                        tempx2-geomx2, lengths.y, _wall_width)
                            wall3[numgeoms[2]]->SetPosition((geomx2+tempx2)/2.0f, geompos.y, geompos.z)
                            wall3[numgeoms[2]]->SetCategoryBits(1)
                            numgeoms[2]=numgeoms[2]+1

                        //add geom below if needed
                        if tempy1>geomy1
                            wall3[numgeoms[2]] = new ode.geoms.Box(super.space,
                                        tempx1-tempx2, tempy1-geomy1, 
                                        _wall_width)
                            wall3[numgeoms[2]]->SetPosition(
                                        (tempx1+tempx2)/2.0f, 
                                        (tempy1+geomy1)/2.0f, geompos.z)
                            wall3[numgeoms[2]]->SetCategoryBits(1)
                            numgeoms[2]=numgeoms[2]+1

                        //add geom above if needed
                        if tempy2<geomy2
                            wall3[numgeoms[2]] = new ode.geoms.Box(super.space,
                                        tempx1-tempx2, geomy2-tempy2, _wall_width)
                            wall3[numgeoms[2]]->SetPosition((tempx1+tempx2)/2.0f, 
                                        (tempy2+geomy2)/2.0f, geompos.z)
                            wall3[numgeoms[2]]->SetCategoryBits(1)
                            numgeoms[2]=numgeoms[2]+1

                        //delete geom and shift geom array
                        delete wall3[i]
                        for var j = i to (numgeoms[2]-2)
                            wall3[j] = wall3[j+1];
                        numgeoms[2] = numgeoms[2] - 1
                        //recheck current i since array has been shifted
                        i = i -1

            //wall4 : left wall
            when 4
                if x1 < 0
                    x1 = 0
                if y1 < 0
                    y1 = 0
                if x2 > _size.depth
                    x2 = _size.depth
                if y2 > _size.height
                    y2 = _size.height
                //get actual values of x1,x2,y1,y2
                x1 = position.z - (_size.depth/2.0f) + x1
                x2 = position.z - (_size.depth/2.0f) + x2
                y1 = position.y - (_size.height/2.0f)+ y1
                y2 = position.y - (_size.height/2.0f)+ y2

                for var i = 0 to (numgeoms[3]-1)
                    //for each geom, check if door intersects with the geom
                    //or not
                    lengths : Vector3 = new Vector3();
                    wall4[i]->GetLengths(lengths)
                    geompos : weak Vector3 = wall4[i]->GetPosition()
                    geomx1 : float = (float)(geompos.z - (lengths.z/2.0f))
                    geomx2 : float = (float)(geompos.z + (lengths.z/2.0f))
                    geomy1 : float = (float)(geompos.y - (lengths.y/2.0f))
                    geomy2 : float = (float)(geompos.y + (lengths.y/2.0f))

                    //if geom lies within the door
                    if (x1 <= geomx1) and (y1 <= geomy1) and (
                                    x2 >= geomx2) and (y2 >= geomy2)
                        delete wall4[i]
                        for var j = i to (numgeoms[3]-2)
                            wall4[j] = wall4[j+1];
                        numgeoms[3] = numgeoms[3] - 1
                        //recheck current i since array has been shifted
                        i = i - 1

                    //if the geom and door intersect
                    else if (x1<geomx2) and (x2>geomx1) and (
                                y1<geomy2) and (y2>geomy1)
                        tempx1 : float = x1
                        tempx2 : float = x2
                        tempy1 : float = y1
                        tempy2 : float = y2
                        //reduce door to cover only the geom
                        if x1<geomx1
                            tempx1=geomx1
                        if y1<geomy1
                            tempy1=geomy1
                        if x2>geomx2
                            tempx2=geomx2
                        if y2>geomy2
                            tempy2=geomy2

                        //add geom to left if needed
                        if tempx1>geomx1
                            wall4[numgeoms[3]] = new ode.geoms.Box(super.space,
                                        _wall_width, lengths.y, tempx1-geomx1)
                            wall4[numgeoms[3]]->SetPosition(geompos.x, 
                                        geompos.y, (geomx1+tempx1)/2.0f)
                            wall4[numgeoms[3]]->SetCategoryBits(1)
                            numgeoms[3]=numgeoms[3]+1
                        
                        //add geom to the right if needed
                        if tempx2<geomx2
                            wall4[numgeoms[3]] = new ode.geoms.Box(super.space,
                                        _wall_width, lengths.y, geomx2-tempx2)
                            wall4[numgeoms[3]]->SetPosition(geompos.x, 
                                        geompos.y, (geomx2+tempx2)/2.0f)
                            wall4[numgeoms[3]]->SetCategoryBits(1)
                            numgeoms[3]=numgeoms[3]+1

                        //add geom below if needed
                        if tempy1>geomy1
                            wall4[numgeoms[3]] = new ode.geoms.Box(super.space,
                                        _wall_width, tempy1-geomy1, 
                                        tempx2-tempx1)
                            wall4[numgeoms[3]]->SetPosition(geompos.x, 
                                        (tempy1+geomy1)/2.0f, 
                                        (tempx1+tempx2)/2.0f)
                            wall4[numgeoms[3]]->SetCategoryBits(1)
                            numgeoms[3]=numgeoms[3]+1

                        //add geom above if needed
                        if tempy2<geomy2
                            wall4[numgeoms[3]] = new ode.geoms.Box(super.space,
                                        _wall_width, geomy2-tempy2, 
                                        tempx2-tempx1)
                            wall4[numgeoms[3]]->SetPosition(geompos.x, 
                                        (tempy2+geomy2)/2.0f, 
                                        (tempx1+tempx2)/2.0f)
                            wall4[numgeoms[3]]->SetCategoryBits(1)
                            numgeoms[3]=numgeoms[3]+1

                        //delete geom and shift geom array
                        delete wall4[i]
                        for var j = i to (numgeoms[3]-2)
                            wall4[j] = wall4[j+1];
                        numgeoms[3] = numgeoms[3] - 1
                        //recheck current i since array has been shifted
                        i = i - 1

            //wall5 : floor
            when 5 
                if x1 < 0
                    x1 = 0
                if y1 < 0
                    y1 = 0
                if x2 > _size.width
                    x2 = _size.width
                if y2 > _size.depth
                    y2 = _size.depth
                //get actual values of x1,x2,y1,y2
                x1 = position.x - (_size.width/2.0f) + x1
                x2 = position.x - (_size.width/2.0f) + x2
                y1 = position.z - (_size.depth/2.0f) + y1
                y2 = position.z - (_size.depth/2.0f) + y2

                for var i = 0 to (numgeoms[4]-1)
                    //for each geom, check if door intersects with the geom 
                    //or not
                    lengths : Vector3 = new Vector3();
                    wall5[i]->GetLengths(lengths)
                    geompos : weak Vector3 = wall5[i]->GetPosition()
                    geomx1 : float = (float)(geompos.x - (lengths.x/2.0f))
                    geomx2 : float = (float)(geompos.x + (lengths.x/2.0f))
                    geomy1 : float = (float)(geompos.z - (lengths.z/2.0f))
                    geomy2 : float = (float)(geompos.z + (lengths.z/2.0f))

                    //if geom lies within the door
                    if (x1 <= geomx1) and (y1 <= geomy1) and (
                                x2 >= geomx2) and (y2 >= geomy2)
                        delete wall5[i]
                        for var j = i to (numgeoms[4]-2)
                            wall5[j] = wall5[j+1];
                        numgeoms[4] = numgeoms[4] - 1
                        //recheck current i since array has been shifted
                        i = i -1


                    //if the geom and door intersect
                    else if (x1<geomx2) and (x2>geomx1) and (
                                y1<geomy2) and (y2>geomy1)
                        tempx1 : float = x1
                        tempx2 : float = x2
                        tempy1 : float = y1
                        tempy2 : float = y2
                        //reduce door to cover only the geom
                        if x1<geomx1
                            tempx1=geomx1
                        if y1<geomy1
                            tempy1=geomy1
                        if x2>geomx2
                            tempx2=geomx2
                        if y2>geomy2
                            tempy2=geomy2

                        //add geom to left if needed
                        if tempx1>geomx1
                            wall5[numgeoms[4]] = new ode.geoms.Box(super.space,
                                        tempx1-geomx1, _wall_width, lengths.z)
                            wall5[numgeoms[4]]->SetPosition(
                                        (geomx1+tempx1)/2.0f, geompos.y, 
                                        geompos.z)
                            wall5[numgeoms[4]]->SetCategoryBits(1)
                            numgeoms[4]=numgeoms[4]+1
                        
                        //add geom to the right if needed
                        if tempx2<geomx2
                            wall5[numgeoms[4]] = new ode.geoms.Box(super.space,
                                        geomx2-tempx2, _wall_width, lengths.z)
                            wall5[numgeoms[4]]->SetPosition(
                                        (geomx2+tempx2)/2.0f, geompos.y, 
                                        geompos.z)
                            wall5[numgeoms[4]]->SetCategoryBits(1)
                            numgeoms[4]=numgeoms[4]+1

                        //add geom below if needed
                        if tempy1>geomy1
                            wall5[numgeoms[4]] = new ode.geoms.Box(super.space,
                                        tempx2-tempx1, _wall_width, 
                                        tempy1-geomy1)
                            wall5[numgeoms[4]]->SetPosition(
                                        (tempx1+tempx2)/2.0f, geompos.y, 
                                        (tempy1+geomy1)/2.0f)
                            wall5[numgeoms[4]]->SetCategoryBits(1)
                            numgeoms[4]=numgeoms[4]+1

                        //add geom above if needed
                        if tempy2<geomy2
                            wall5[numgeoms[4]] = new ode.geoms.Box(super.space,
                                        tempx2-tempx1, _wall_width, geomy2-tempy2)
                            wall5[numgeoms[4]]->SetPosition(
                                        (tempx1+tempx2)/2.0f, geompos.y, 
                                        (tempy2+geomy2)/2.0f)
                            wall5[numgeoms[4]]->SetCategoryBits(1)
                            numgeoms[4]=numgeoms[4]+1

                        //delete geom and shift geom array
                        delete wall5[i]
                        for var j = i to (numgeoms[4]-2)
                            wall5[j] = wall5[j+1];
                        numgeoms[4] = numgeoms[4] - 1
                        //recheck current i since array has been shifted
                        i = i -1

            //wall6 : ceiling
            when 6 
                if x1 < 0
                    x1 = 0
                if y1 < 0
                    y1 = 0
                if x2 > _size.width
                    x2 = _size.width
                if y2 > _size.depth
                    y2 = _size.depth
                //get actual values of x1,x2,y1,y2
                x1 = position.x - (_size.width/2.0f) + x1
                x2 = position.x - (_size.width/2.0f) + x2
                y1 = position.z + (_size.depth/2.0f) - y1
                y2 = position.z + (_size.depth/2.0f) - y2

                for var i = 0 to (numgeoms[5]-1)
                    //for each geom, check if door intersects with the geom 
                    //or not
                    lengths : Vector3 = new Vector3();
                    wall6[i]->GetLengths(lengths)
                    geompos : weak Vector3 = wall6[i]->GetPosition()
                    geomx1 : float = (float)(geompos.x - (lengths.x/2.0f))
                    geomx2 : float = (float)(geompos.x + (lengths.x/2.0f))
                    geomy1 : float = (float)(geompos.z + (lengths.z/2.0f))
                    geomy2 : float = (float)(geompos.z - (lengths.z/2.0f))

                    //if geom lies within the door
                    if (x1 <= geomx1) and (y1 >= geomy1) and (
                                x2 >= geomx2) and (y2 <= geomy2)
                        delete wall6[i]
                        for var j = i to (numgeoms[5]-2)
                            wall6[j] = wall6[j+1];
                        numgeoms[5] = numgeoms[5] - 1
                        //recheck current i since array has been shifted
                        i = i - 1


                    //if the geom and door intersect
                    else if (x1<geomx2) and (x2>geomx1) and (
                                y1>geomy2) and (y2<geomy1)
                        tempx1 : float = x1
                        tempx2 : float = x2
                        tempy1 : float = y1
                        tempy2 : float = y2
                        //reduce door to cover only the geom
                        if x1<geomx1
                            tempx1=geomx1
                        if y1>geomy1
                            tempy1=geomy1
                        if x2>geomx2
                            tempx2=geomx2
                        if y2<geomy2
                            tempy2=geomy2

                        //add geom to left if needed
                        if tempx1>geomx1
                            wall6[numgeoms[5]] = new ode.geoms.Box(super.space,
                                        tempx1-geomx1, _wall_width, lengths.z)
                            wall6[numgeoms[5]]->SetPosition(
                                        (geomx1+tempx1)/2.0f, geompos.y, 
                                        geompos.z)
                            wall6[numgeoms[5]]->SetCategoryBits(1)
                            numgeoms[5]=numgeoms[5]+1
                        
                        //add geom to the right if needed
                        if tempx2<geomx2
                            wall6[numgeoms[5]] = new ode.geoms.Box(super.space,
                                        geomx2-tempx2, _wall_width, lengths.z)
                            wall6[numgeoms[5]]->SetPosition(
                                        (geomx2+tempx2)/2.0f,geompos.y, 
                                        geompos.z)
                            wall6[numgeoms[5]]->SetCategoryBits(1)
                            numgeoms[5]=numgeoms[5]+1

                        //add geom below if needed
                        if tempy1<geomy1
                            wall6[numgeoms[5]] = new ode.geoms.Box(super.space,
                                        tempx2-tempx1, _wall_width, geomy1-tempy1)
                            wall6[numgeoms[5]]->SetPosition(
                                        (tempx1+tempx2)/2.0f, geompos.y, 
                                        (tempy1+geomy1)/2.0f)
                            wall6[numgeoms[5]]->SetCategoryBits(1)
                            numgeoms[5]=numgeoms[5]+1

                        //add geom above if needed
                        if tempy2>geomy2
                            wall6[numgeoms[5]] = new ode.geoms.Box(super.space,
                                        tempx2-tempx1, _wall_width, 
                                        tempy2-geomy2)
                            wall6[numgeoms[5]]->SetPosition(
                                        (tempx1+tempx2)/2.0f, geompos.y, 
                                        (tempy2+geomy2)/2.0f)
                            wall6[numgeoms[5]]->SetCategoryBits(1)
                            numgeoms[5]=numgeoms[5]+1

                        //delete geom and shift geom array
                        delete wall6[i]
                        for var j = i to (numgeoms[5]-2)
                            wall6[j] = wall6[j+1];
                        numgeoms[5] = numgeoms[5] - 1
                        //recheck current i since array has been shifted
                        i = i - 1
        _updated = true
