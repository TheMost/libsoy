/*
 *  libsoy - soy.scenes.Scene.debugquad_glslf
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

precision mediump float;
uniform sampler2D depthMap;
varying vec2 TexCoords;


// uniform float near_plane;
// uniform float far_plane;

 // required when using a perspective projection matrix
/* float LinearizeDepth(float depth)
 {
     float z = depth * 2.0 - 1.0; // Back to NDC
     return (2.0 * near_plane * far_plane) / (far_plane + near_plane - z * (far_plane - near_plane));
 }
*/

 void main()
 {
    float depthValue = texture2D(depthMap, TexCoords).x;
     // FragColor = vec4(vec3(LinearizeDepth(depthValue) / far_plane), 1.0); // perspective
    gl_FragColor = vec4(vec3(depthValue), 1.0); // orthographic
 }
