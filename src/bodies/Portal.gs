/*
 *  libsoy - soy.bodies.Portal
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GL
    ode


class soy.bodies.Portal : soy.bodies.Body
    _ebo : GLuint   // Element Buffer Object
    _vbo : GLuint   // Vertex Buffer Object
    _updated : bool // Buffers need updating
    _material : soy.materials.Material
    portal_space : unowned ode.spaces.Space*
    geoms_in_portal : array of ode.geoms.Geom*
    num_geoms_in_portal : int

    construct (position : soy.atoms.Position?, size : soy.atoms.Size?)
        super(position, size, 0.0f)
        _material = new soy.materials.Colored("blue")
        _ebo = 0
        portal_space = null
        geoms_in_portal = new array of ode.geoms.Geom*[10]
        num_geoms_in_portal = 0

    final
        if self.scene is not null and self.scene.portals.contains(self)
            soy.scenes._stepLock.writer_lock()
            self.scene.portals.remove(self)
            soy.scenes._stepLock.writer_unlock()

    def override add_extra ()
        scene.portals.add(self)

    def override remove_extra ()
        scene.portals.remove(self)



    def override create_geom (geom_param : Object?, geom_scalar : float)
        _width = _height = _depth = 1.0f

        geom = new ode.geoms.Box(null, (Real) _width, (Real) _height,
                                 (Real) 0.01f)

        // set size if provided
        if geom_param is not null
            self.size = (soy.atoms.Size) geom_param
        //make the portal solid til a target is set
        //this logically means that the portal opens up nowhere
        geom.SetCategoryBits(1)
        geom.SetData((void*) self)

        body.SetData((void*) self)

        // Copy position and orientation from geom
        pos : weak ode.Vector3 = geom.GetPosition()
        body.SetPosition(pos.x, pos.y, pos.z)
        body.SetRotation(geom.GetRotation())

        self.geom.SetBody(self.body)

        // Set mass of the body
        self.set_mass (self.density)

        _updated = true

    def override set_mass (density : float)
        if body is null
            return

        if density is not 0 and self.volume() != 0.0f
            mass : ode.Mass = new ode.Mass()
            mass.SetBox(density, (Real) _width, (Real) _height, (Real) 1.0f)
            body.SetMass(mass)
            body.SetGravityMode(1)
        else
            body.SetGravityMode(0)

    def is_point_in_frustum(point : soy.atoms.Position, view : array of 
                                GLfloat, camera : soy.bodies.Camera) : bool
        p : array of float[4] = {point.x, point.y, point.z, 1.0f}
        //point in camera space
        p_cs : array of float[4] = new array of float[4]
        //note : view matrix is column major
        for var i = 0 to 3
            p_cs[i] = 0.0f
            for var j = 0 to 3
                p_cs[i] += view[i+4*j] * p[j]

        half_height : float = Posix.tanf(camera.lens
                                    / 2.0f * 3.141592653589793f 
                                    / 180.0f) * camera.zfar
        half_width : float = camera.aspect * half_height
        //5.0f is an offset added to znear to prevent scenes/bodies that are
        // just in front of the camera from being rendered
        if (p_cs[0]<half_width) and (p_cs[0]> -half_width) and (p_cs[1]<
                    half_height) and (p_cs[1]> -half_height) and (p_cs[2]>
                    -camera.zfar) and (p_cs[2]<-camera.znear+5.0f)
            return true
        else
            return false

    def render_extra(view : array of GLfloat, projection : array of 
                               GLfloat, camera : soy.bodies.Camera, 
                                renderedScenes : dict of soy.scenes.Scene, int)
        //target scene
        t_scene : soy.scenes.Scene = _target.scene
        renderedScenes[t_scene] = 1
        //portal bodies
        p_bodies : dict of string, soy.bodies.Body = t_scene.bodies

        if is_point_in_frustum(position, view, camera)
            t_scene.render_extra(view, projection, camera, 
                    t_scene.lights.to_array())

            for var s in p_bodies.keys
                if p_bodies[s].geom.GetCategoryBits() is GeomBody
                    geompos : weak ode.Vector3 = p_bodies[s].geom.GetPosition()
                    if is_point_in_frustum(new soy.atoms.Position(
                                (float)geompos.x, (float)geompos.y, 
                                (float)geompos.z), view, camera)
                        p_bodies[s].render(false, view, projection, 
                                    t_scene.lights.to_array(), 
                                    t_scene.ambient.get4f(), 
                                    t_scene.fog.get4f())
                        p_bodies[s].render(true, view, projection, 
                                    t_scene.lights.to_array(),
                                    t_scene.ambient.get4f(), 
                                    t_scene.fog.get4f())
                else if p_bodies[s].geom.GetCategoryBits() is GeomPortal
                    //continue only if this portal is not the other half of the
                    //current portal
                    targetScene : soy.scenes.Scene = ((soy.bodies.Portal)
                                                    (p_bodies[s])).target.scene
                    if  renderedScenes.has_key(targetScene) == false
                        geompos : weak ode.Vector3 = p_bodies[s].geom.GetPosition()
                        if is_point_in_frustum(new soy.atoms.Position(
                                        (float)geompos.x, (float)geompos.y, 
                                        (float)geompos.z), view, camera)
                            ((soy.bodies.Portal)(p_bodies[s])).render_extra(
                                                        view, projection, 
                                                        camera, renderedScenes)


    //useful for error checking and debugging
    /*def override render ( alpha_stage : bool, view : array of GLfloat,
                          projection : array of GLfloat, lights : array of
                          soy.bodies.Light, ambient : array of GLfloat,
                          fog_color: array of GLfloat )

        // Lock so body cant be changed during render
        mutex.lock()

        // get model matrix
        model : array of GLfloat = self.model_matrix()

        // modelview matrix
        model_view : array of GLfloat[16] = self.calculate_model_view(
                                                            model, view)
        //model_view : array of GLfloat[16] = view

        // Update ebo/vbo as needed
        if _updated
            _update_portal()

        // Re-bind buffers when not updating
        else
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo)
            glBindBuffer(GL_ARRAY_BUFFER, _vbo)

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, (GLsizei) 
                                (sizeof(GLfloat) * 11), null) 
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, (GLsizei) 
                                (sizeof(GLfloat) * 11), (GLvoid*) 
                                (sizeof(GLfloat) * 3)) 
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, (GLsizei) 
                                (sizeof(GLfloat) * 11), (GLvoid*) 
                                (sizeof(GLfloat) * 6)) 
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, (GLsizei) 
                                (sizeof(GLfloat) * 11), (GLvoid*)
                                (sizeof(GLfloat) * 8))

        glEnableVertexAttribArray(0)
        glEnableVertexAttribArray(1)
        glEnableVertexAttribArray(2)
        glEnableVertexAttribArray(3)

        i : int = 0
        while self._material.enable(i, model_view, view, projection, lights,
                                    ambient, fog_color)
            glDrawElements(GL_TRIANGLES, (GLsizei) 36,
                           GL_UNSIGNED_SHORT, (GLvoid*) 0)
            i++

        glDisableVertexAttribArray(0) 
        glDisableVertexAttribArray(1) 
        glDisableVertexAttribArray(2) 
        glDisableVertexAttribArray(3) 
        self._material.disable()

        // Rendering done, unlock
        mutex.unlock()*/



    def _update_portal()

        // on the first pass
        if _ebo is 0
            buffers : array of GLuint = {0,0}
            glGenBuffers(buffers)
            _ebo = buffers[0]
            _vbo = buffers[1]

        elements : array of GLushort = new array of GLushort[36]
        vertices : array of GLfloat = new array of GLfloat[264]
        eoffset : int = 0
        evalue : GLushort = 0

        _add_to_vertices((ode.geoms.Box)geom, vertices)
        for var i = 0 to 5
            elements[eoffset] = evalue
            elements[eoffset+1] = evalue+1
            elements[eoffset+2] = evalue+2
            elements[eoffset+3] = evalue+2
            elements[eoffset+4] = evalue+1
            elements[eoffset+5] = evalue+3
            eoffset+=6
            evalue+=4

        // bind elements
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizei) (36 * sizeof(GLushort))
                     ,elements, GL_STATIC_DRAW)

        // bind vertices
        glBindBuffer(GL_ARRAY_BUFFER, _vbo)
        glBufferData(GL_ARRAY_BUFFER, (GLsizei) (264 * sizeof(GLfloat)),
                     vertices, GL_STATIC_DRAW)

        // Reset updated flag
        _updated = false


    def _add_to_vertices(box : ode.geoms.Box, vertices : array of GLfloat)
        lengths : Vector3 = new Vector3();
        box.GetLengths(lengths)
        voffset : int = 0

        //back face        
        for var i = 0 to 1
            for var j = 0 to 1
                //position
                vertices[voffset] = (GLfloat)((2*i-1)*(lengths.x/2.0f))
                vertices[voffset+1] = (GLfloat)((2*j-1)*(lengths.y/2.0f))
                vertices[voffset+2] = (GLfloat)((-lengths.z/2.0f))
                //normal
                vertices[voffset+3] = 0.0f
                vertices[voffset+4] = 0.0f
                vertices[voffset+5] = -1.0f
                //uv
                vertices[voffset+6] = 1.0f-j
                vertices[voffset+7] = i
                //tangent
                vertices[voffset+8] = -1.0f
                vertices[voffset+9] = 0.0f
                vertices[voffset+10] = 0.0f
                voffset+=11


        //front face        
        for var i = 0 to 1
            for var j = 0 to 1
                //position
                vertices[voffset] = (GLfloat)((2*j-1)*(lengths.x/2.0f))
                vertices[voffset+1] = (GLfloat)((2*i-1)*(lengths.y/2.0f))
                vertices[voffset+2] = (GLfloat)((lengths.z/2.0f))
                //normal
                vertices[voffset+3] = 0.0f
                vertices[voffset+4] = 0.0f
                vertices[voffset+5] = 1.0f
                //uv
                vertices[voffset+6] = j
                vertices[voffset+7] = i
                //tangent
                vertices[voffset+8] = 1.0f
                vertices[voffset+9] = 0.0f
                vertices[voffset+10] = 0.0f
                voffset+=11


        //left face        
        for var i = 0 to 1
            for var j = 0 to 1
                //position
                vertices[voffset] = (GLfloat)(-(lengths.x/2.0f))
                vertices[voffset+1] = (GLfloat)((2*i-1)*(lengths.y/2.0f))
                vertices[voffset+2] = (GLfloat)((2*j-1)*(lengths.z/2.0f))
                //normal
                vertices[voffset+3] = -1.0f
                vertices[voffset+4] = 0.0f
                vertices[voffset+5] = 0.0f
                //uv
                vertices[voffset+6] = j
                vertices[voffset+7] = i
                //tangent
                vertices[voffset+8] = 0.0f
                vertices[voffset+9] = 0.0f
                vertices[voffset+10] = 1.0f
                voffset+=11


        //right face        
        for var i = 0 to 1
            for var j = 0 to 1
                //position
                vertices[voffset] = (GLfloat)((lengths.x/2.0f))
                vertices[voffset+1] = (GLfloat)((2*j-1)*(lengths.y/2.0f))
                vertices[voffset+2] = (GLfloat)((2*i-1)*(lengths.z/2.0f))
                //normal
                vertices[voffset+3] = 1.0f
                vertices[voffset+4] = 0.0f
                vertices[voffset+5] = 0.0f
                //uv
                vertices[voffset+6] = 1.0f-i
                vertices[voffset+7] = j
                //tangent
                vertices[voffset+8] = 0.0f
                vertices[voffset+9] = 0.0f
                vertices[voffset+10] = -1.0f
                voffset+=11


        //bottom face        
        for var i = 0 to 1
            for var j = 0 to 1
                //position
                vertices[voffset] = (GLfloat)((2*j-1)*(lengths.x/2.0f))
                vertices[voffset+1] = (GLfloat)(-(lengths.y/2.0f))
                vertices[voffset+2] = (GLfloat)((2*i-1)*(lengths.z/2.0f))
                //normal
                vertices[voffset+3] = 0.0f
                vertices[voffset+4] = -1.0f
                vertices[voffset+5] = 0.0f
                //uv
                vertices[voffset+6] = j
                vertices[voffset+7] = i
                //tangent
                vertices[voffset+8] = 1.0f
                vertices[voffset+9] = 0.0f
                vertices[voffset+10] = 0.0f
                voffset+=11


        //top face        
        for var i = 0 to 1
            for var j = 0 to 1
                //position
                vertices[voffset] = (GLfloat)((2*i-1)*(lengths.x/2.0f))
                vertices[voffset+1] = (GLfloat)((lengths.y/2.0f))
                vertices[voffset+2] = (GLfloat)((2*j-1)*(lengths.z/2.0f))
                //normal
                vertices[voffset+3] = 0.0f
                vertices[voffset+4] = 1.0f
                vertices[voffset+5] = 0.0f
                //uv
                vertices[voffset+6] = 0.0f
                vertices[voffset+7] = 1.0f-j
                //tangent
                vertices[voffset+8] = 1.0f
                vertices[voffset+9] = 0.0f
                vertices[voffset+10] = 0.0f
                voffset+=11



    def override volume ( ) : float
        return (self._width * self._height * self._depth)

    // portal_space->GetGeom(i) seems to remove geom from the space 
    // I'll save all the geoms in my own array as a counter measure
    def check_portal_space()
        if portal_space is null
            return
        numberOfContacts : int
        contactGeoms : ode.ContactGeomArray* = new ode.ContactGeomArray(3)
        for var i = 0 to (num_geoms_in_portal-1)
            numberOfContacts = geom.Collide(geoms_in_portal[i], 3,
                                         contactGeoms,
                                         (int) sizeof(ode.ContactGeom))
            if numberOfContacts is 0
                if portal_space->Query(geoms_in_portal[i]) is 1
                    portal_space->Remove(geoms_in_portal[i])

                //if not colliding with portal and not in current scene, add to
                //target scene
                if not scene.is_point_in_scene(
                            geoms_in_portal[i]->GetPosition())
                    bodykey : string = ((soy.bodies.Body*)(
                                geoms_in_portal[i]->GetData()))->key
                    scene.bodies.unset(bodykey)
                    ((soy.bodies.Body*)(geoms_in_portal[i]->GetData())
                                )->scene = _target.scene
                    if _target.scene.space.Query(geoms_in_portal[i]) is 0
                        _target.scene.space.Add(geoms_in_portal[i])
                    _target.scene.bodies[bodykey] = ((soy.bodies.Body*)(
                                geoms_in_portal[i]->GetData()))
                else
                    if scene.space.Query(geoms_in_portal[i]) is 0
                        scene.space.Add(geoms_in_portal[i])

                //remove the geom from geoms_in_portal
                //this has to be done for both ends of the portal
                for var j = i to (num_geoms_in_portal-2)
                    geoms_in_portal[j] = geoms_in_portal[j+1]
                num_geoms_in_portal = num_geoms_in_portal - 1
                //check ith position again since array has been shifted
                i--

    //set transfer_to_portal of body if body collides with portal_space
    def body_portal_collided (body : soy.bodies.Body*)
        if (portal_space is null)
            return
        numberOfContacts : int
        contactGeoms : ode.ContactGeomArray* = new ode.ContactGeomArray(4)
        numberOfContacts = body->geom.Collide(portal_space, 4,
                                         contactGeoms,
                                         (int) sizeof(ode.ContactGeom))
        if numberOfContacts>0
            body->transfer_to_portal = self


    ////////////////////////////////////////////////////////////////////////
    // Properties

    //
    // Size Property
    _width  : GLfloat
    _height : GLfloat
    _depth  : GLfloat
    _size_obj : weak soy.atoms.Size?

    def _size_set(size : soy.atoms.Size)
        // Set size while locked to avoid potential rendering weirdness
        mutex.lock()
        _width = (GLfloat) size.width
        _height = (GLfloat) size.height
        _depth = (GLfloat) size.depth
        _updated = true
        mutex.unlock()

        if scene is not null
            soy.scenes._stepLock.writer_lock()
        ((geoms.Box) self.geom).SetLengths((Real) _width,
                                           (Real) _height,
                                           (Real) _depth)
        self.set_mass (self.density)

        if scene is not null
            soy.scenes._stepLock.writer_unlock()

    def _size_weak(size : Object)
        _size_obj = null

    prop size : soy.atoms.Size
        owned get
            value : soy.atoms.Size? = self._size_obj
            if value is null
                value = new soy.atoms.Size((float) _width,
                                           (float) _height,
                                           (float) _depth)
                value.on_set.connect(self._size_set)
                value.weak_ref(self._size_weak)
                self._size_obj = value
            return value
        set
            self._size_set(value)
            _size_obj = value
            value.on_set.connect(self._size_set)
            value.weak_ref(self._size_weak)


    // target property
    _target : soy.bodies.Portal?
    prop target : soy.bodies.Portal?
        get
            return _target
        set
            //TODO check this once portal is setup for 2 scenes
            mutex.lock()
            //free already exsting portal_space if it is not being used by 
            //either ends of the portal
            if (value is null) and (_target is not null) and (
                        _target.portal_space is null)
                delete portal_space
            _target = value

            if _target is not null
                //allow objects to pass through the portal
                geom.SetCategoryBits(17)
                if _target.portal_space is null
                    portal_space = new ode.spaces.Space(null)
                else
                    portal_space = _target.portal_space
            else
                geom.SetCategoryBits(1)
            mutex.unlock()
