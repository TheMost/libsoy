/*
 *  libsoy - soy.scenes.Voxelization
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program if not, see http:/www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GLib
    GLib.Math
    GL
    Gee
    ode

class soy.generators.Simplex2d : Object
    _perm : array of int
    _generator : GLib.Rand?

    init
        _seed = 0
        _min = 0.0f
        _max = 0.0f
        _scale = 0.0f
        _voxel = new soy.atoms.Voxel(new soy.atoms.Color.named("red"))
        _perm = new array of int[512]
        _generator = null

    construct(seed : int, min : float, max : float, scale : float, voxel : soy.atoms.Voxel)
        this._min = min
        this._max = max
        this._scale = scale
        this._seed = seed
        this._voxel = new soy.atoms.Voxel(voxel.color)
        this._generator = new GLib.Rand.with_seed(this._seed)
        for var i = 0 to 255
            _perm[i] = i
        for var i = 0 to 255
            j : int = _generator.int_range(0,256)
            aux : int = _perm[i]
            _perm[i] = _perm[j]
            _perm[j] = aux
        for var i = 0 to 255
            _perm[i+256] = _perm[i]

    //returns 2D simplex noise value for given coordinates.
    //return is a voxel if the simplex noise value is > 0, null otherwise
    def val(x : float, y : float, height : float) : soy.atoms.Voxel?
        x = x/scale
        y = y/scale
        // Noise contributions from the four corners
        n0 : float
        n1 : float
        n2 : float

        // Skew the input space to determine which simplex cell we're in
        F2 : float = 0.5f * (1.73205080757f - 1.0f)
        s : float = (x+y)*F2 // Very nice and simple skew factor for 3D
        i : int = fastfloor(x+s)
        j : int = fastfloor(y+s)

        G2 : float = (3.0f - 1.73205080757f) / 6.0f // Very nice and simple unskew factor, too
        t : float = (i+j)*G2
        X0 : float = i-t // Unskew the cell origin back to (x,y) space
        Y0 : float = j-t
        x0 : float = x-X0 // The x,y distances from the cell origin
        y0 : float = y-Y0

        // For the 2D case, the simplex shape is a slightly irregular tetrahedron.
        // Determine which simplex we are in.

        // Offsets for second corner of simplex in (i,j) coords
        i1 : int
        j1 : int

        if x0 >= y0
            i1=1
            j1=0
        else
            i1=0
            j1=1

        // A step of (1,0,0) in (i,j,k) means a step of (1-c,-c,-c) in (x,y,z),
        // a step of (0,1,0) in (i,j,k) means a step of (-c,1-c,-c) in (x,y,z), and
        // a step of (0,0,1) in (i,j,k) means a step of (-c,-c,1-c) in (x,y,z), where
        // c = 1/6.
        x1 : float = x0 - i1 + G2 // Offsets for middle corner in (x,y) coords
        y1 : float = y0 - j1 + G2
        x2 : float = x0 - 1.0f + 2.0f * G2 // Offsets for last corner in (x,y) coords
        y2 : float = y0 - 1.0f + 2.0f * G2

        // Work out the hashed gradient indices of the four simplex corners
        ii : int = i & 255
        jj : int = j & 255
        gi0 : int = _perm[ii+_perm[jj]] % 12
        gi1 : int = _perm[ii+i1+_perm[jj+j1]] % 12
        gi2 : int = _perm[ii+1+_perm[jj+1]] % 12

        // Calculate the contribution from the four corners
        t0 : float = 0.5f - x0*x0 - y0*y0
        if t0 < 0
            n0 = 0.0f
        else
            t0 *= t0
            n0 = t0 * t0 * dot(gi0, x0, y0)

        t1 : float = 0.5f - x1*x1 - y1*y1
        if t1 < 0
            n1 = 0.0f
        else
            t1 *= t1
            n1 = t1 * t1 * dot(gi1, x1, y1)

        t2 : float = 0.5f - x2*x2 - y2*y2
        if t2 < 0
            n2 = 0.0f
        else
            t2 *= t2
            n2 = t2 * t2 * dot(gi2, x2, y2)

        // Add contributions from each corner to get the final noise value.
        // The result is scaled to stay just inside [-1,1]
        result : float = 32.0f*(n0 + n1 + n2)
        //scale the result
        result += 1 //in [0,2]
        result *= 0.5f // in [0,1]
        difference : float = this._max-this._min
        if (this._min + difference * result) < height do return null
        return this._voxel

    def fastfloor(x : float) : int
        return x > 0 ? (int) x : ((int) x) - 1

    def dot(g : int, x : float, y : float) : float
        return grad3[g,0]*x + grad3[g,1]*y

    _seed : int
    prop seed : int
        get
            return _seed
        set
            self._seed = value

    _min : float
    prop min : float
        get
            return _min
        set
            self._min = value

    _max : float
    prop max : float
        get
            return _max
        set
            self._max = value

    _scale : float
    prop scale : float
        get
            return _scale
        set
            self._scale = value

    _voxel : soy.atoms.Voxel?
    prop voxel : soy.atoms.Voxel?
        get
            return _voxel
        set
            self._voxel = value

    grad3 : static array of int[,]

    init static
        grad3 = {
            {1,1,0}, {-1,1,0}, {1,-1,0}, {-1,-1,0},
            {1,0,1}, {-1,0,1}, {1,0,-1}, {-1,0,-1},
            {0,1,1}, {0,-1,1}, {0,1,-1}, {0,-1,-1}
        }
