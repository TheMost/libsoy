/*
 *  libsoy - soy.fields.Monopole
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GLib
    ode

class soy.fields.Monopole : soy.fields.Field
    /*
    soy.fields.Monopole

    Monopoles are invisible fields that either attract or repulse other
    bodies in the entire scene based on an inverse square law.
    */

    construct(mult : float = 0.0f, position : soy.atoms.Position = new soy.atoms.Position(0,0,0))
        self._multiplier = mult
        self.position = position


    init
        position = new soy.atoms.Position(0,0,0);
        _multiplier = 0.0f


    // This function is applied once per physics cycle for each combination of affected body and field.
    def override exert(other : soy.bodies.Body) : bool
        if !inside_region(other)
            return false
        var xd = other.position.x - self.position.x
        var yd = other.position.y - self.position.y
        var zd = other.position.z - self.position.z

        // calculate direct distances
        var d2 = xd*xd + yd*yd + zd*zd
        if d2 > 0.2   // other wise force becomes too much
            var d = Math.sqrtf(d2)
            // get other mass
            var mm2 = other.density * other.volume()
            // calculate force
            var f = self.multiplier * mm2 / d2
            // apply force as a vector
            other.addForce(-f * xd/d, -f*yd/d, -f*zd/d)
        return true

    //
    // Properties

    // Monopole's multiplier
    // This is how much force will be applied.
    _multiplier : float
    prop multiplier : float
        get
            return self._multiplier
        set
            self._multiplier = value
