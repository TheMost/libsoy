/*
 *  libsoy - soy.recording.TypeSerializer
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses GL

class soy.recording.TypeSerializer : Object
    def static serializeGLuintArr ( arr : array of GLuint ) : string
        var builder = new StringBuilder()
        builder.append(arr.length.to_string() + "/")
        i : int
        for i = 0 to (arr.length-1)
            builder.append(arr[i].to_string())
            builder.append("$")
        return builder.str

    def static deserializeGLuintArr(str : string) : array of GLuint
        var lensplit = str.split("/")
        var length = int.parse(lensplit[0])

        var elements = lensplit[1].split("$")
        var ret = new array of GLuint[length]
        i : int
        for i = 0 to (length-1)
            ret[i] = (GLuint)int.parse(elements[i])
        return ret



    def static serializeGLintArr ( arr : array of GLint ) : string
        var builder = new StringBuilder()
        builder.append(arr.length.to_string() + "/")
        i : int
        for i = 0 to (arr.length-1)
            builder.append(arr[i].to_string())
            builder.append("$")
        return builder.str

    def static deserializeGLintArr(str : string) : array of GLint
        var lensplit = str.split("/")
        var length = int.parse(lensplit[0])

        var elements = lensplit[1].split("$")
        var ret = new array of GLint[length]
        i : int
        for i = 0 to (length-1)
            ret[i] = (GLint)int.parse(elements[i])
        return ret



    def static serializeGLfloatArr ( arr : array of GLfloat ) : string
        var builder = new StringBuilder()
        builder.append(arr.length.to_string() + "/")
        i : int
        for i = 0 to (arr.length-1)
            builder.append(arr[i].to_string())
            builder.append("$")
        return builder.str

    def static deserializeGLfloatArr(str : string) : array of GLfloat
        var lensplit = str.split("/")
        var length = int.parse(lensplit[0])

        var elements = lensplit[1].split("$")
        var ret = new array of GLfloat[length]
        i : int
        for i = 0 to (length-1)
            ret[i] = (GLfloat)double.parse(elements[i])
        return ret



    def static serializeGLcharptrArr ( arr : array of GLchar* ) : string
        var builder = new StringBuilder()
        i : int
        builder.append(arr.length.to_string() + "/")

        for i = 0 to (arr.length-1)
            var strLen = 0
            while true
                if arr[i][strLen] == '\0'
                    break
                strLen++

            var subStrBuilder = new StringBuilder()
            charIdx : int
            for charIdx = 0 to (strLen-1)
                subStrBuilder.append(arr[i][charIdx].to_string())
            var encodedStr = Base64.encode(subStrBuilder.data)
            builder.append(encodedStr + "$")

        return builder.str

    def static deserializeGLcharptrArr(str : string) : array of GLchar*
        var lensplit = str.split("/")
        var length = int.parse(lensplit[0])

        var elements = lensplit[1].split("$")
        var ret = new array of GLchar*[length]
        i : int
        for i = 0 to (length-1)
            var decoded = Base64.decode(elements[i])
            var len = decoded.length
            ret[i] = new array of GLchar[decoded.length+1]
            si : int
            for si = 0 to (decoded.length-1)
                ret[i][si] = (char)decoded[si]
        return ret



    def static deserializeGLbooleanArr(str : string) : array of GLboolean
        var lensplit = str.split("/")
        var length = int.parse(lensplit[0])

        var elements = lensplit[1].split("$")
        var ret = new array of GLboolean[length]
        i : int
        for i = 0 to (length-1)
            ret[i] = (GLboolean)bool.parse(elements[i])
        return ret

    def static serializeGLbooleanArr ( arr : array of GLboolean ) : string
        var builder = new StringBuilder()
        builder.append(arr.length.to_string() + "/")
        i : int
        for i = 0 to (arr.length-1)
            builder.append(arr[i].to_string())
            builder.append("$")
        return builder.str



    def static serializeGLcharArr ( arr : array of GLchar ) : string
        var builder = new StringBuilder()
        builder.append(arr.length.to_string() + "/")
        i : int
        for i = 0 to (arr.length-1)
            builder.append(arr[i].to_string())
            builder.append("$")
        return builder.str

