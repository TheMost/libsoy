/*
 *  libsoy - soy.scenes.Voxelization
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GLib
    GL
    Gee
    ode
    GLib.Math

CHUNKSIZE : int = 16
CHUNKSIZE_POW2 : int = 4
CHUNKSIZE_MASK : int = 15
WORLDSIZE : int = 8
WORLDSIZE_POW2 : int = 3
WORLDSIZE_MASK : int = 7
WORLDHEIGHT : int = 4
WORLDHEIGHT_POW2 : int = 2
NUM_VOXEL_VERT_FLOATS : int = 8
AO_MAX_RAD : int = 9
//LIGHTSUM_SIZE = (CHUNKSIZE+AO_MAX_RAD+AO_MAX_RAD)
LIGHTSUM_SIZE : int = 34
lightSum : array of int[,,]
voxelColors : list of soy.atoms.Color

class soy.scenes.Voxelization : soy.scenes.Scene

    //TODO: Put Plane and Frustum in soy.bodies.Camera and use it for general frustum culling
    class Plane : Object
        normal : soy.atoms.Vector
        dist : float

        init
            normal = new soy.atoms.Vector(0,0,0)
            dist = 0.0f

        construct(p0 : soy.atoms.Vector, p1 : soy.atoms.Vector, p2 : soy.atoms.Vector)
            v : soy.atoms.Vector = new soy.atoms.Vector(p1.x-p0.x, p1.y-p0.y, p1.z-p0.z)
            u : soy.atoms.Vector = new soy.atoms.Vector(p2.x-p0.x, p2.y-p0.y, p2.z-p0.z)
            normal = u.cross(v)
            if normal.magnitude() > 0.001f do normal.normalize()
            dist = -normal.dot(p0)

        def inside(center : soy.atoms.Vector, radius : float) : bool
            distance : float = normal.dot(center) + dist
            return (distance < radius)

    class Frustum : Object
        enum PlaneID
            TOP_PLANE = 0
            BOTTOM_PLANE = 1
            LEFT_PLANE = 2
            RIGHT_PLANE = 3
            NEAR_PLANE = 4
            FAR_PLANE = 5

        planes : array of Plane

        init
            planes = new array of Plane[6]

        def test(center : soy.atoms.Vector, radius : float) : bool
            for var i = 0 to 5
                if this.planes[i].inside(center, radius) == false do return false
            return true

        def inverse(m : array of float) : array of float
            inv : array of float = new array of float[16]
            det : float = 0;
            inv[0] = m[5]  * m[10] * m[15] - m[5]  * m[11] * m[14] - \
                     m[9]  * m[6]  * m[15] + m[9]  * m[7]  * m[14] + \
                     m[13] * m[6]  * m[11] - m[13] * m[7]  * m[10]
            inv[4] = -m[4]  * m[10] * m[15] + m[4]  * m[11] * m[14] + \
                    m[8]  * m[6]  * m[15] - m[8]  * m[7]  * m[14] - \
                    m[12] * m[6]  * m[11] + m[12] * m[7]  * m[10]
            inv[8] = m[4]  * m[9] * m[15] - m[4]  * m[11] * m[13] - \
                    m[8]  * m[5] * m[15] + m[8]  * m[7] * m[13] + \
                    m[12] * m[5] * m[11] - m[12] * m[7] * m[9]
            inv[12] = -m[4]  * m[9] * m[14] +  m[4]  * m[10] * m[13] + \
                    m[8]  * m[5] * m[14] -  m[8]  * m[6] * m[13] - \
                    m[12] * m[5] * m[10] +  m[12] * m[6] * m[9]
            inv[1] = -m[1]  * m[10] * m[15] + m[1]  * m[11] * m[14] + \
                    m[9]  * m[2] * m[15] - m[9]  * m[3] * m[14] - \
                    m[13] * m[2] * m[11] + m[13] * m[3] * m[10]
            inv[5] = m[0]  * m[10] * m[15] - m[0]  * m[11] * m[14] - \
                    m[8]  * m[2] * m[15] + m[8]  * m[3] * m[14] + \
                    m[12] * m[2] * m[11] - m[12] * m[3] * m[10]
            inv[9] = -m[0]  * m[9] * m[15] + m[0]  * m[11] * m[13] + \
                    m[8]  * m[1] * m[15] - m[8]  * m[3] * m[13] - \
                    m[12] * m[1] * m[11] + m[12] * m[3] * m[9]
            inv[13] = m[0]  * m[9] * m[14] - m[0]  * m[10] * m[13] - \
                    m[8]  * m[1] * m[14] + m[8]  * m[2] * m[13] + \
                    m[12] * m[1] * m[10] - m[12] * m[2] * m[9]
            inv[2] = m[1]  * m[6] * m[15] - m[1]  * m[7] * m[14] - \
                    m[5]  * m[2] * m[15] + m[5]  * m[3] * m[14] + \
                    m[13] * m[2] * m[7] - m[13] * m[3] * m[6]
            inv[6] = -m[0]  * m[6] * m[15] + m[0]  * m[7] * m[14] + \
                    m[4]  * m[2] * m[15] - m[4]  * m[3] * m[14] - \
                    m[12] * m[2] * m[7] + m[12] * m[3] * m[6]
            inv[10] = m[0]  * m[5] * m[15] - m[0]  * m[7] * m[13] - \
                    m[4]  * m[1] * m[15] + m[4]  * m[3] * m[13] + \
                    m[12] * m[1] * m[7] - m[12] * m[3] * m[5]
            inv[14] = -m[0]  * m[5] * m[14] +  m[0]  * m[6] * m[13] + \
                    m[4]  * m[1] * m[14] -  m[4]  * m[2] * m[13] - \
                    m[12] * m[1] * m[6] +  m[12] * m[2] * m[5]
            inv[3] = -m[1] * m[6] * m[11] + m[1] * m[7] * m[10] + \
                    m[5] * m[2] * m[11] - m[5] * m[3] * m[10] - \
                    m[9] * m[2] * m[7] + m[9] * m[3] * m[6]
            inv[7] = m[0] * m[6] * m[11] - m[0] * m[7] * m[10] - \
                    m[4] * m[2] * m[11] + m[4] * m[3] * m[10] + \
                    m[8] * m[2] * m[7] - m[8] * m[3] * m[6]
            inv[11] = -m[0] * m[5] * m[11] +  m[0] * m[7] * m[9] + \
                    m[4] * m[1] * m[11] -  m[4] * m[3] * m[9] - \
                    m[8] * m[1] * m[7] +  m[8] * m[3] * m[5]
            inv[15] = m[0] * m[5] * m[10] - m[0] * m[6] * m[9] - \
                    m[4] * m[1] * m[10] + m[4] * m[2] * m[9] + \
                    m[8] * m[1] * m[6] - m[8] * m[2] * m[5]

            det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12]

            if det != 0 do det = 1.0f / det

            for var i = 0 to 15
                inv[i] = inv[i] * det

            return inv

        def multMatrix(a : array of float, b : array of float) : array of GLfloat
            mtx : array of float[16] = new array of float[16]
            for var r=0 to 3
                for var c=0 to 3
                    mtx[4*r+c] = 0.0f
                    for var j=0 to 3
                        mtx[4*r+c] += (b[4*j+c] * a[4*r+j])
            return mtx

        def multMatrixVec4(mat : array of float, vec : array of float) : array of float
            res : array of float = new array of float[4]
            for var i=0 to 3
                res[i] = 0.0f
                for var j=0 to 3
                    res[i] += (mat[4*i+j] * vec[j])
            return res

        def buildPlanes(VP : array of float)
            invVP : array of float = this.inverse(VP)
            // compute the 4 corners of the frustum on the near plane
            ntl : array of float = multMatrixVec4(invVP, {-1, 1,-1, 1})
            ntr : array of float = multMatrixVec4(invVP, { 1, 1,-1, 1})
            nbl : array of float = multMatrixVec4(invVP, {-1,-1,-1, 1})
            nbr : array of float = multMatrixVec4(invVP, { 1,-1,-1, 1})
            // compute the 4 corners of the frustum on the far plane
            ftl : array of float = multMatrixVec4(invVP, {-1, 1, 1, 1})
            ftr : array of float = multMatrixVec4(invVP, { 1, 1, 1, 1})
            fbl : array of float = multMatrixVec4(invVP, {-1,-1, 1, 1})
            fbr : array of float = multMatrixVec4(invVP, { 1,-1, 1, 1})
            // compute the six planes
            planes[0] = new Plane(new soy.atoms.Vector(ntl[0]/ntl[3], ntl[1]/ntl[3], ntl[2]/ntl[3]), \
                                  new soy.atoms.Vector(ftl[0]/ftl[3], ftl[1]/ftl[3], ftl[2]/ftl[3]), \
                                  new soy.atoms.Vector(ftr[0]/ftr[3], ftr[1]/ftr[3], ftr[2]/ftr[3]))
            planes[1] = new Plane(new soy.atoms.Vector(nbl[0]/nbl[3], nbl[1]/nbl[3], nbl[2]/nbl[3]), \
                                  new soy.atoms.Vector(nbr[0]/nbr[3], nbr[1]/nbr[3], nbr[2]/nbr[3]), \
                                  new soy.atoms.Vector(fbr[0]/fbr[3], fbr[1]/fbr[3], fbr[2]/fbr[3]))
            planes[2] = new Plane(new soy.atoms.Vector(nbl[0]/nbl[3], nbl[1]/nbl[3], nbl[2]/nbl[3]), \
                                  new soy.atoms.Vector(fbl[0]/fbl[3], fbl[1]/fbl[3], fbl[2]/fbl[3]), \
                                  new soy.atoms.Vector(ftl[0]/ftl[3], ftl[1]/ftl[3], ftl[2]/ftl[3]))
            planes[3] = new Plane(new soy.atoms.Vector(ntr[0]/ntr[3], ntr[1]/ntr[3], ntr[2]/ntr[3]), \
                                  new soy.atoms.Vector(ftr[0]/ftr[3], ftr[1]/ftr[3], ftr[2]/ftr[3]), \
                                  new soy.atoms.Vector(fbr[0]/fbr[3], fbr[1]/fbr[3], fbr[2]/fbr[3]))
            planes[4] = new Plane(new soy.atoms.Vector(nbl[0]/nbl[3], nbl[1]/nbl[3], nbl[2]/nbl[3]), \
                                  new soy.atoms.Vector(ntl[0]/ntl[3], ntl[1]/ntl[3], ntl[2]/ntl[3]), \
                                  new soy.atoms.Vector(ntr[0]/ntr[3], ntr[1]/ntr[3], ntr[2]/ntr[3]))
            planes[5] = new Plane(new soy.atoms.Vector(fbr[0]/fbr[3], fbr[1]/fbr[3], fbr[2]/fbr[3]), \
                                  new soy.atoms.Vector(ftr[0]/ftr[3], ftr[1]/ftr[3], ftr[2]/ftr[3]), \
                                  new soy.atoms.Vector(ftl[0]/ftl[3], ftl[1]/ftl[3], ftl[2]/ftl[3]))

    class Chunk : Object
        _cubes : array of int[,,]
        _scene : soy.scenes.Voxelization?
        _needsRebuild : bool
        _vbo : GLuint
        _vboLod : GLuint
        _numVertices : int
        _numVerticesLod : int
        seen : bool

        struct VoxVert
            p_x : GLubyte
            p_y : GLubyte
            p_z : GLubyte
            n : GLubyte
            light : GLubyte
            c_x : GLubyte
            c_y : GLubyte
            c_z : GLubyte

        init
            _vbo = 0
            _vboLod = 0
            _cubes = new array of int[CHUNKSIZE, CHUNKSIZE, CHUNKSIZE]
            _position = new soy.atoms.Position(10,10,10)
            _scene = null
            _needsRebuild = true
            _numVertices = 0
            _numVerticesLod = 0
            seen = true

        construct (position : soy.atoms.Position,
                   scene : soy.scenes.Voxelization)
            this._position = position
            _scene = scene

        _position : soy.atoms.Position
        prop position : soy.atoms.Position
            get
                return _position
            set
                _position = value

        def getVoxelIntPos(position : soy.atoms.Position) : int
            return this.getVoxelInt((int) position.x, (int) position.y, (int) position.z)

        def getVoxelInt(x : int, y : int, z : int) : int
            if x < 0 or x >= CHUNKSIZE or y < 0 or y >= CHUNKSIZE or z < 0 or z >= CHUNKSIZE
                return _scene.getVoxelInt(
                        (int)_position.x + x,
                        (int)_position.y + y,
                        (int)_position.z + z
                    )
            return _cubes[x, y, z]

        def setVoxelIntPos(position : soy.atoms.Position, id : int)
            this.setVoxelInt((int) position.x, (int) position.y, (int) position.z, id)

        def setVoxelInt(x : int, y : int, z : int, id : int)
            if x < 0 or x >= CHUNKSIZE or y < 0 or y >= CHUNKSIZE or z < 0 or z >= CHUNKSIZE
                _scene.getVoxelInt(
                    (int)_position.x + x,
                    (int)_position.y + y,
                    (int)_position.z + z
                )
            _cubes[x, y, z] = id
            _needsRebuild = true

        def buildMesh()
            if !_needsRebuild do return
            _needsRebuild = false

            if _vbo == 0
                buffers : array of GLuint = {0, 0}
                glGenBuffers(buffers)
                _vbo = buffers[0]
                _vboLod = buffers[1]

            vertices : list of VoxVert? = new list of VoxVert?
            verticesLod : list of VoxVert? = new list of VoxVert?
            calcLightSum()
            avgcolor : array of float = {0.0f, 0.0f, 0.0f, 1.0f}
            avgcolorObj : soy.atoms.Color = new soy.atoms.Color.named("red")
            for var x = 0 to (CHUNKSIZE - 1)
                for var y = 0 to (CHUNKSIZE - 1)
                    for var z = 0 to (CHUNKSIZE - 1)
                        pushCubeToArray(x, y, z, vertices)
                        if x%2 == 0 && y%2 == 0 && z%2 == 0
                            for var i = 0 to 2
                                avgcolor[i] = 0.0f
                            numBlocks : int = 0
                            for var x2 = x to (x+1)
                                for var y2 = y to (y+1)
                                    for var z2 = z to (z+1)
                                        v : int = getVoxelInt(x2, y2, z2)
                                        if getVoxelInt(x2, y2, z2) > 0
                                            numBlocks += 1
                                            col2 : array of float = voxelColors[v-1].get4f()
                                            for var i = 0 to 2
                                                avgcolor[i] += col2[i]
                            if numBlocks >= 4
                                for var i = 0 to 2
                                    avgcolor[i] /= numBlocks
                                avgcolorObj.set4f(avgcolor)
                                pushLodToArray(x, y, z, avgcolorObj, verticesLod)
            _numVertices = vertices.size
            _numVerticesLod = verticesLod.size
            index : int = 0
            //transform into flat array of GLubyte. 8 GLubyte per vert
            data : array of GLubyte = new array of GLubyte[vertices.size*NUM_VOXEL_VERT_FLOATS]
            for i in vertices
                data[index] = i.p_x
                data[index+1] = i.p_y
                data[index+2] = i.p_z
                data[index+3] = i.n
                data[index+4] = i.light
                data[index+5] = i.c_x
                data[index+6] = i.c_y
                data[index+7] = i.c_z
                index += NUM_VOXEL_VERT_FLOATS
            indexLod : int = 0
            //transform into flat array of GLubyte. 8 GLubyte per vert
            dataLod : array of GLubyte = new array of GLubyte[verticesLod.size*NUM_VOXEL_VERT_FLOATS]
            for i in verticesLod
                dataLod[indexLod] = i.p_x
                dataLod[indexLod+1] = i.p_y
                dataLod[indexLod+2] = i.p_z
                dataLod[indexLod+3] = i.n
                dataLod[indexLod+4] = i.light
                dataLod[indexLod+5] = i.c_x
                dataLod[indexLod+6] = i.c_y
                dataLod[indexLod+7] = i.c_z
                indexLod += NUM_VOXEL_VERT_FLOATS

            glBindBuffer(GL_ARRAY_BUFFER, _vbo)
            glBufferData(GL_ARRAY_BUFFER,
                        (GLsizei) (data.length*sizeof(GLubyte)),
                        data,
                        GL_STATIC_DRAW)
            glBindBuffer(GL_ARRAY_BUFFER, _vboLod)
            glBufferData(GL_ARRAY_BUFFER,
                        (GLsizei) (dataLod.length*sizeof(GLubyte)),
                        dataLod,
                        GL_STATIC_DRAW)

        def pushCubeToArray(x : uchar, y : uchar, z : uchar, data : list of VoxVert?)
            cubeID : int
            cubeID = _cubes[x,y,z]
            if cubeID == 0 do return
            color : array of uchar = voxelColors[cubeID-1].get4ub()
            if getVoxelInt(x, y, z+1) == 0 // front face
                data.add({x+1, y+1, z+1, 0, calcLight(x+1, y+1, z+1, 0, 0, 1), color[0], color[1], color[2]})
                data.add({x  , y+1, z+1, 0, calcLight(x  , y+1, z+1, 0, 0, 1), color[0], color[1], color[2]})
                data.add({x+1, y  , z+1, 0, calcLight(x+1, y  , z+1, 0, 0, 1), color[0], color[1], color[2]})
                data.add({x  , y  , z+1, 0, calcLight(x  , y  , z+1, 0, 0, 1), color[0], color[1], color[2]})
                data.add({x+1, y  , z+1, 0, calcLight(x+1, y  , z+1, 0, 0, 1), color[0], color[1], color[2]})
                data.add({x  , y+1, z+1, 0, calcLight(x  , y+1, z+1, 0, 0, 1), color[0], color[1], color[2]})

            if getVoxelInt(x, y, z-1) == 0 // back face
                data.add({x+1, y  , z  , 1, calcLight(x+1, y  , z  , 0, 0,-1), color[0], color[1], color[2]})
                data.add({x  , y+1, z  , 1, calcLight(x  , y+1, z  , 0, 0,-1), color[0], color[1], color[2]})
                data.add({x+1, y+1, z  , 1, calcLight(x+1, y+1, z  , 0, 0,-1), color[0], color[1], color[2]})
                data.add({x  , y  , z  , 1, calcLight(x  , y  , z  , 0, 0,-1), color[0], color[1], color[2]})
                data.add({x  , y+1, z  , 1, calcLight(x  , y+1, z  , 0, 0,-1), color[0], color[1], color[2]})
                data.add({x+1, y  , z  , 1, calcLight(x+1, y  , z  , 0, 0,-1), color[0], color[1], color[2]})

            if getVoxelInt(x+1, y, z) == 0 // left face
                data.add({x+1, y  , z+1, 2, calcLight(x+1, y  , z+1, 1, 0, 0), color[0], color[1], color[2]})
                data.add({x+1, y  , z  , 2, calcLight(x+1, y  , z  , 1, 0, 0), color[0], color[1], color[2]})
                data.add({x+1, y+1, z+1, 2, calcLight(x+1, y+1, z+1, 1, 0, 0), color[0], color[1], color[2]})
                data.add({x+1, y  , z  , 2, calcLight(x+1, y  , z  , 1, 0, 0), color[0], color[1], color[2]})
                data.add({x+1, y+1, z  , 2, calcLight(x+1, y+1, z  , 1, 0, 0), color[0], color[1], color[2]})
                data.add({x+1, y+1, z+1, 2, calcLight(x+1, y+1, z+1, 1, 0, 0), color[0], color[1], color[2]})

            if getVoxelInt(x-1, y, z) == 0 // right face
                data.add({x  , y  , z+1, 3, calcLight(x  , y  , z+1,-1, 0, 0), color[0], color[1], color[2]})
                data.add({x  , y+1, z+1, 3, calcLight(x  , y+1, z+1,-1, 0, 0), color[0], color[1], color[2]})
                data.add({x  , y  , z  , 3, calcLight(x  , y  , z  ,-1, 0, 0), color[0], color[1], color[2]})
                data.add({x  , y+1, z+1, 3, calcLight(x  , y+1, z+1,-1, 0, 0), color[0], color[1], color[2]})
                data.add({x  , y+1, z  , 3, calcLight(x  , y+1, z  ,-1, 0, 0), color[0], color[1], color[2]})
                data.add({x  , y  , z  , 3, calcLight(x  , y  , z  ,-1, 0, 0), color[0], color[1], color[2]})

            if getVoxelInt(x, y-1, z) == 0 // bottom face
                data.add({x+1, y  , z  , 4, calcLight(x+1, y  , z  , 0,-1, 0), color[0], color[1], color[2]})
                data.add({x  , y  , z+1, 4, calcLight(x  , y  , z+1, 0,-1, 0), color[0], color[1], color[2]})
                data.add({x  , y  , z  , 4, calcLight(x  , y  , z  , 0,-1, 0), color[0], color[1], color[2]})
                data.add({x+1, y  , z  , 4, calcLight(x+1, y  , z  , 0,-1, 0), color[0], color[1], color[2]})
                data.add({x+1, y  , z+1, 4, calcLight(x+1, y  , z+1, 0,-1, 0), color[0], color[1], color[2]})
                data.add({x  , y  , z+1, 4, calcLight(x  , y  , z+1, 0,-1, 0), color[0], color[1], color[2]})

            if getVoxelInt(x, y+1, z) == 0 //top face
                data.add({x+1, y+1, z  , 5, calcLight(x+1, y+1, z  , 0, 1, 0), color[0], color[1], color[2]})
                data.add({x  , y+1, z  , 5, calcLight(x  , y+1, z  , 0, 1, 0), color[0], color[1], color[2]})
                data.add({x  , y+1, z+1, 5, calcLight(x  , y+1, z+1, 0, 1, 0), color[0], color[1], color[2]})
                data.add({x+1, y+1, z  , 5, calcLight(x+1, y+1, z  , 0, 1, 0), color[0], color[1], color[2]})
                data.add({x  , y+1, z+1, 5, calcLight(x  , y+1, z+1, 0, 1, 0), color[0], color[1], color[2]})
                data.add({x+1, y+1, z+1, 5, calcLight(x+1, y+1, z+1, 0, 1, 0), color[0], color[1], color[2]})

        def pushLodToArray(x : uchar, y : uchar, z : uchar, avgcolor : soy.atoms.Color, data : list of VoxVert?)
            color : array of uchar = avgcolor.get4ub()
            if not isLodFull(x, y, z+2) // front face
                data.add({x+2, y+2, z+2, 0, calcLightLod(x+2, y+2, z+2, 0, 0, 1), color[0], color[1], color[2]})
                data.add({x  , y+2, z+2, 0, calcLightLod(x  , y+2, z+2, 0, 0, 1), color[0], color[1], color[2]})
                data.add({x+2, y  , z+2, 0, calcLightLod(x+2, y  , z+2, 0, 0, 1), color[0], color[1], color[2]})
                data.add({x  , y  , z+2, 0, calcLightLod(x  , y  , z+2, 0, 0, 1), color[0], color[1], color[2]})
                data.add({x+2, y  , z+2, 0, calcLightLod(x+2, y  , z+2, 0, 0, 1), color[0], color[1], color[2]})
                data.add({x  , y+2, z+2, 0, calcLightLod(x  , y+2, z+2, 0, 0, 1), color[0], color[1], color[2]})

            if not isLodFull(x, y, z-2) // back face
                data.add({x+2, y  , z  , 1, calcLightLod(x+2, y  , z  , 0, 0,-1), color[0], color[1], color[2]})
                data.add({x  , y+2, z  , 1, calcLightLod(x  , y+2, z  , 0, 0,-1), color[0], color[1], color[2]})
                data.add({x+2, y+2, z  , 1, calcLightLod(x+2, y+2, z  , 0, 0,-1), color[0], color[1], color[2]})
                data.add({x  , y  , z  , 1, calcLightLod(x  , y  , z  , 0, 0,-1), color[0], color[1], color[2]})
                data.add({x  , y+2, z  , 1, calcLightLod(x  , y+2, z  , 0, 0,-1), color[0], color[1], color[2]})
                data.add({x+2, y  , z  , 1, calcLightLod(x+2, y  , z  , 0, 0,-1), color[0], color[1], color[2]})

            if not isLodFull(x+2, y, z) // left face
                data.add({x+2, y  , z+2, 2, calcLightLod(x+2, y  , z+2, 1, 0, 0), color[0], color[1], color[2]})
                data.add({x+2, y  , z  , 2, calcLightLod(x+2, y  , z  , 1, 0, 0), color[0], color[1], color[2]})
                data.add({x+2, y+2, z+2, 2, calcLightLod(x+2, y+2, z+2, 1, 0, 0), color[0], color[1], color[2]})
                data.add({x+2, y  , z  , 2, calcLightLod(x+2, y  , z  , 1, 0, 0), color[0], color[1], color[2]})
                data.add({x+2, y+2, z  , 2, calcLightLod(x+2, y+2, z  , 1, 0, 0), color[0], color[1], color[2]})
                data.add({x+2, y+2, z+2, 2, calcLightLod(x+2, y+2, z+2, 1, 0, 0), color[0], color[1], color[2]})

            if not isLodFull(x-2, y, z) // right face
                data.add({x  , y  , z+2, 3, calcLightLod(x  , y  , z+2,-1, 0, 0), color[0], color[1], color[2]})
                data.add({x  , y+2, z+2, 3, calcLightLod(x  , y+2, z+2,-1, 0, 0), color[0], color[1], color[2]})
                data.add({x  , y  , z  , 3, calcLightLod(x  , y  , z  ,-1, 0, 0), color[0], color[1], color[2]})
                data.add({x  , y+2, z+2, 3, calcLightLod(x  , y+2, z+2,-1, 0, 0), color[0], color[1], color[2]})
                data.add({x  , y+2, z  , 3, calcLightLod(x  , y+2, z  ,-1, 0, 0), color[0], color[1], color[2]})
                data.add({x  , y  , z  , 3, calcLightLod(x  , y  , z  ,-1, 0, 0), color[0], color[1], color[2]})

            if not isLodFull(x, y-2, z) // bottom face
                data.add({x+2, y  , z  , 4, calcLightLod(x+2, y  , z  , 0,-1, 0), color[0], color[1], color[2]})
                data.add({x  , y  , z+2, 4, calcLightLod(x  , y  , z+2, 0,-1, 0), color[0], color[1], color[2]})
                data.add({x  , y  , z  , 4, calcLightLod(x  , y  , z  , 0,-1, 0), color[0], color[1], color[2]})
                data.add({x+2, y  , z  , 4, calcLightLod(x+2, y  , z  , 0,-1, 0), color[0], color[1], color[2]})
                data.add({x+2, y  , z+2, 4, calcLightLod(x+2, y  , z+2, 0,-1, 0), color[0], color[1], color[2]})
                data.add({x  , y  , z+2, 4, calcLightLod(x  , y  , z+2, 0,-1, 0), color[0], color[1], color[2]})

            if not isLodFull(x, y+2, z) // top face
                data.add({x+2, y+2, z  , 5, calcLightLod(x+2, y+2, z  , 0, 1, 0), color[0], color[1], color[2]})
                data.add({x  , y+2, z  , 5, calcLightLod(x  , y+2, z  , 0, 1, 0), color[0], color[1], color[2]})
                data.add({x  , y+2, z+2, 5, calcLightLod(x  , y+2, z+2, 0, 1, 0), color[0], color[1], color[2]})
                data.add({x+2, y+2, z  , 5, calcLightLod(x+2, y+2, z  , 0, 1, 0), color[0], color[1], color[2]})
                data.add({x  , y+2, z+2, 5, calcLightLod(x  , y+2, z+2, 0, 1, 0), color[0], color[1], color[2]})
                data.add({x+2, y+2, z+2, 5, calcLightLod(x+2, y+2, z+2, 0, 1, 0), color[0], color[1], color[2]})

        def isLodFull(x : int, y : int, z : int) : bool
            numBlocks : int = 0
            for var x2 = x to (x+1)
                for var y2 = y to (y+1)
                    for var z2 = z to (z+1)
                        if getVoxelInt(x2, y2, z2) > 0
                            numBlocks += 1
            return (numBlocks >= 4)

        def calcLightSum() : void
            for var x = 0 to (LIGHTSUM_SIZE-1)
                for var y = 0 to (LIGHTSUM_SIZE-1)
                    for var z = 0 to (LIGHTSUM_SIZE-1)
                        res : int = getVoxelInt(x-AO_MAX_RAD, y-AO_MAX_RAD, z-AO_MAX_RAD)
                        lightSum[x,y,z] = res == 0 ? 1 : 0
            for var x = 1 to (LIGHTSUM_SIZE-1)
                for var y = 0 to (LIGHTSUM_SIZE-1)
                    for var z = 0 to (LIGHTSUM_SIZE-1)
                        lightSum[x,y,z] += lightSum[x-1,y,z]
            for var x = 0 to (LIGHTSUM_SIZE-1)
                for var y = 1 to (LIGHTSUM_SIZE-1)
                    for var z = 0 to (LIGHTSUM_SIZE-1)
                        lightSum[x,y,z] += lightSum[x,y-1,z]
            for var x = 0 to (LIGHTSUM_SIZE-1)
                for var y = 0 to (LIGHTSUM_SIZE-1)
                    for var z = 1 to (LIGHTSUM_SIZE-1)
                        lightSum[x,y,z] += lightSum[x,y,z-1]

        def sumRect(x1 : int, y1 : int, z1 : int, x2 : int, y2 : int, z2 : int) : int
            if x1 > x2
                temp : int = x1
                x1 = x2
                x2 = temp
            if y1 > y2
                temp : int = y1
                y1 = y2
                y2 = temp
            if z1 > z2
                temp : int = z1
                z1 = z2
                z2 = temp

            x1 -= 1
            y1 -= 1
            z1 -= 1

            x1 += AO_MAX_RAD
            y1 += AO_MAX_RAD
            z1 += AO_MAX_RAD
            x2 += AO_MAX_RAD
            y2 += AO_MAX_RAD
            z2 += AO_MAX_RAD

            result : int = (lightSum[x2,y2,z2]
                            - lightSum[x2,y2,z1] - lightSum[x2,y1,z2] - lightSum[x1,y2,z2]
                            + lightSum[x2,y1,z1] + lightSum[x1,y2,z1] + lightSum[x1,y1,z2]
                            - lightSum[x1,y1,z1])
            return result

        def calcSubLight(x : int, y : int, z : int, dx : int, dy : int, dz : int, d : int) : float
            x1 : int = dx ==  1 ? x : x-d
            x2 : int = dx == -1 ? x : x+d
            y1 : int = dy ==  1 ? y : y-d
            y2 : int = dy == -1 ? y : y+d
            z1 : int = dz ==  1 ? z : z-d
            z2 : int = dz == -1 ? z : z+d

            a : float = sumRect(x1, y1, z1, x2-1, y2-1, z2-1);
            b : float = d*d*d*4
            return  a/b

        def calcLight(x : int, y : int, z : int, dx : int, dy : int, dz : int) : uchar
            light : float = 0;
            light += calcSubLight(x, y, z, dx, dy, dz, 1) * 0.5f
            light += calcSubLight(x, y, z, dx, dy, dz, 2) * 0.25f
            light += calcSubLight(x, y, z, dx, dy, dz, 4) * 0.15f
            light += calcSubLight(x, y, z, dx, dy, dz, 8) * 0.1f
            light *= light
            return (uchar)(255*light)

        def calcLightLod(x : int, y : int, z : int, dx : int, dy : int, dz : int) : uchar
            light : float = 0;
            light += calcSubLight(x, y, z, dx, dy, dz, 2) * 0.5f
            light += calcSubLight(x, y, z, dx, dy, dz, 4) * 0.25f
            light += calcSubLight(x, y, z, dx, dy, dz, 6) * 0.15f
            light += calcSubLight(x, y, z, dx, dy, dz, 8) * 0.1f
            light *= light
            return (uchar)(255*light)

        def render(view : array of GLfloat, uniform_loc : int, distance : float)
            if _numVertices == 0 do return
            //if seen == false do return
            vbo : GLuint = _vboLod
            numVert : int = _numVerticesLod

            if distance < 30000
                vbo = _vbo
                numVert = _numVertices

            mv : array of GLfloat = new array of GLfloat[16]

            for var i = 0 to 15
                mv[i] = view[i]

            mv[12] += (int) position.x
            mv[13] += (int) position.y
            mv[14] += (int) position.z

            glUniformMatrix4fv(uniform_loc, 1, GL_FALSE, mv)

            glBindBuffer(GL_ARRAY_BUFFER, vbo)

            glVertexAttribPointer(0, 3, GL_UNSIGNED_BYTE, GL_FALSE, (GLsizei)
                                  (sizeof(GLubyte) * NUM_VOXEL_VERT_FLOATS), null)
            glVertexAttribPointer(1, 1, GL_UNSIGNED_BYTE, GL_FALSE, (GLsizei)
                                  (sizeof(GLubyte) * NUM_VOXEL_VERT_FLOATS), (GLvoid*)
                                  (sizeof(GLubyte) * 3))
            glVertexAttribPointer(2, 1, GL_UNSIGNED_BYTE, GL_TRUE, (GLsizei)
                                  (sizeof(GLubyte) * NUM_VOXEL_VERT_FLOATS), (GLvoid*)
                                  (sizeof(GLubyte) * 4))
            glVertexAttribPointer(3, 3, GL_UNSIGNED_BYTE, GL_TRUE, (GLsizei)
                                  (sizeof(GLubyte) * NUM_VOXEL_VERT_FLOATS), (GLvoid*)
                                  (sizeof(GLubyte) * 5))

            glDrawArrays(GL_TRIANGLES, 0, numVert)

    _chunks : array of Chunk[,,]
    _material : soy.materials.Voxelmat?
    _frustum : Frustum?

    init
        lightSum = new array of int[34,34,34]
        voxelColors = new list of soy.atoms.Color
        voxelColors.add(new soy.atoms.Color.named("saddlebrown"))
        voxelColors.add(new soy.atoms.Color.named("lawngreen"))
        _frustum = new Frustum()
        _chunks = new array of Chunk[WORLDSIZE, WORLDHEIGHT, WORLDSIZE]
        _material = new soy.materials.Voxelmat("white")
        for var x = 0 to (WORLDSIZE - 1)
            for var z = 0 to (WORLDSIZE - 1)
                for var y = 0 to (WORLDHEIGHT - 1)
                    _chunks[x,(WORLDHEIGHT - 1 - y),z] = new Chunk(new soy.atoms.Position(x*CHUNKSIZE, (WORLDHEIGHT - 1 - y)*CHUNKSIZE, z*CHUNKSIZE), this)

        for var x = 0 to (WORLDSIZE*CHUNKSIZE - 1)
            for var z = 0 to (WORLDSIZE*CHUNKSIZE - 1)
                for var y = 0 to (WORLDHEIGHT*CHUNKSIZE - 1)
                    y2 : int = (WORLDHEIGHT*CHUNKSIZE - 1) - y
                    data : int = this._getMockData(x, y2, z)
                    this.setVoxelInt(x, y2, z, data != 0 ? (getVoxelInt(x, y2 + 1, z) > 0 ? 1 : 2) : 0)

    ////////////////////////////////////////////////////////////////////////
    // Methods

    def override render_extra (view : array of GLfloat,
                               projection : array of GLfloat,
                               camera : soy.bodies.Camera,
                               lights : array of soy.bodies.Light)
        i : int = 0
        loc : int = -1
        _frustum.buildPlanes(_frustum.multMatrix(projection, view))
        for var x = 0 to (WORLDSIZE - 1)
            for var y = 0 to (WORLDHEIGHT - 1)
                for var z = 0 to (WORLDSIZE - 1)
                    c : Chunk? = _chunks[x,y,z]
                    radius : float = (float)sqrt(3*pow(CHUNKSIZE*0.5,2))
                    if c != null
                        c.seen = _frustum.test(new soy.atoms.Vector(c.position.x+radius, c.position.y+radius, c.position.z+radius), radius)

        while _material.enable(i, view, view, projection, lights, ambient.get4f(), fog.get4f()) == true
            if loc == -1
                loc = _material.getLocForMV()

            glEnableVertexAttribArray(0)
            glEnableVertexAttribArray(1)
            glEnableVertexAttribArray(2)
            glEnableVertexAttribArray(3)
            for var x = 0 to (WORLDSIZE - 1)
                for var y = 0 to (WORLDHEIGHT - 1)
                    for var z = 0 to (WORLDSIZE - 1)
                        if _chunks[x,y,z] != null
                            distance : float = _chunks[x,y,z].position.distance_squared(camera.position)
                            _chunks[x,y,z].buildMesh()
                            _chunks[x,y,z].render(view, loc, distance)
            glDisableVertexAttribArray(3)
            glDisableVertexAttribArray(2)
            glDisableVertexAttribArray(1)
            glDisableVertexAttribArray(0)
            i += 1

    def getVoxelAtomPos(position : soy.atoms.Position) : soy.atoms.Voxel?
        return getVoxelAtom((int) position.x, (int) position.y, (int) position.z)

    def getVoxelAtom(x : int, y : int, z : int) : soy.atoms.Voxel?
        id : int = getVoxelInt(x, y, z)
        if id is 0 do return null
        return new soy.atoms.Voxel(voxelColors[id-1])

    def getVoxelIntPos(position : soy.atoms.Position) : int
        return getVoxelInt((int) position.x, (int) position.y, (int) position.z)

    def getVoxelInt(x : int, y : int, z : int) : int
        chunkX : int = x>>CHUNKSIZE_POW2
        chunkY : int = y>>CHUNKSIZE_POW2
        chunkZ : int = z>>CHUNKSIZE_POW2

        if chunkX < 0 or chunkX >= WORLDSIZE or chunkY < 0 or chunkY >= WORLDHEIGHT or chunkZ < 0 or chunkZ >= WORLDSIZE
            return 0

        cubeX : int = x&CHUNKSIZE_MASK
        cubeY : int = y&CHUNKSIZE_MASK
        cubeZ : int = z&CHUNKSIZE_MASK

        if _chunks[chunkX, chunkY, chunkZ] != null
            return _chunks[chunkX, chunkY, chunkZ].getVoxelInt(cubeX, cubeY, cubeZ)
        else
            return 0

    def setVoxelAtomPos(position : soy.atoms.Position, vox : soy.atoms.Voxel)
        setVoxelAtom((int) position.x, (int) position.y, (int) position.z, vox)

    def setVoxelAtom(x : int, y : int, z : int, vox : soy.atoms.Voxel)
        index : int = -1
        for var i = 0 to (voxelColors.size-1)
            if vox.color == voxelColors[i]
                index = i+1
                break
        if index == -1
            index = voxelColors.size+1
            voxelColors.add(vox.color)
        this.setVoxelInt(x, y, z, index)

    def setVoxelIntPos(position : soy.atoms.Position, id : int)
        setVoxelInt((int) position.x, (int) position.y, (int) position.z, id)

    def setVoxelInt(x : int, y : int, z : int, id : int)
        chunkX : int = x>>CHUNKSIZE_POW2
        chunkY : int = y>>CHUNKSIZE_POW2
        chunkZ : int = z>>CHUNKSIZE_POW2

        if chunkX < 0 or chunkX >= WORLDSIZE or chunkY < 0 or chunkY >= WORLDHEIGHT or chunkZ < 0 or chunkZ >= WORLDSIZE
            return

        if id > voxelColors.size do return

        cubeX : int = x&CHUNKSIZE_MASK
        cubeY : int = y&CHUNKSIZE_MASK
        cubeZ : int = z&CHUNKSIZE_MASK

        if _chunks[chunkX, chunkY, chunkZ] != null
            _chunks[chunkX, chunkY, chunkZ].setVoxelInt(cubeX, cubeY, cubeZ, id)
        else
            return

    def _getMockData(x : int, y : int, z : int) : int
        offset : int = (CHUNKSIZE*WORLDSIZE)/2
        scale : float = 0.08f
        x2 : int = x - offset
        z2 : int = z - offset
        if y-20 < sin(sqrt(x2*x2*scale+z2*z2*scale))*3
            return 1
        return 0
