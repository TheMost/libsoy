/*
 *  libsoy - soy.fields.Field
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GLib
    ode

class soy.fields.Field : Object
    /*
    soy.fields.Field

    Fields invisible cubodial regions in  space that apply forces and other
    changes to other bodies if they are in their region.
    */

    //TODO: find better way to define a region of filed in a scene
    //true when a region is defines
    region : bool

    //coordinates of the region.
    //lower and upper limit coordinates for each dimension
    rx1: float   //lower limit
    rx2: float   //upperlimit
    ry1: float   //lower limit
    ry2: float   //upperlimit
    rz1: float   //lower limit
    rz2: float   //upperlimit
    construct ( ) // TODO position
        region = false
        rx1=rx2=ry1=ry2=rz1=rz2=0;
        position = new soy.atoms.Position(0,0,0)

    //Methods

    //defines the region of the field
    def addRegion(x1:float,x2:float,
                  y1:float,y2:float,
                  z1:float,z2:float)
        region = true
        x:float = (x1+x2)/2.0f
        y:float = (y1+y2)/2.0f
        z:float = (z1+z2)/2.0f
        position = new soy.atoms.Position(x,y,z)
        self.rx1=x1
        self.rx2=x2
        self.ry1=y1
        self.ry2=y2
        self.rz1=z1
        self.rz2=z2
        self.region_changed()

    def inside_region(other : soy.bodies.Body) : bool
        if !region
            return true
        if other.position.x<rx2 && other.position.x>rx1
            if other.position.y<ry2 && other.position.y>ry1
                if other.position.z<rz2 && other.position.z>rz1
                    return true
        return false

    def apply() : bool
        return true


    // This function is applied once per physics cycle for each combination of affected body and field.
    def virtual exert(other : soy.bodies.Body) : bool
        return true

    // This function is called by the physics cycle for each field before
    // any calls to _exert.  It is used for any initialization or cleanup that
    // the Field requires.
    def virtual give(data : int)
        return

    // This function is to called by the physics cycle after every call _exert
    // has been done.
    def virtual commit()
        return

    //This function is called when position is changed
    def virtual position_changed(value : soy.atoms.Position?)
        pass

    def virtual region_changed()
        pass

    //position of the monopoles
    rposition : soy.atoms.Position
    prop new position : soy.atoms.Position?
        get
            return self.rposition
        set
            rx1 = rx1+value.x
            rx2 = rx2+value.x
            ry1 = ry1+value.y
            ry2 = ry2+value.y
            rz1 = rz1+value.z
            rz2 = rz2+value.z
            self.rposition = value
