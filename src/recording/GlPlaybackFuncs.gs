/*
 *  libsoy - soy.recording.GlPlaybackFuncs.gs
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses GL

class soy.recording.PlaybackFuncs : Object
        def static playbackGlActiveTexture(cmd : EncodedCmd)
            GL.raw_glActiveTexture((GLenum)int.parse(cmd.parameters[0] ))
            return
        def static playbackGlAttachShader(cmd : EncodedCmd)
            GL.raw_glAttachShader((GLuint)int.parse(cmd.parameters[0]), (GLuint)int.parse(cmd.parameters[1]) )
            return
        def static playbackGlBindAttribLocation(cmd : EncodedCmd)
            GL.raw_glBindAttribLocation( (GLuint)int.parse(cmd.parameters[0]),(GLuint)int.parse(cmd.parameters[1]), cmd.parameters[2])
            return
        def static playbackGlBindBuffer(cmd : EncodedCmd)
            GL.raw_glBindBuffer((GLenum)int.parse(cmd.parameters[0]), (GLuint)int.parse(cmd.parameters[1]))
            return
        def static playbackGlBindFramebuffer(cmd : EncodedCmd)
            GL.raw_glBindFramebuffer((GLenum)int.parse(cmd.parameters[0]), (GLuint)int.parse(cmd.parameters[1]))
            return
        def static playbackGlBindRenderbuffer(cmd : EncodedCmd)
            GL.raw_glBindRenderbuffer((GLenum)int.parse(cmd.parameters[0]), (GLuint)int.parse(cmd.parameters[1]))
            return
        def static playbackGlBindTexture(cmd : EncodedCmd)
            GL.raw_glBindTexture((GLenum)int.parse(cmd.parameters[0]), (GLuint)int.parse(cmd.parameters[1]))
            return
        def static playbackGlBlendColor(cmd : EncodedCmd)
            GL.raw_glBlendColor( (GLclampf)double.parse(cmd.parameters[0]) , (GLclampf)double.parse(cmd.parameters[1]), (GLclampf)double.parse(cmd.parameters[2]), (GLclampf)double.parse(cmd.parameters[3])    )
            return
        def static playbackGlBlendEquation(cmd : EncodedCmd)
            GL.raw_glBlendEquation((GLenum)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlBlendEquationSeparate(cmd : EncodedCmd)
            GL.raw_glBlendEquationSeparate((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]) )
            return
        def static playbackGlBlendFunc(cmd : EncodedCmd)
            GL.raw_glBlendFunc((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]) )
            return
        def static playbackGlBlendFuncSeparate(cmd : EncodedCmd)
            GL.raw_glBlendFuncSeparate((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), (GLenum)int.parse(cmd.parameters[2]), (GLenum)int.parse(cmd.parameters[3]) )
            return
        def static playbackGlBufferData(cmd : EncodedCmd)
            rett : array of char
            PointerSerializer.deserializeglBufferData(cmd, out rett)
            GL.raw_glBufferData((GLenum)int.parse(cmd.parameters[0]),(GLsizeiptr)int.parse(cmd.parameters[1])  ,rett,(GLenum)int.parse(cmd.parameters[3]) )
            return
        def static playbackGlBufferSubData(cmd : EncodedCmd)
            assert(false)
            GL.raw_glBufferSubData((GLenum)int.parse(cmd.parameters[0]), (GLintptr)int.parse(cmd.parameters[1])  ,(GLsizeiptr)int.parse(cmd.parameters[2])  , (GLvoid*)0 )
            return
        def static playbackGlCheckFramebufferStatus(cmd : EncodedCmd)
            GL.raw_glCheckFramebufferStatus((GLenum)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlClear(cmd : EncodedCmd)
            GL.raw_glClear((GLbitfield)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlClearColor(cmd : EncodedCmd)
            GL.raw_glClearColor( (GLclampf)double.parse(cmd.parameters[0]), (GLclampf)double.parse(cmd.parameters[1]), (GLclampf)double.parse(cmd.parameters[2]), (GLclampf)double.parse(cmd.parameters[3]) )
            return
        def static playbackGlClearDepthf(cmd : EncodedCmd)
            GL.raw_glClearDepthf( (GLclampf)double.parse(cmd.parameters[0]) )
            return
        def static playbackGlClearStencil(cmd : EncodedCmd)
            GL.raw_glClearStencil( (GLint)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlColorMask(cmd : EncodedCmd)
            GL.raw_glColorMask( (GLboolean)bool.parse(cmd.parameters[0]), (GLboolean)bool.parse(cmd.parameters[1]), (GLboolean)bool.parse(cmd.parameters[2]), (GLboolean)bool.parse(cmd.parameters[3])    )
            return
        def static playbackGlCompileShader(cmd : EncodedCmd)
            GL.raw_glCompileShader( (GLuint)int.parse(cmd.parameters[0]))
            return
        def static playbackGlCompressedTexImage2D(cmd : EncodedCmd)
            assert(false)
            GL.raw_glCompressedTexImage2D( (GLenum)int.parse(cmd.parameters[0]), (GLint)int.parse(cmd.parameters[1]) , (GLenum)int.parse(cmd.parameters[2]), (GLsizei)int.parse(cmd.parameters[3]),(GLsizei)int.parse(cmd.parameters[4]), (GLint)int.parse(cmd.parameters[5]), (GLsizei)int.parse(cmd.parameters[6]), (GLvoid*)0)
            return
        def static playbackGlCompressedTexSubImage2D(cmd : EncodedCmd)
            assert(false)
            GL.raw_glCompressedTexSubImage2D((GLenum)int.parse(cmd.parameters[0]),(GLint)int.parse(cmd.parameters[1])  ,(GLint)int.parse(cmd.parameters[2])  ,(GLint)int.parse(cmd.parameters[3])  ,(GLsizei)int.parse(cmd.parameters[4]), (GLsizei)int.parse(cmd.parameters[5]), (GLenum)int.parse(cmd.parameters[6]),(GLsizei)int.parse(cmd.parameters[7]), (GLvoid*)0 )
            return
        def static playbackGlCopyTexImage2D(cmd : EncodedCmd)
            GL.raw_glCopyTexImage2D((GLenum)int.parse(cmd.parameters[0]),(GLint)int.parse(cmd.parameters[1])  , (GLenum)int.parse(cmd.parameters[2]), (GLint)int.parse(cmd.parameters[3]), (GLint)int.parse(cmd.parameters[4]),(GLsizei)int.parse(cmd.parameters[5])  ,(GLsizei)int.parse(cmd.parameters[6])  , (GLint)int.parse(cmd.parameters[7]))
            return
        def static playbackGlCopyTexSubImage2D(cmd : EncodedCmd)
            GL.raw_glCopyTexSubImage2D((GLenum)int.parse(cmd.parameters[0]),(GLint)int.parse(cmd.parameters[1]), (GLint)int.parse(cmd.parameters[2]), (GLint)int.parse(cmd.parameters[3]), (GLint)int.parse(cmd.parameters[4]),(GLint)int.parse(cmd.parameters[5]) , (GLsizei)int.parse(cmd.parameters[6])  ,(GLsizei)int.parse(cmd.parameters[7])   )
            return
        def static playbackGlCreateProgram(cmd : EncodedCmd)
            GL.raw_glCreateProgram()
            return
        def static playbackGlCreateShader(cmd : EncodedCmd)
            var a = GL.raw_glCreateShader((GLenum)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlCullFace(cmd : EncodedCmd)
            GL.raw_glCullFace((GLenum)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlDeleteBuffers(cmd : EncodedCmd)
            GL.raw_glDeleteBuffers(TypeSerializer.deserializeGLuintArr(cmd.parameters[0]))
            return
        def static playbackGlDeleteFramebuffers(cmd : EncodedCmd)
            GL.raw_glDeleteFramebuffers(TypeSerializer.deserializeGLuintArr(cmd.parameters[0]) )
            return
        def static playbackGlDeleteProgram(cmd : EncodedCmd)
            GL.raw_glDeleteProgram((GLuint)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlDeleteRenderbuffers(cmd : EncodedCmd)
            GL.raw_glDeleteRenderbuffers(TypeSerializer.deserializeGLuintArr(cmd.parameters[0]) )
            return
        def static playbackGlDeleteShader(cmd : EncodedCmd)
            GL.raw_glDeleteShader((GLuint)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlDeleteTextures(cmd : EncodedCmd)
            GL.raw_glDeleteTextures(TypeSerializer.deserializeGLuintArr(cmd.parameters[0]) )
            return
        def static playbackGlDepthFunc(cmd : EncodedCmd)
            GL.raw_glDepthFunc( (GLenum)int.parse(cmd.parameters[0]))
            return
        def static playbackGlDepthMask(cmd : EncodedCmd)
            GL.raw_glDepthMask( (GLboolean)bool.parse(cmd.parameters[0]) )
            return
        def static playbackGlDepthRangef(cmd : EncodedCmd)
            GL.raw_glDepthRangef( (GLclampf)double.parse(cmd.parameters[0]), (GLclampf)double.parse(cmd.parameters[1])  )
            return
        def static playbackGlDetachShader(cmd : EncodedCmd)
            GL.raw_glDetachShader( (GLuint)int.parse(cmd.parameters[0]), (GLuint)int.parse(cmd.parameters[1]))
            return
        def static playbackGlDisable(cmd : EncodedCmd)
            GL.raw_glDisable( (GLenum)int.parse(cmd.parameters[0]))
            return
        def static playbackGlDisableVertexAttribArray(cmd : EncodedCmd)
            GL.raw_glDisableVertexAttribArray( (GLuint)int.parse(cmd.parameters[0]))
            return
        def static playbackGlDrawArrays(cmd : EncodedCmd)
            GL.raw_glDrawArrays( (GLenum)int.parse(cmd.parameters[0]), (GLint)int.parse(cmd.parameters[1]), (GLsizei)int.parse(cmd.parameters[2])  )
            return
        def static playbackGlDrawElements(cmd : EncodedCmd)
            assert(int.parse(cmd.parameters[3])==0)
            GL.raw_glDrawElements( (GLenum)int.parse(cmd.parameters[0]),(GLsizei)int.parse(cmd.parameters[1])  , (GLenum)int.parse(cmd.parameters[2]), (GLvoid*)0)
            return
        def static playbackGlEnable(cmd : EncodedCmd)
            GL.raw_glEnable((GLenum)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlEnableVertexAttribArray(cmd : EncodedCmd)
            GL.raw_glEnableVertexAttribArray( (GLuint)int.parse(cmd.parameters[0]))
            return
        def static playbackGlFinish(cmd : EncodedCmd)
            GL.raw_glFinish( )
            return
        def static playbackGlFlush(cmd : EncodedCmd)
            GL.raw_glFlush( )
            return
        def static playbackGlFramebufferRenderbuffer(cmd : EncodedCmd)
            GL.raw_glFramebufferRenderbuffer( (GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), (GLenum)int.parse(cmd.parameters[2]), (GLuint)int.parse(cmd.parameters[3]))
            return
        def static playbackGlFramebufferTexture2D(cmd : EncodedCmd)
            GL.raw_glFramebufferTexture2D((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), (GLenum)int.parse(cmd.parameters[2]), (GLuint)int.parse(cmd.parameters[3]), (GLint)int.parse(cmd.parameters[4])  )
            return
        def static playbackGlFrontFace(cmd : EncodedCmd)
            GL.raw_glFrontFace((GLenum)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlGenBuffers(cmd : EncodedCmd)
            GL.raw_glGenBuffers( TypeSerializer.deserializeGLuintArr(cmd.parameters[0]))
            return
        def static playbackGlGenerateMipmap(cmd : EncodedCmd)
            GL.raw_glGenerateMipmap((GLenum)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlGenFramebuffers(cmd : EncodedCmd)
            GL.raw_glGenFramebuffers(TypeSerializer.deserializeGLuintArr(cmd.parameters[0]) )
            return
        def static playbackGlGenRenderbuffers(cmd : EncodedCmd)
            GL.raw_glGenRenderbuffers(TypeSerializer.deserializeGLuintArr(cmd.parameters[0]) )
            return
        def static playbackGlGenTextures(cmd : EncodedCmd)
            GL.raw_glGenTextures((TypeSerializer.deserializeGLuintArr(cmd.parameters[0]) ))
            return
        def static playbackGlGetActiveAttrib(cmd : EncodedCmd)
            unused : GLuint
            unused2 : GLint
            unused3 : GLsizei
            GL.raw_glGetActiveAttrib( (GLuint)int.parse(cmd.parameters[0]),(GLuint)int.parse(cmd.parameters[1]), (GLsizei)int.parse(cmd.parameters[2]) , out unused3, out unused2,out unused, cmd.parameters[6])
            return
        def static playbackGlGetActiveUniform(cmd : EncodedCmd)
            unused : GLint
            unused2 : GLuint
            unused3 : GLsizei
            GL.raw_glGetActiveUniform((GLuint)int.parse(cmd.parameters[0]), (GLuint)int.parse(cmd.parameters[1]),(GLsizei)int.parse(cmd.parameters[2])  ,out unused3 ,out unused ,out unused2, cmd.parameters[6])
            return
        def static playbackGlGetAttachedShaders(cmd : EncodedCmd)
            unused : GLsizei
            unused2 : GLuint
            GL.raw_glGetAttachedShaders((GLuint)int.parse(cmd.parameters[0]), (GLsizei)int.parse(cmd.parameters[1]), out unused, out unused2)
            return
        def static playbackGlGetAttribLocation(cmd : EncodedCmd)
            GL.raw_glGetAttribLocation((GLuint)int.parse(cmd.parameters[0]), cmd.parameters[1] )
            return
        def static playbackGlGetBooleanv(cmd : EncodedCmd)
            GL.raw_glGetBooleanv((GLenum)int.parse(cmd.parameters[0]), TypeSerializer.deserializeGLbooleanArr(cmd.parameters[1]) )
            return
        def static playbackGlGetBufferParameteriv(cmd : EncodedCmd)
            unused : GLint
            GL.raw_glGetBufferParameteriv((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), out unused )
            return
        def static playbackGlGetError(cmd : EncodedCmd)
            GL.raw_glGetError( )
            return
        def static playbackGlGetFloatv(cmd : EncodedCmd)
            GL.raw_glGetFloatv( (GLenum)int.parse(cmd.parameters[0]), TypeSerializer.deserializeGLfloatArr(cmd.parameters[1]))
            return
        def static playbackGlGetFramebufferAttachmentParameteriv(cmd : EncodedCmd)
            unused : GLint
            GL.raw_glGetFramebufferAttachmentParameteriv( (GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), (GLenum)int.parse(cmd.parameters[2]), out unused)
            return
        def static playbackGlGetIntegerv(cmd : EncodedCmd)
            GL.raw_glGetIntegerv((GLenum)int.parse(cmd.parameters[0]), TypeSerializer.deserializeGLintArr(cmd.parameters[1]) )
            return
        def static playbackGlGetProgramiv(cmd : EncodedCmd)
            unused : GLint
            GL.raw_glGetProgramiv( (GLuint)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), out unused)
            assert(unused != 0)
            return
        def static playbackGlGetProgramInfoLog(cmd : EncodedCmd)
            unused : GLsizei
            var bufsize = (GLsizei)int.parse(cmd.parameters[1])
            var unusedarr = new array of GLchar[bufsize]
            GL.raw_glGetProgramInfoLog((GLuint)int.parse(cmd.parameters[0]), bufsize, out unused, unusedarr )
            return
        def static playbackGlGetRenderbufferParameteriv(cmd : EncodedCmd)
            unused : GLint
            GL.raw_glGetRenderbufferParameteriv((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), out unused )
            return
        def static playbackGlGetShaderiv(cmd : EncodedCmd)
            unused : GLint
            GL.raw_glGetShaderiv( (GLuint)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), out unused)
            assert(unused != 0)  
            return
        def static playbackGlGetShaderInfoLog(cmd : EncodedCmd)
            unused : GLsizei
            var bufSize = (GLsizei)int.parse(cmd.parameters[1])
            var unusedarr = new array of GLchar[bufSize]
            GL.raw_glGetShaderInfoLog((GLuint)int.parse(cmd.parameters[0]), bufSize, out unused,  unusedarr )
            return
        def static playbackGlGetShaderPrecisionFormat(cmd : EncodedCmd)
            unused : GLint
            unused2 : GLint
            GL.raw_glGetShaderPrecisionFormat((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), out unused, out unused2 )
            return
        def static playbackGlGetShaderSource(cmd : EncodedCmd)
            unused : GLsizei
            GL.raw_glGetShaderSource((GLuint)int.parse(cmd.parameters[0]), (GLsizei)int.parse(cmd.parameters[1]), out unused, cmd.parameters[3]  )
            return
        def static playbackGlGetString(cmd : EncodedCmd)
            GL.raw_glGetString((GLenum)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlGetTexParameterfv(cmd : EncodedCmd)
            unused : GLfloat
            GL.raw_glGetTexParameterfv((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), out unused )
            return
        def static playbackGlGetTexParameteriv(cmd : EncodedCmd)
            unused : GLint
            GL.raw_glGetTexParameteriv((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]) , out unused)
            return
        def static playbackGlGetUniformfv(cmd : EncodedCmd)
            GL.raw_glGetUniformfv( (GLuint)int.parse(cmd.parameters[0]), (GLint)int.parse(cmd.parameters[1]), TypeSerializer.deserializeGLfloatArr(cmd.parameters[2]) )
            return
        def static playbackGlGetUniformiv(cmd : EncodedCmd)
            GL.raw_glGetUniformiv((GLuint)int.parse(cmd.parameters[0]), (GLint)int.parse(cmd.parameters[1]), TypeSerializer.deserializeGLintArr(cmd.parameters[2])   )
            return
        def static playbackGlGetUniformLocation(cmd : EncodedCmd)
            GL.raw_glGetUniformLocation( (GLuint)int.parse(cmd.parameters[0]), cmd.parameters[1])
            return
        def static playbackGlGetVertexAttribfv(cmd : EncodedCmd)
            unused : GLfloat
            GL.raw_glGetVertexAttribfv((GLuint)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), out unused )
            return
        def static playbackGlGetVertexAttribiv(cmd : EncodedCmd)
            unused : GLint
            GL.raw_glGetVertexAttribiv( (GLuint)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), out unused)
            return
        def static playbackGlGetVertexAttribPointerv(cmd : EncodedCmd)
            unused : GLvoid*
            GL.raw_glGetVertexAttribPointerv((GLuint)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), out unused)
            return
        def static playbackGlHint(cmd : EncodedCmd)
            GL.raw_glHint((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]) )
            return
        def static playbackGlIsBuffer(cmd : EncodedCmd)
            GL.raw_glIsBuffer((GLuint)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlIsEnabled(cmd : EncodedCmd)
            GL.raw_glIsEnabled((GLenum)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlIsFramebuffer(cmd : EncodedCmd)
            GL.raw_glIsFramebuffer((GLuint)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlIsProgram(cmd : EncodedCmd)
            GL.raw_glIsProgram((GLuint)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlIsRenderbuffer(cmd : EncodedCmd)
            GL.raw_glIsRenderbuffer((GLuint)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlIsShader(cmd : EncodedCmd)
            GL.raw_glIsShader((GLuint)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlIsTexture(cmd : EncodedCmd)
            GL.raw_glIsTexture((GLuint)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlLineWidth(cmd : EncodedCmd)
            GL.raw_glLineWidth( (GLfloat)double.parse(cmd.parameters[0]) )
            return
        def static playbackGlLinkProgram(cmd : EncodedCmd)
            GL.raw_glLinkProgram((GLuint)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlPixelStorei(cmd : EncodedCmd)
            GL.raw_glPixelStorei( (GLenum)int.parse(cmd.parameters[0]), (GLint)int.parse(cmd.parameters[1]) )
            return
        def static playbackGlPolygonOffset(cmd : EncodedCmd)
            GL.raw_glPolygonOffset( (GLfloat)double.parse(cmd.parameters[0]), (GLfloat)double.parse(cmd.parameters[1])  )
            return
        def static playbackGlReadPixels(cmd : EncodedCmd)
            var data = new array of GLvoid[int.parse(cmd.parameters[2]) *  int.parse(cmd.parameters[3])  * 4* 4]
            GL.raw_glReadPixels((GLint)int.parse(cmd.parameters[0]), (GLint)int.parse(cmd.parameters[1]), (GLsizei)int.parse(cmd.parameters[2]) , (GLsizei)int.parse(cmd.parameters[3]) ,(GLenum)int.parse(cmd.parameters[4]), (GLenum)int.parse(cmd.parameters[5]), data )
            return
        def static playbackGlReleaseShaderCompiler(cmd : EncodedCmd)
            GL.raw_glReleaseShaderCompiler( )
            return
        def static playbackGlRenderbufferStorage(cmd : EncodedCmd)
            GL.raw_glRenderbufferStorage((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), (GLsizei)int.parse(cmd.parameters[2]), (GLsizei)int.parse(cmd.parameters[3]) )
            return
        def static playbackGlSampleCoverage(cmd : EncodedCmd)
            GL.raw_glSampleCoverage( (GLclampf)double.parse(cmd.parameters[0]), (GLboolean)bool.parse(cmd.parameters[1])  )
            return
        def static playbackGlScissor(cmd : EncodedCmd)
            GL.raw_glScissor((GLint)int.parse(cmd.parameters[0]), (GLint)int.parse(cmd.parameters[1]),(GLsizei)int.parse(cmd.parameters[2]) ,(GLsizei)int.parse(cmd.parameters[3]) )
            return
        def static playbackGlShaderBinary(cmd : EncodedCmd)
            assert(false)
            GL.raw_glShaderBinary((GLsizei)int.parse(cmd.parameters[0]),(GLuint*)0 ,(GLenum)int.parse(cmd.parameters[2]), (GLvoid*)0,(GLsizei)int.parse(cmd.parameters[4])  )
            return
        def static playbackGlShaderSource(cmd : EncodedCmd)
            var a =  TypeSerializer.deserializeGLcharptrArr(cmd.parameters[2])
            var b = TypeSerializer.deserializeGLintArr(cmd.parameters[3])
            GL.raw_glShaderSource( (GLuint)int.parse(cmd.parameters[0]), (GLsizei)int.parse(cmd.parameters[1]), a, b)
            return
        def static playbackGlStencilFunc(cmd : EncodedCmd)
            GL.raw_glStencilFunc((GLenum)int.parse(cmd.parameters[0]),(GLint)int.parse(cmd.parameters[1])  ,(GLuint)int.parse(cmd.parameters[2]) )
            return
        def static playbackGlStencilFuncSeparate(cmd : EncodedCmd)
            GL.raw_glStencilFuncSeparate((GLenum)int.parse(cmd.parameters[0]),(GLenum)int.parse(cmd.parameters[1]) ,(GLint)int.parse(cmd.parameters[2])  ,(GLuint)int.parse(cmd.parameters[3]) )
            return
        def static playbackGlStencilMask(cmd : EncodedCmd)
            GL.raw_glStencilMask((GLuint)int.parse(cmd.parameters[0]) )
            return
        def static playbackGlStencilMaskSeparate(cmd : EncodedCmd)
            GL.raw_glStencilMaskSeparate((GLenum)int.parse(cmd.parameters[0]), (GLuint)int.parse(cmd.parameters[1]) )
            return
        def static playbackGlStencilOp(cmd : EncodedCmd)
            GL.raw_glStencilOp((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), (GLenum)int.parse(cmd.parameters[2]) )
            return
        def static playbackGlStencilOpSeparate(cmd : EncodedCmd)
            GL.raw_glStencilOpSeparate((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), (GLenum)int.parse(cmd.parameters[2]), (GLenum)int.parse(cmd.parameters[3]) )
            return
        def static playbackGlTexImage2D(cmd : EncodedCmd)
            ptr : array of char
            PointerSerializer.deserializeglTexImage2D(cmd, out ptr)
            GL.raw_glTexImage2D((GLenum)long.parse(cmd.parameters[0]), (GLint)int.parse(cmd.parameters[1]), (GLint)int.parse(cmd.parameters[2]), (GLsizei)int.parse(cmd.parameters[3]) , (GLsizei)int.parse(cmd.parameters[4]) , (GLint)int.parse(cmd.parameters[5]), (GLenum)int.parse(cmd.parameters[6]), (GLenum)int.parse(cmd.parameters[7]), ptr )
            return
        def static playbackGlTexParameterf(cmd : EncodedCmd)
            GL.raw_glTexParameterf((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), (GLfloat)double.parse(cmd.parameters[2])  )
            return
        def static playbackGlTexParameterfv(cmd : EncodedCmd)
            GL.raw_glTexParameterfv( (GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), TypeSerializer.deserializeGLfloatArr(cmd.parameters[2]))
            return
        def static playbackGlTexParameteri(cmd : EncodedCmd)
            GL.raw_glTexParameteri((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]), (GLint)int.parse(cmd.parameters[2])  )
            return
        def static playbackGlTexParameteriv(cmd : EncodedCmd)
            GL.raw_glTexParameteriv((GLenum)int.parse(cmd.parameters[0]), (GLenum)int.parse(cmd.parameters[1]),  TypeSerializer.deserializeGLintArr(cmd.parameters[2]) )
            return
        def static playbackGlTexSubImage2D(cmd : EncodedCmd)
            assert(false)
            GL.raw_glTexSubImage2D((GLenum)int.parse(cmd.parameters[0]),(GLint)int.parse(cmd.parameters[1]), (GLint)int.parse(cmd.parameters[2]), (GLint)int.parse(cmd.parameters[3]), (GLsizei)int.parse(cmd.parameters[4]) , (GLsizei)int.parse(cmd.parameters[0]) ,(GLenum)int.parse(cmd.parameters[6]), (GLenum)int.parse(cmd.parameters[7]), (GLvoid*)0  )
            return
        def static playbackGlUniform1f(cmd : EncodedCmd)
            GL.raw_glUniform1f((GLint)int.parse(cmd.parameters[0]), (GLfloat)double.parse(cmd.parameters[1])   )
            return
        def static playbackGlUniform1fv(cmd : EncodedCmd)
            GL.raw_glUniform1fv((GLint)int.parse(cmd.parameters[0]), (GLsizei)int.parse(cmd.parameters[1]), (GLfloat*)0   )
            return
        def static playbackGlUniform1i(cmd : EncodedCmd)
            GL.raw_glUniform1i((GLint)int.parse(cmd.parameters[0]), (GLint)int.parse(cmd.parameters[1])   )
            return
        def static playbackGlUniform1iv(cmd : EncodedCmd)
            ret : array of char
            PointerSerializer.deserializeglUniform1iv(cmd, out ret)
            GL.raw_glUniform1iv((GLint)int.parse(cmd.parameters[0]), (GLsizei)int.parse(cmd.parameters[1]), ret   )
            return
        def static playbackGlUniform2f(cmd : EncodedCmd)
            GL.raw_glUniform2f((GLint)int.parse(cmd.parameters[0]), (GLfloat)double.parse(cmd.parameters[1]), (GLfloat)double.parse(cmd.parameters[2])    )
            return
        def static playbackGlUniform2fv(cmd : EncodedCmd)
            GL.raw_glUniform2fv((GLint)int.parse(cmd.parameters[0]), (GLsizei)int.parse(cmd.parameters[1]), (GLfloat*)0   )
            return
        def static playbackGlUniform2i(cmd : EncodedCmd)
            GL.raw_glUniform2i((GLint)int.parse(cmd.parameters[0]), (GLint)int.parse(cmd.parameters[1]), (GLint)int.parse(cmd.parameters[2])    )
            return
        def static playbackGlUniform2iv(cmd : EncodedCmd)
            GL.raw_glUniform2iv((GLint)int.parse(cmd.parameters[0]), (GLsizei)int.parse(cmd.parameters[1]), (GLint*)0   )
            return
        def static playbackGlUniform3f(cmd : EncodedCmd)
            GL.raw_glUniform3f((GLint)int.parse(cmd.parameters[0]), (GLfloat)double.parse(cmd.parameters[1]), (GLfloat)double.parse(cmd.parameters[2]), (GLfloat)double.parse(cmd.parameters[3])     )
            return
        def static playbackGlUniform3fv(cmd : EncodedCmd)
            ret : array of char
            PointerSerializer.deserializeglUniform3fv(cmd, out ret)
            GL.raw_glUniform3fv((GLint)int.parse(cmd.parameters[0]), (GLsizei)int.parse(cmd.parameters[1]), ret)
            return
        def static playbackGlUniform3i(cmd : EncodedCmd)
            GL.raw_glUniform3i((GLint)int.parse(cmd.parameters[0]), (GLint)int.parse(cmd.parameters[1]), (GLint)int.parse(cmd.parameters[2]), (GLint)int.parse(cmd.parameters[3])     )
            return
        def static playbackGlUniform3iv(cmd : EncodedCmd)
            GL.raw_glUniform3iv( (GLint)int.parse(cmd.parameters[0]), (GLsizei)int.parse(cmd.parameters[1]), (GLint*)0  )
            return
        def static playbackGlUniform4f(cmd : EncodedCmd)
            GL.raw_glUniform4f( (GLint)int.parse(cmd.parameters[0]), (GLfloat)double.parse(cmd.parameters[1]), (GLfloat)double.parse(cmd.parameters[2]), (GLfloat)double.parse(cmd.parameters[3]), (GLfloat)double.parse(cmd.parameters[4]))
            return
        def static playbackGlUniform4fv(cmd : EncodedCmd)
            ret : array of char
            PointerSerializer.deserializeglUniform4fv(cmd, out ret)
            GL.raw_glUniform4fv( (GLint)int.parse(cmd.parameters[0]), (GLsizei)int.parse(cmd.parameters[1]), ret  )
            return
        def static playbackGlUniform4i(cmd : EncodedCmd)
            GL.raw_glUniform4i( (GLint)int.parse(cmd.parameters[0]), (GLint)int.parse(cmd.parameters[1]), (GLint)int.parse(cmd.parameters[2]), (GLint)int.parse(cmd.parameters[3]), (GLint)int.parse(cmd.parameters[4]))
            return
        def static playbackGlUniform4iv(cmd : EncodedCmd)
            GL.raw_glUniform4iv((GLint)int.parse(cmd.parameters[0]), (GLsizei)int.parse(cmd.parameters[1]), (GLint*)0   )
            return
        def static playbackGlUniformMatrix2fv(cmd : EncodedCmd)
            GL.raw_glUniformMatrix2fv((GLint)int.parse(cmd.parameters[0]),(GLsizei)int.parse(cmd.parameters[1])  , (GLboolean)bool.parse(cmd.parameters[2]),   (GLfloat*)0 )
            return
        def static playbackGlUniformMatrix3fv(cmd : EncodedCmd)
            GL.raw_glUniformMatrix3fv((GLint)int.parse(cmd.parameters[0]), (GLsizei)int.parse(cmd.parameters[1]) , (GLboolean)bool.parse(cmd.parameters[2]),   (GLfloat*)0 )
            return
        def static playbackGlUniformMatrix4fv(cmd : EncodedCmd)
            ret : array of char
            PointerSerializer.deserializeglUniformMatrix4fv(cmd, out ret)
            GL.raw_glUniformMatrix4fv((GLint)int.parse(cmd.parameters[0]),(GLsizei)int.parse(cmd.parameters[1])  , (GLboolean)bool.parse(cmd.parameters[2]), ret )
            return
        def static playbackGlUseProgram(cmd : EncodedCmd)
            GL.raw_glUseProgram( (GLuint)int.parse(cmd.parameters[0]))
            return
        def static playbackGlValidateProgram(cmd : EncodedCmd)
            GL.raw_glValidateProgram( (GLuint)int.parse(cmd.parameters[0]))
            return
        def static playbackGlVertexAttrib1f(cmd : EncodedCmd)
            GL.raw_glVertexAttrib1f( (GLuint)int.parse(cmd.parameters[0]), (GLfloat)double.parse(cmd.parameters[1]) )
            return
        def static playbackGlVertexAttrib1fv(cmd : EncodedCmd)
            GL.raw_glVertexAttrib1fv( (GLuint)int.parse(cmd.parameters[0]), (GLfloat*)0)
            return
        def static playbackGlVertexAttrib2f(cmd : EncodedCmd)
            GL.raw_glVertexAttrib2f( (GLuint)int.parse(cmd.parameters[0]), (GLfloat)double.parse(cmd.parameters[1]), (GLfloat)double.parse(cmd.parameters[2])  )
            return
        def static playbackGlVertexAttrib2fv(cmd : EncodedCmd)
            GL.raw_glVertexAttrib2fv( (GLuint)int.parse(cmd.parameters[0]), (GLfloat*)0)
            return
        def static playbackGlVertexAttrib3f(cmd : EncodedCmd)
            GL.raw_glVertexAttrib3f( (GLuint)int.parse(cmd.parameters[0]), (GLfloat)double.parse(cmd.parameters[1]), (GLfloat)double.parse(cmd.parameters[2]), (GLfloat)double.parse(cmd.parameters[3])   )
            return
        def static playbackGlVertexAttrib3fv(cmd : EncodedCmd)
            GL.raw_glVertexAttrib3fv( (GLuint)int.parse(cmd.parameters[0]), (GLfloat*)0)
            return
        def static playbackGlVertexAttrib4f(cmd : EncodedCmd)
            GL.raw_glVertexAttrib4f( (GLuint)int.parse(cmd.parameters[0]), (GLfloat)double.parse(cmd.parameters[1]), (GLfloat)double.parse(cmd.parameters[2]), (GLfloat)double.parse(cmd.parameters[3]), (GLfloat)double.parse(cmd.parameters[4]))
            return
        def static playbackGlVertexAttrib4fv(cmd : EncodedCmd)
            GL.raw_glVertexAttrib4fv( (GLuint)int.parse(cmd.parameters[0]), (GLfloat*)0)
            return
        def static playbackGlVertexAttribPointer(cmd : EncodedCmd)
            GL.raw_glVertexAttribPointer( (GLuint)int.parse(cmd.parameters[0]),(GLint)int.parse(cmd.parameters[1]), (GLenum)int.parse(cmd.parameters[2]), (GLboolean)bool.parse(cmd.parameters[3]), (GLsizei)int.parse(cmd.parameters[4])  ,(GLvoid*)int.parse(cmd.parameters[5]) )
            return
        def static playbackGlViewport(cmd : EncodedCmd)
            GL.raw_glViewport((GLint)int.parse(cmd.parameters[0]), (GLint)int.parse(cmd.parameters[1]), (GLsizei)int.parse(cmd.parameters[2]), (GLsizei)int.parse(cmd.parameters[3])     )
            return
