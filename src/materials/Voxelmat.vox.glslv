/*
 *  libsoy - soy.materials.Voxelmat.vox_glslv
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

attribute vec3 vertex, color;
attribute float normal;
attribute float light;
uniform mat4 mv_matrix, p_matrix;
varying vec3 vVertex, vNormal;
varying vec4 vColor;
varying float vAO;

void main() {
    vec3 normals[6];
    normals[0] = vec3(0,0,1);
    normals[1] = vec3(0,0,-1);
    normals[2] = vec3(1,0,0);
    normals[3] = vec3(-1,0,0);
    normals[4] = vec3(0,-1,0);
    normals[5] = vec3(0,1,0);
    gl_Position = p_matrix*mv_matrix*vec4(vertex,1.0);
    vNormal = vec3(mv_matrix*vec4(normals[int(floor(normal))],0.0));
    vVertex = vec3(mv_matrix*vec4(vertex,1.0));
    vAO = light;
    vColor = vec4(color, 1);
}
