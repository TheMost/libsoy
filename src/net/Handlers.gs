/*
 *  libsoy - soy.net.Handlers
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 *  soy.net.Handlers - Handlers for different connection events.
 *  They can be used to define custom methods to perform certain functions on such events
 *  Statistics Gathering is an example of such functions
 */


[indent=4]
uses
    GLib
    Lm
    Nice

delegate HandlerDelegate ()
delegate StatisticsDelegate (jid : string, stats: dict of string, string)

class soy.net.Handlers : Object
 
    connection_success: unowned HandlerDelegate
    connection_fail: unowned HandlerDelegate
    authentication_success : unowned HandlerDelegate
    authentication_fail : unowned HandlerDelegate
    statistics_handler : unowned StatisticsDelegate

    //These events are hardcoded because at present, they are the only possible events for a client state
    //Callback Functions can be defined by the user
    
    construct (conn_success : HandlerDelegate, conn_fail : HandlerDelegate, auth_success : HandlerDelegate, auth_fail : HandlerDelegate, stats_handler : StatisticsDelegate)
    
        connection_success = conn_success
        connection_fail =  conn_fail
        authentication_success = auth_success
        authentication_fail  = auth_fail
        statistics_handler = stats_handler
