/*
 *  libsoy - soy.atoms.Face
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GLib


class soy.atoms.Face : Object
    event on_set (face : Face)

    area_vector: soy.atoms.Vector

    construct (a : Vertex, b : Vertex, c : Vertex)
        self._a = a
        self._b = b
        self._c = c
        _a.on_set.connect(self._vertex_set)
        _b.on_set.connect(self._vertex_set)
        _c.on_set.connect(self._vertex_set)
        self._calculate_face_vector()

        self._material = new soy.materials.Material()

    construct with_material (a : Vertex, b : Vertex, c : Vertex,
                             material : soy.materials.Material)
        self._a = a
        self._b = b
        self._c = c
        _calculate_face_vector()
        _a.on_set.connect(self._vertex_set)
        _b.on_set.connect(self._vertex_set)
        _c.on_set.connect(self._vertex_set)
        self._material = material

    ////////////////////////////////////////////////////////////////////////
    //Methods

    //Calculates and stores area vectors

    def _calculate_face_vector()
        vectA1 : soy.atoms.Vector = new soy.atoms.Vector(
                                            _a.position.x,
                                            _a.position.y,
                                            _a.position.z)
        vectA2 : soy.atoms.Vector = new soy.atoms.Vector(
                                            _a.position.x,
                                            _a.position.y,
                                            _a.position.z)
        vectB : soy.atoms.Vector = new soy.atoms.Vector(
                                            _b.position.x,
                                            _b.position.y,
                                            _b.position.z)
        vectC : soy.atoms.Vector = new soy.atoms.Vector(
                                            _c.position.x,
                                            _c.position.y,
                                            _c.position.z)
        vectA1.subtract(vectB)
        vectA2.subtract(vectC)
        (vectA1.cross(vectA2)).multiply(0.5f)
        self.area_vector = vectA1


    //Returns face vector after rotation by unit quaternion
    def get_rotated_face(rotation: soy.atoms.Rotation) : soy.atoms.Vector
        conjugate : soy.atoms.Rotation = rotation.conjugate()
        temp : soy.atoms.Rotation =  rotation.mul(new soy.atoms.Rotation(
                                                        0,
                                                        area_vector.x,
                                                        area_vector.y,
                                                        area_vector.z)).mul(conjugate)
        return new soy.atoms.Vector(temp.x,temp.y,temp.z)

    //returns whether a ray is intersecting a face/triange or not
    //position : position of origin of ray
    //direction : direction of ray
    def is_intersecting(position : soy.atoms.Vector, direction: soy.atoms.Vector) : bool
        //using Moller–Trumbore intersection algorithm
        vectA : soy.atoms.Vector = new soy.atoms.Vector(
                                            _a.position.x,
                                            _a.position.y,
                                            _a.position.z)
        edge1 : soy.atoms.Vector = new soy.atoms.Vector(
                                            _b.position.x,
                                            _b.position.y,
                                            _b.position.z)
        edge2 : soy.atoms.Vector = new soy.atoms.Vector(
                                            _c.position.x,
                                            _c.position.y,
                                            _c.position.z)
        edge1.subtract(vectA)
        edge2.subtract(vectA)
        direction.normalize()
        p : soy.atoms.Vector = direction.cross(edge2)
        det : float = edge1.dot(p)
        if det<0.0001f and det>-0.0001f
            return false;
        distance : soy.atoms.Vector =   new soy.atoms.Vector(
                                            _a.position.x,
                                            _a.position.y,
                                            _a.position.z)
        position.subtract(vectA)
        distance = position
        inv_det : float = 1.0f/det
        u : float = distance.dot(p)*inv_det
        if u < 0 || u > 1
            return false

        v : float = direction.dot(distance.cross(edge1))*inv_det
        if v<0 || (u+v) >1
            return false

        t : float =  edge2.dot(distance.cross(edge1))*inv_det
        if t > 0.0001f
            return true
        return false


    ////////////////////////////////////////////////////////////////////////
    // Sequence Methods

    _a : Vertex
    _b : Vertex
    _c : Vertex

    def new get (index : int) : Vertex?
        if index is 0
            return self._a
        if index is 1
            return self._b
        if index is 2
            return self._c
        return null

    def new set (index : int, value : Vertex)
        if index is 0
            _a.on_set.disconnect(self._vertex_set)
            self._a = value
        else if index is 1
            _b.on_set.disconnect(self._vertex_set)
            self._b = value
        else if index is 2
            _c.on_set.disconnect(self._vertex_set)
            self._c = value
        value.on_set.connect(self._vertex_set)
        self._calculate_face_vector()
        self.on_set(self)


    ////////////////////////////////////////////////////////////////////////
    // Properties

    def _vertex_set(position : soy.atoms.Vertex)
        //print("face: vertex_set")
        self.on_set(self)

    _material : soy.materials.Material
    prop material : soy.materials.Material
        get
            return self._material
        set
            self._material = value
            self.on_set(self)


    def static cmp_eq (left : Object, right : Object) : bool
        if not (left isa soy.atoms.Face) or not (right isa soy.atoms.Face)
            return false

        _a : bool = soy.atoms.Vertex.cmp_eq(((soy.atoms.Face) left).get(0),
                                            ((soy.atoms.Face) right).get(0))
        _b : bool = soy.atoms.Vertex.cmp_eq(((soy.atoms.Face) left).get(1),
                                            ((soy.atoms.Face) right).get(1))
        _c : bool = soy.atoms.Vertex.cmp_eq(((soy.atoms.Face) left).get(2),
                                            ((soy.atoms.Face) right).get(2))
        // TODO: add material check?

        return (_a & _b & _c)


    def static cmp_ne (left : Object, right : Object) : bool
        return not cmp_eq(left, right)
