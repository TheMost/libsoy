/*
 *  libsoy - soy.textures.Heightmap
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GL
    GLib.Math

class soy.textures.Heightmap : soy.textures.Texture
    
    _mutex : Mutex

    init
        _mutex = Mutex()
        translucent = false

    construct ()
        channels = 1

    construct heightFieldData (dim_x : int, dim_y : int, frequency : float)

        u : uchar
        self.channels = 1
        self.size = new soy.atoms.Size(dim_x, dim_y)
        total : int = 0
        for var x = 0 to (dim_x-1)
            for var y = 0 to (dim_y-1)
                texels[total] = (uchar)(((marble_noise(10 , 0.5, frequency, x, y, dim_x, dim_y)+1) * 255) / 2)
                total = total + 1
             

    construct from_jpg (filename : string) raises IOError, MemoryError
        super.from_jpg(filename)

        c : int = self.channels
        o : int
        total : int = ((int)self.size.width * (int)self.size.height) - 1
        for var n = 0 to total
            o = n*c
            texels[n] = (texels[o] + texels[o+1] + texels[o+2]) / 3

        self.channels = 1

    construct from_png (filename : string) raises IOError, MemoryError
        super.from_png(filename)
        
        c : int = self.channels
        o : int
        total : int = ((int)self.size.width * (int)self.size.height) - 1
        for var n = 0 to total
            o = n*c
            texels[n] = (texels[o] + texels[o+1] + texels[o+2]) / 3

        self.channels = 1

    ////////////////////////////////////////////////////////////////////////
    // Methods

    def new get (index : int) : int
        // Return -1 if requested index is out of bounds
        if index < 0 or index >= (self.size.width * self.size.height)
            return -1

        return texels[index]


    def new set (index : int, value : int)

        texels[index] = (uchar) value

        // Flag for updating
        self._mutex.lock()
        self.updated = true
        self._mutex.unlock()

    // Noise functions are adaped from the implementation by Stefan Gustavson and Eliot Eshelman

    def marble_noise(octaves : int, persistence : double, scale : double, x : double, 
        y : double, xdim : int, ydim : int) : double
        
        twist : double = 4
        return Math.cos(x * scale/xdim + y * scale/ydim + twist * octave_noise(octaves, persistence, scale, x, y))

    
    def octave_noise(octaves : int, persistence : double, scale : double, x : double, y : double) : double
        // 2D Multi-Octave Simplex noise.

        // For each octave, a higher frequency/lower amplitude function will be added
        // to the original. The higher the persistence [0-1], the more of each
        // succeeding octave will be added.
    
        total : double = 0.0
        frequency : double = scale
        amplitude : double = 1.0

        // We have to keep track of the largest possible amplitude,
        // because each octave adds more, and we need a value in [-1, 1].
        maxAmplitude : double = 0.0;

        for var i = 0 to octaves
            total += raw_noise(x * frequency, y * frequency) * amplitude
            frequency *= 2.0
            maxAmplitude += amplitude;
            amplitude *= persistence

        return total / maxAmplitude

    def raw_noise(x : double, y : double) : double
        // 2D Raw Simplex noise.
        // Noise contributions from the three corners
        n0 : double = 0.0 
        n1 : double = 0.0 
        n2 : double = 0.0

        // Skew the input space to determine which simplex cell we're in
        F2 :double = 0.5 * (Math.sqrt(3.0) - 1.0)
        // Hairy skew factor for 2D
        s : double = (x + y) * F2
        i : int = (int)(x + s)
        j : int = (int)(y + s)

        G2 : double = (3.0 - Math.sqrt(3.0)) / 6.0
        t : double = (double)(i + j) * G2
        // Unskew the cell origin back to (x,y) space
        X0 : double = i - t
        Y0 : double = j - t
        // The x,y distances from the cell origin
        x0 : double = x - X0
        y0 : double = y - Y0

        // For the 2D case, the simplex shape is an equilateral triangle.
        // Determine which simplex we are in.
        i1 : int = 0 
        j1 : int = 0 // Offsets for second (middle) corner of simplex in (i,j) coords
        if x0 > y0 // lower triangle, XY order: (0,0)->(1,0)->(1,1)
            i1 = 1
            j1 = 0
        else        // upper triangle, YX order: (0,0)->(0,1)->(1,1)
            i1 = 0
            j1 = 1

        // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
        // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
        // c = (3-sqrt(3))/6
        x1 : double = x0 - i1 + G2       // Offsets for middle corner in (x,y) unskewed coords
        y1 : double = y0 - j1 + G2
        x2 : double = x0 - 1.0 + 2.0 * G2  // Offsets for last corner in (x,y) unskewed coords
        y2 : double = y0 - 1.0 + 2.0 * G2

        // Work out the hashed gradient indices of the three simplex corners
        ii : int = (int)(i) & 255
        jj : int = (int)(j) & 255
        gi0 : int = perm[ii + perm[jj]] % 12
        gi1 : int = perm[ii + i1 + perm[jj+j1]] % 12
        gi2 : int = perm[ii + 1 + perm[jj+1]] % 12

        // Calculate the contribution from the three corners
        t0 : double = 0.5 - x0*x0 - y0*y0
        if t0 < 0
            n0 = 0.0
        else
            t0 *= t0
            n0 = t0 * t0 * dot2d(grad3[gi0], x0, y0)
    
        t1 : double = 0.5 - x1*x1 - y1*y1
        if t1 < 0
            n1 = 0.0
        else
            t1 *= t1
            n1 = t1 * t1 * dot2d(grad3[gi1], x1, y1)

        t2 : double = 0.5 - x2*x2-y2*y2
        if t2 < 0
            n2 = 0.0
        else
            t2 *= t2
            n2 = t2 * t2 * dot2d(grad3[gi2], x2, y2)

        // Add contributions from each corner to get the final noise value.
        // The result is scaled to return values in the interval [-1,1].
        return 70.0 * (n0 + n1 + n2)

    def dot2d(g : soy.atoms.Position, x : double, y : double) : double
        return g.x*x + g.y*y

    // A permutarion array used by Ken Perlin
    perm : array of int = {
        151,160,137,91,90,15,
        131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
        190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
        88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
        77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
        102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
        135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
        5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
        223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
        129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
        251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
        49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
        138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180,
        151,160,137,91,90,15,
        131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
        190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
        88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
        77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
        102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
        135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
        5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
        223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
        129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
        251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
        49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
        138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
        }

    // The gradients are the midpoints of the vertices of a cube.
    grad3 : array of soy.atoms.Position = {
        new soy.atoms.Position(1,1,0), 
        new soy.atoms.Position(-1,1,0), 
        new soy.atoms.Position(1,-1,0), 
        new soy.atoms.Position(-1,-1,0),
        new soy.atoms.Position(1,0,1), 
        new soy.atoms.Position(-1,0,1), 
        new soy.atoms.Position(1,0,-1), 
        new soy.atoms.Position(-1,0,-1),
        new soy.atoms.Position(0,1,1), 
        new soy.atoms.Position(0,-1,1), 
        new soy.atoms.Position(0,1,-1), 
        new soy.atoms.Position(0,-1,-1)
        }


