/*
 *  libsoy - soy.net.Commands
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 *  soy.net.Commands - a Adhoc Command handler class for the Server
 */


[indent=4]
uses
    GLib
    Lm
    Nice

class soy.net.Commands : Object

    command_handlers : dict of string, CommandHandler;
    //construct (port : int)
    
    def handle_command (handler: CommandHandler) : bool
 
        print "command_handler"
        
        //The Lm.Message parameter is essential here for further processing of Adhoc commands
        //Nodes and attributes can be extracted from the command Stanzas for processing
        
        handler.function()
        return true

    // Add commands and respective handlers
    def add_command(command: string, function: HandleCommandFunction) : bool
        if this.command_handlers[command] != null
           return false
        var command_object = new CommandHandler(function)
        this.command_handlers[command] = command_object
        return true

    //Get a command from the given string
    def get_command(command: string) : CommandHandler
        
        var ob = this.command_handlers[command]
        return ob
        
    //This method results in the list of all available commands. Node and name are the same now for simplicity.
    //Make sure the nodes are human understandable.
    def get_commands(sid: string, jid : string) : Lm.Message
        var mess = new Lm.Message(jid, Lm.MessageType.IQ);
        var iq = mess.get_node()
        iq.set_attribute("from", sid)
        iq.set_attribute("type","result")
        //Add attributes to the nodes
        
        var query = iq.add_child("query", null)
        query.set_attribute("xmlns", "http://jabber.org/protocol/disco#items")
        query.set_attribute("node", "http://jabber.org/protocol/commands")

        for s in command_handlers.keys
            var tmp = query.add_child("item", null)
            tmp.set_attribute("jid", sid)
            tmp.set_attribute("node", s)
            tmp.set_attribute("name", s)
            
        return mess

    //XMPP Message when a 'cancel command' is received by the Server
    //The message pertaining to that cancel command received by the client should be passed as an argument
    def handle_cancel_command(m : Lm.Message) : Lm.Message
        var iq = m.get_node()
        var to = iq.get_attribute("from")
        var from = iq.get_attribute("to")
        var id = iq.get_attribute("id")

        var command = iq.get_child("command")
        //Add attributes to the nodes
        var sessionid = command.get_attribute("sessionsid")
        var node = command.get_attribute("node")

        var mess = new Lm.Message(to, Lm.MessageType.IQ);
        var siq = mess.get_node()
        siq.set_attribute("from", from)
        siq.set_attribute("id", id)

        var scommand = iq.add_child("command", null)
        scommand.set_attribute("xmlns", "http://jabber.org/protocol/commands")
        scommand.set_attribute("sessionid", sessionid)
        scommand.set_attribute("node", node)
        scommand.set_attribute("status", "canceled")
        
        return mess
    
    //XMPP Message when server is handling the 'Executing command'
    //Arugments are the XMPP message object received by the server and a dictionary    
    def handle_executing_command(m : Lm.Message, payload : dict of string, string) : Lm.Message
        var iq = m.get_node()
        var to = iq.get_attribute("from")
        var from = iq.get_attribute("to")
        var id = iq.get_attribute("id")

        var command = iq.get_child("command")
        //Add attributes to the nodes
        var sessionid = command.get_attribute("sessionsid")
        var node = command.get_attribute("node")

        var mess = new Lm.Message(to, Lm.MessageType.IQ);
        var siq = mess.get_node()
        siq.set_attribute("from", from)
        siq.set_attribute("id", id)

        var scommand = iq.add_child("command", null)
        scommand.set_attribute("xmlns", "http://jabber.org/protocol/commands")
        scommand.set_attribute("sessionid", sessionid)
        scommand.set_attribute("node", node)
        scommand.set_attribute("status", "canceled")
        
        var x = iq.add_child("x", null)
        x.set_attribute("xmlns", "jabber:x:data")
        x.set_attribute("type", "result")
        var field = x.add_child("field", null)    
        
        //TODO : Now, only a basic kind of payload is supported. It is not very extensible
        for var s in payload.keys
            var tmp = field.add_child("option", null)
            tmp.set_attribute("label", s)
            tmp.set_attribute("value", payload[s])

        return mess

//Delegate to Pass around function pointers
delegate HandleCommandFunction () : bool

class CommandHandler : Object

    function : unowned HandleCommandFunction?
    construct (function : HandleCommandFunction)
        self.function = function
